/* https://eddmann.com/posts/implementing-a-dynamic-vector-array-in-c/
 * */
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

typedef struct LinkedList
{
    void *item;
    struct LinkedList *next;
    
} LinkedList;

typedef struct Vector
{
    LinkedList *items;
    int size;
    
} Vector;

/* Init a vector */
void VectorInit(Vector *vector);

/* Add an item */
void VectorAddItem(Vector *vector, void *item);

#endif
