/* Chip 8 functions */

#include "Chip8.h"

/* Init Memory */
bool Chip8_Init(Chip8 *chip)
{
    /* Set memory to 0 */
    memset(chip->ram, 0, CHIP8_RAM_SIZE);
    memset(chip->registers, 0, CHIP8_REGISTERS_SIZE);
    memset(chip->screen, 0, CHIP8_SCREEN_WIDTH*CHIP8_SCREEN_HEIGHT);
    memset(chip->stack, 0, CHIP8_STACK_SIZE);
    memset(chip->keys, 0, CHIP8_NUM_KEYS);

    /* Load the Pre-Stored Font into RAM */
    uint8_t font[] = {
        0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
        0x20, 0x60, 0x20, 0x20, 0x70, // 1
        0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
        0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
        0x90, 0x90, 0xF0, 0x10, 0x10, // 4
        0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
        0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
        0xF0, 0x10, 0x20, 0x40, 0x40, // 7
        0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
        0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
        0xF0, 0x90, 0xF0, 0x90, 0x90, // A
        0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
        0xF0, 0x80, 0x80, 0x80, 0xF0, // C
        0xE0, 0x90, 0x90, 0x90, 0xE0, // D
        0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
        0xF0, 0x80, 0xF0, 0x80, 0x80 // F
    };

    memcpy(chip->ram, font, sizeof(font));

    /* Program Counter starting location */
    chip->pc = CHIP8_PC_START;

    /* Set the Instruction Register */
    chip->I = 0;

    /* Set the stack pointer */
    chip->stackPointer = 0;
    
    /* Set the delay timer */
    chip->delay = 0;

    /* Set the sound timer */
    chip->sound = 0;

    return true;
}

/* Load the given ROM into the chip8 RAM */
bool Chip8_LoadRom(Chip8 *chip, char *fileName)
{
    int i;
    int fsize;

    //printf("File name: %s\n", fileName);

    /* Open the File */
    FILE *fp = fopen(fileName, "rb");
    if (!fp) {
        //printf("Chip8_LoadRom: failed to open file: %s\n", fileName);
        return false;
    }
    
    /* Get the file size */
    fseek(fp, 0, SEEK_END);
    fsize = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    /* Read the ROM into game memory */
    int bytesRead = fread((void*)(&chip->ram[CHIP8_ROM_START]), sizeof(uint8_t), fsize, fp);
    if (bytesRead == 0) {
        //printf("Chip8_LoadRom: Failed to read ROM\n");
        if (fp) {
            fclose(fp);
            fp = NULL;
            return false;
        }
    }

#if 0
    // DEBUG!
    for (i = CHIP8_ROM_START; i < CHIP8_RAM_SIZE - CHIP8_ROM_START; i++)
    {
        //printf("0x%02X ", chip->ram[i]);

        if (i != CHIP8_ROM_START && i % 16 == 0) {
            //printf("\n");
        }
    }
    //printf("\n\n\n");
#endif

    fclose(fp); fp = NULL;
    return true;
}

/* Load 2bytes (for an opcode)
 * from RAM memory at the given location */
uint16_t Chip8_LoadInstruction(Chip8 *chip, int loc)
{
    /* Should be 16bit aligned! (2 bytes) */
    //if (loc % 2 != 0) {
    //    //printf("Chip8_LoadOp: unaligned access at 0x%X\n", loc);
    //    return 0;
    //}

    uint8_t byte0 = chip->ram[loc];
    uint8_t byte1 = chip->ram[loc+1];
    
    uint16_t op = (byte0 << 8) | byte1;

    return op;
}

bool Chip8_ExecuteInstruction(Chip8 *chip, uint16_t instruction)
{
    /* Get the op to run */
    int op = Instruction_Op(instruction);

    /* Call the target function */
    ops[op](chip, instruction);
    
    return true;
}

void Chip8_DecodeOp0(Chip8 *chip, uint16_t instruction)
{
    /* Get the subfunction */
    uint8_t subfunc = Instruction_SubOpDouble(instruction);
    
    /* Call the subfunction */
    ops_0[subfunc](chip, instruction);
}

void Chip8_DecodeOp8(Chip8 *chip, uint16_t instruction)
{
    /* Get the subfunction */
    uint8_t subfunc = Instruction_SubOpSingle(instruction);
    
    /* Call the subfunction */
    ops_8[subfunc](chip, instruction);
}

void Chip8_DecodeOpE(Chip8 *chip, uint16_t instruction)
{
    /* Get the subfunction */
    uint8_t subfunc = Instruction_SubOpDouble(instruction);
    
    /* Call the subfunction */
    ops_E[subfunc](chip, instruction);
}

void Chip8_DecodeOpF(Chip8 *chip, uint16_t instruction)
{
    /* Get the subfunction */
    uint8_t subfunc = Instruction_SubOpDouble(instruction);
    
    /* Call the subfunction */
    ops_F[subfunc](chip, instruction);
}
