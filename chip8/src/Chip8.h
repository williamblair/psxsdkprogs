/* Chip8 header */
#include "Display.h"
#include "Pad.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <psx.h>

#ifndef CHIP8_H_INCLUDED
#define CHIP8_H_INCLUDED

/* 16 bytes registers */
#define CHIP8_REGISTERS_SIZE 0x10

/* 4KB Ram */
#define CHIP8_RAM_SIZE 0x1000

/* 16 stack levels */
#define CHIP8_STACK_SIZE 0x10

/* 16 Keys, 0x0-0xF */
#define CHIP8_NUM_KEYS 0x10

/* Where programs should reside in MEM */
#define CHIP8_ROM_START 0x200

/* Chip8 has screen resolution of 64x32, black and white */
#define CHIP8_SCREEN_WIDTH  64
#define CHIP8_SCREEN_HEIGHT 32

/* Where the program counter starts 
 * (same place as ROM) */
#define CHIP8_PC_START CHIP8_ROM_START

/* The macros below for names use the format
 * used by the cowbyte spec in terms of instructions: 
 * http://vanbeveren.byethost13.com/stuff/CHIP8.pdf?i=1 */

/* ops are 16 bits and we want bits 16-12,
 * so no mask required */
#define Instruction_Op(instr) \
    (instr >> 12)
    
/* For instructions like 0x0 bits 12-1 determine 
 * the subfunction (same as NNN) */
#define Instruction_SubOpTriple(instr) \
    (instr & 0xFFF)

/* For instructions like F bits 8-1 determine the
 * subfunction (same as KK) */
#define Instruction_SubOpDouble(instr) \
    (instr & 0xFF)

/* For instructions like 8 bits 4-1 determine the\
 * subfunction (same as N) */
#define Instruction_SubOpSingle(instr) \
    (instr & 0xF)

/* Bits 12-1 */
#define Instruction_NNN(instr) \
    (instr & 0xFFF)

/* Bits 12-8 */
#define Instruction_X(instr) \
    ((instr >> 8) & 0xF)

/* Bits 8-4 */
#define Instruction_Y(instr) \
    ((instr >> 4) & 0xF)

/* Bits 8-1 */
#define Instruction_KK(instr) \
    (instr & 0xFF)

/* Bits 4-1 */
#define Instruction_N(instr) \
    (instr & 0xF)

/* Get a pixel from screen mem */
#define GetPixel(chip, x, y) \
    (chip->screen[ (y) * CHIP8_SCREEN_WIDTH + (x)])

/* Set a pixel in screen mem 
 (XORs its current value) */
#define DrawPixel(chip, x, y ) \
    (chip->screen[ (y) * CHIP8_SCREEN_WIDTH + (x)] ^= 1)

/* Set a pixel in screen mem with an exact value */
#define DrawPixelWithValue(chip, x, y, value) \
    (chip->screen[ (y) * CHIP8_SCREEN_WIDTH + (x)] = value)

typedef struct Chip8
{
    /* Screen Memory */
    uint8_t screen[CHIP8_SCREEN_WIDTH*CHIP8_SCREEN_HEIGHT];

    /* RAM memory */
    uint8_t ram[CHIP8_RAM_SIZE];

    /* The Stack */
    uint16_t stack[CHIP8_STACK_SIZE];
    int      stackPointer; // the current top of the stack

    /* Keyboard */
    uint8_t keys[CHIP8_NUM_KEYS];

    /* Address register */
    uint16_t I;
    
    /* Program Counter */
    int pc;

    /* Registers */
    uint8_t registers[CHIP8_REGISTERS_SIZE];
    
    /* Delay Timer */
    uint8_t delay;
    
    /* Sound timer */
    uint8_t sound;

} Chip8;

bool Chip8_Init(Chip8 *chip);
bool Chip8_LoadRom(Chip8 *chip, char *fileName);
uint16_t Chip8_LoadInstruction(Chip8 *chip, int loc);
bool Chip8_ExecuteInstruction(Chip8 *chip, uint16_t instruction);

/* Placeholder instruction */
void NOP(Chip8 *chip, uint16_t instruction);

/* Decode instructions with subfunctions */
void Chip8_DecodeOp0(Chip8 *chip, uint16_t instruction);
void Chip8_DecodeOp8(Chip8 *chip, uint16_t instruction);
void Chip8_DecodeOpE(Chip8 *chip, uint16_t instruction);
void Chip8_DecodeOpF(Chip8 *chip, uint16_t instruction);

/* Instructions */
void Chip8_Op1NNN  (Chip8 *chip, uint16_t instruction);
void Chip8_Op6XKK  (Chip8 *chip, uint16_t instruction);
void Chip8_OpANNN  (Chip8 *chip, uint16_t instruction);
void Chip8_Op8XY0  (Chip8 *chip, uint16_t instruction);
void Chip8_OpDXYN  (Chip8 *chip, uint16_t instruction);
void Chip8_Op7XKK  (Chip8 *chip, uint16_t instruction);
void Chip8_Op3XKK  (Chip8 *chip, uint16_t instruction);
void Chip8_Op4XKK  (Chip8 *chip, uint16_t instruction);
void Chip8_Op5XY0  (Chip8 *chip, uint16_t instruction);
void Chip8_Op2NNN  (Chip8 *chip, uint16_t instruction);
void Chip8_Op00E0  (Chip8 *chip, uint16_t instruction);
void Chip8_Op00EE  (Chip8 *chip, uint16_t instruction);

void Chip8_OpBNNN  (Chip8 *chip, uint16_t instruction);
void Chip8_OpCXKK  (Chip8 *chip, uint16_t instruction);

void Chip8_Op8XY1  (Chip8 *chip, uint16_t instruction);
void Chip8_Op8XY2  (Chip8 *chip, uint16_t instruction);
void Chip8_Op8XY3  (Chip8 *chip, uint16_t instruction);
void Chip8_Op8XY4  (Chip8 *chip, uint16_t instruction);
void Chip8_Op8XY5  (Chip8 *chip, uint16_t instruction);
void Chip8_Op8XY6  (Chip8 *chip, uint16_t instruction);
void Chip8_Op8XY7  (Chip8 *chip, uint16_t instruction);
void Chip8_Op8XYE  (Chip8 *chip, uint16_t instruction);

void Chip8_Op9XY0  (Chip8 *chip, uint16_t instruction);

void Chip8_OpEX9E  (Chip8 *chip, uint16_t instruction);
void Chip8_OpEXA1  (Chip8 *chip, uint16_t instruction);

void Chip8_OpFX07   (Chip8 *chip, uint16_t instruction);
void Chip8_OpFX0A   (Chip8 *chip, uint16_t instruction);
void Chip8_OpFX15   (Chip8 *chip, uint16_t instruction);
void Chip8_OpFX18   (Chip8 *chip, uint16_t instruction);
void Chip8_OpFX1E   (Chip8 *chip, uint16_t instruction);
void Chip8_OpFX29   (Chip8 *chip, uint16_t instruction);
void Chip8_OpFX33   (Chip8 *chip, uint16_t instruction);
void Chip8_OpFX55   (Chip8 *chip, uint16_t instruction);
void Chip8_OpFX65   (Chip8 *chip, uint16_t instruction);
void Chip8_OpFX75   (Chip8 *chip, uint16_t instruction);
void Chip8_OpFX85   (Chip8 *chip, uint16_t instruction);

/* Function array */
typedef void (* Chip8_OP)(Chip8 *, uint16_t);

/* 16 possible main op functions (0-F) */
static Chip8_OP ops[16] = {
    Chip8_DecodeOp0,
    Chip8_Op1NNN,
    Chip8_Op2NNN,
    Chip8_Op3XKK,
    Chip8_Op4XKK,
    Chip8_Op5XY0,
    Chip8_Op6XKK,
    Chip8_Op7XKK,
    Chip8_DecodeOp8,
    Chip8_Op9XY0,
    Chip8_OpANNN,
    Chip8_OpBNNN,
    Chip8_OpCXKK,
    Chip8_OpDXYN,
    Chip8_DecodeOpE,
    Chip8_DecodeOpF
};

/* 256 possible op 0 subfunctions (0x0 - 0xFF) */
static Chip8_OP ops_0[256] = {
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 0-9
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 10-19
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 20-29
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 30-39
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 40-49
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 50-59
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 60-69
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 70-79
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 80-89
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 90-99
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 100-109
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 110-119
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 120-129
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 130-139
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 140-149
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 150-159
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 160-169
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 170-179
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 180-189
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 190-199
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 200-209
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 210-219 
    NOP, NOP, NOP, NOP, Chip8_Op00E0, NOP, NOP, NOP, NOP, NOP, // 220-229 (E0 = 224)
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, Chip8_Op00EE, NOP, // 230-239 (EE = 238)
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 240-249
    NOP, NOP, NOP, NOP, NOP, NOP // 250-255
};

/* 16 possible op 8 subfunctions (0x0 - 0xF) */
static Chip8_OP ops_8[16] = {
    Chip8_Op8XY0, // 0x0
    Chip8_Op8XY1, // 0x1
    Chip8_Op8XY2, // 0x2
    Chip8_Op8XY3, // 0x3
    Chip8_Op8XY4, // 0x4
    Chip8_Op8XY5, // 0x5
    Chip8_Op8XY6, // 0x6
    Chip8_Op8XY7, // 0x7
    NOP,          // 0x8
    NOP,          // 0x9
    NOP,          // 0xA
    NOP,          // 0xB
    NOP,          // 0xC
    NOP,          // 0xD
    Chip8_Op8XYE, // 0xE
    NOP           // 0xF
};

/* 256 possible op 0 subfunctions (0x0 - 0xFF) */
static Chip8_OP ops_E[256] = {
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 0-9
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 10-19
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 20-29
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 30-39
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 40-49
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 50-59
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 60-69
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 70-79
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 80-89
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 90-99
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 100-109
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 110-119
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 120-129
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 130-139
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 140-149
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, Chip8_OpEX9E, NOP, // 150-159 (0x9E = 158)
    NOP, Chip8_OpEXA1, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 160-169 (0xA1 = 161)
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 170-179
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 180-189
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 190-199
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 200-209
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 210-219 
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 220-229
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 230-239
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 240-249
    NOP, NOP, NOP, NOP, NOP, NOP // 250-255
};

/* 256 possible op F subfunctions (0x0 - 0xFF) */
static Chip8_OP ops_F[256] = {
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, Chip8_OpFX07, NOP, NOP, // 0-9
    Chip8_OpFX0A,   NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 10-19
    NOP, Chip8_OpFX15, NOP, NOP, Chip8_OpFX18, NOP, NOP, NOP, NOP, NOP, // 20-29
    Chip8_OpFX1E,   NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 30-39
    NOP,   Chip8_OpFX29,   NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 40-49
    NOP,   Chip8_OpFX33,   NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 50-59
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 60-69
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 70-79
    NOP, NOP, NOP, NOP, NOP,   Chip8_OpFX55,   NOP, NOP, NOP, NOP, // 80-89
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 90-99
    NOP,   Chip8_OpFX65,   NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 100-109
    NOP, NOP, NOP, NOP, NOP, NOP, NOP,   Chip8_OpFX75,   NOP, NOP, // 110-119
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 120-129
    NOP, NOP, NOP,   Chip8_OpFX85,   NOP, NOP, NOP, NOP, NOP, NOP, // 130-139
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 140-149
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 150-159
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 160-169
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 170-179
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 180-189
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 190-199
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 200-209
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 210-219 
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 220-229
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 230-239
    NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, NOP, // 240-249
    NOP, NOP, NOP, NOP, NOP, NOP // 250-255
};

#endif
