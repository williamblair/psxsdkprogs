/* PSXSDK stuff */
#include <psx.h>

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>

#ifndef DISPLAY_H_INCLUDED
#define DISPLAY_H_INCLUDED

/* The Chip8 has a 64x32 resolution, while we're using a 
 * 320x240 resolution */
#define SCALE_X (320 / 64)
#define SCALE_Y (240 / 32)

/* Initialize the PSX system and graphics */
bool DisplayInit(void);

/* Update the screen, drawing pixels
 * based on the chip 8's screen memory */
bool DisplayUpdate(uint8_t *screen);

/* If displayIsOld, i.e. ready to draw */
bool DisplayCanDraw(void);

/* Draw the prim list, wait 
 * till done drawing */
bool DisplayVSync(void);

/* Draw a 'Pixel', relative to the size of the screen */
void DisplayDrawPixelRGB(int x, int y, int r, int g, int b);

/* Draw a rectangle, relative to the size of the screen */
void DisplayDrawRectRGB(int x, int y, int w, int h, int r, int g, int b);

/* Macro so it's easy to draw with 0 or 1 */
#define DisplayDrawPixel(x, y, color) \
    DisplayDrawPixelRGB((x), (y), (color), (color), (color))

#define DisplayDrawRect(x, y, w, h, color) \
    DisplayDrawRectRGB((x), (y), (w), (h), (color), (color), (color))

/* turn our needs update boolean to true */
void DisplaySetNeedsUpdate(void);

/* Clear the screen */
void DisplayClearScreen(void);

#endif

