#include "Pad.h"

static unsigned short padbuf;

bool PAD_IsPressed(int button)
{
    /* Get the current pad info and store in padbuf */
    PSX_ReadPad(&padbuf, NULL);

    return (padbuf & button);
}

bool PAD_IsClicked(int button)
{
    PSX_ReadPad(&padbuf, NULL);

    /* Wait for the button to be released
     * if it's currently held */
    if (padbuf & button) {
        while(padbuf & button) PSX_ReadPad(&padbuf, NULL);
    }
    else {
        return false;
    }

    return true;
}

uint16_t PAD_AnyPressed(void)
{
    PSX_ReadPad(&padbuf, NULL);
    
    //printf("padbuf: %d\n", padbuf);
    
    return padbuf;
}

uint8_t PAD_MapButton(uint16_t button)
{
    /* Return a value not in the chip8
     * keyboard by default */
    uint8_t chip8Button = 0xFF;
    switch (button)
    {
        case PAD_LEFT:  chip8Button = 0x4; break;
        case PAD_RIGHT: chip8Button = 0x6; break;
        case PAD_CROSS: chip8Button = 0x5; break;
        default:
            printf("Chip8_OpFX0A - unmapped PSX Button: 0x%X\n", button);
            break;
    }
    
    return chip8Button;
}