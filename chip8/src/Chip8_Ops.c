#include "Chip8.h"

/* Placeholder for function array */
void NOP(Chip8 *chip, uint16_t instruction)
{
    printf("Chip8 NOP!\n");
}

/* 1NNN - set the Program Counter to address NNN */
void Chip8_Op1NNN(Chip8 *chip, uint16_t instruction)
{
    /* Get the address to jump to */
    int addr = Instruction_NNN(instruction);

    /* Set the instruction address */
    chip->pc = addr;
}

/* 6XKK - Set register X with value KK */
void Chip8_Op6XKK(Chip8 *chip, uint16_t instruction)
{
    /* Get the register to write to */
    int reg = Instruction_X(instruction);

    /* Get the byte to write to the register */
    uint8_t val = Instruction_KK(instruction);

    /* Set the register */
    chip->registers[reg] = val;

    // DEBUG
    //printf("Chip8_Op6XKK: set register 0x%X to value 0x%X\n", reg, val);
}

/* ANNN - Set address I with value NNN */
void Chip8_OpANNN(Chip8 *chip, uint16_t instruction)
{
    /* Get the value to set Address I with */
    uint16_t val = Instruction_NNN(instruction);

    /* Set the value */
    chip->I = val;

    // DEBUG
    //printf("Chip8_OpANNN: set I to 0x%X\n", chip->I);
}

/* BNNN - Jump to NNN + V0 */
void Chip8_OpBNNN(Chip8 *chip, uint16_t instruction)
{
    /* Get NNN */
    uint16_t nnn = Instruction_NNN(instruction);
    
    /* Set the PC to NNN + V0 */
    chip->pc = nnn + chip->registers[0];
}

/* CXKK - VX = Random Number AND KK */
void Chip8_OpCXKK(Chip8 *chip, uint16_t instruction)
{
    /* Get the register to set */
    int reg = Instruction_X(instruction);
    
    /* Get the value to AND the random with */
    uint16_t kk = Instruction_KK(instruction);
    
    /* Set the register value to a random (0-255) & KK */
    chip->registers[reg] = (rand() % 255) & kk;
}

/* DXYN  - Draws a sprite with value based on 
 * address I, with screen coords from the register values at X,Y 
 * the Flag register (F) is set to 1 if any sprite is overwritten */
void Chip8_OpDXYN(Chip8 *chip, uint16_t instruction)
{
    /* Current X and Y when drawing */
    int i,j;

    /* Sprite dimensions */
    int sprWidth = 0, sprHeight = 0;

    /* Get the number of bytes to draw for the sprite 
     * if N = 0, sprite is size 16x16px, otherwise 8xN px */
    int sprSize = Instruction_N(instruction);
    if (sprSize == 0) {
        sprWidth = sprHeight = 16;
    }
    else {
        sprWidth = 8; sprHeight = sprSize;
    }

    /* Where in RAM to get the value to draw
     * to the screen */
    int addrI = chip->I;
    int yoffset = 0;

    /* Get the starting X and Y */
    int regX = Instruction_X(instruction);
    int regY = Instruction_Y(instruction);
    int x = chip->registers[regX];
    int y = chip->registers[regY];

#if 0
// DRW Vx, Vy, nibble
// Dxyn
// Display n-byte sprite starting at memory location I at (Vx, Vy), set VF equal to collision.
case 0xD000:
    this.v[0xF] = 0;

    var height = opcode & 0x000F;
    var registerX = this.v[x];
    var registerY = this.v[y];
    var x, y, spr;

    for (y = 0; y < height; y++) {
        spr = this.memory[this.i + y];
        for (x = 0; x < 8; x++) {
            if ((spr & 0x80) > 0) {
                if (this.setPixel(registerX + x, registerY + y)) {
                    this.v[0xF] = 1;
                }
            }
            spr <<= 1;
        }
    }
    this.drawFlag = true;
#endif

    /* Set values, check for collision */
    for (j = y; j < y + sprHeight; j++, yoffset++)
    {
        /* The Current data to draw to the screen */
        uint8_t data = chip->ram[addrI + yoffset];

        for (i = x; i < x + sprWidth; i++)
        {
            if ((data & 0x80) > 0) {
                
                /* What color the pixel will be */
                int color = 1;
                
                /* Screen wrapping */
                int wrappedX = i % CHIP8_SCREEN_WIDTH;
                if (wrappedX < i) {
                    j += 1;
                }

                /* Check For collision */
                if (GetPixel(chip, i, j)) {
                    chip->registers[0xF] = 1;
                    color = 0;
                }
                else {
                    chip->registers[0xF] = 0;
                }

                /* Set the Value in our internal screen memory */
                DrawPixelWithValue(chip, i, j, color);
                
                /* Draw a scaled rect representing a pixel on a normal chip8 screen */
                DisplayDrawRectRGB(i*SCALE_X, j*SCALE_Y, SCALE_X, SCALE_Y, color*255, color*255, color*255);
            }

            /* Offset/get the current pixel */
             data <<= 1;
        }
    }
    
    /* Tell the display we need to update the screen */
    DisplaySetNeedsUpdate();

#if 0
    // DEBUG
    //printf("Screen Mem:\n");
    for(i = 0; i < CHIP8_SCREEN_WIDTH; i++)
    {
        for(j = 0; j < CHIP8_SCREEN_HEIGHT; j++)
        {
            //printf("%X", GetPixel(chip, i, j));
        }
        //printf("\n");
    }
    //printf("\n");
#endif
}

/* 7XKK - adds KK to register X */
void Chip8_Op7XKK(Chip8 *chip, uint16_t instruction)
{
    /* Get the register */
    int reg = Instruction_X(instruction);

    /* Get the value to add */
    uint8_t val = Instruction_KK(instruction);

    /* Set the register value */
    chip->registers[reg] = chip->registers[reg] + val;
}

/* 3XKK - skip next instruction if register X
 * equals KK */
void Chip8_Op3XKK(Chip8 *chip, uint16_t instruction)
{
    /* Get the register value */
    int reg = Instruction_X(instruction);
    int val = chip->registers[reg];

    /* Skip the next instruction if val == kk */
    if (val == Instruction_KK(instruction)) {
        chip->pc += 2;

        // TEST
        //printf("Chip8_Sne: skipping instruction: 0x%X\n", Chip8_LoadInstruction(chip, chip->pc));
    }
}

/* Op 5XY0 - skip next instruction if VX == VY */
void Chip8_Op5XY0(Chip8 *chip, uint16_t instruction)
{
    /* Get VX and VY */
    int regX = chip->registers[Instruction_X(instruction)];
    int regY = chip->registers[Instruction_Y(instruction)];
    
    /* Skip next instruction if VX == VY */
    if (regX == regY) {
        chip->pc += 2;
        //printf("Chip8_Seq: skipping instruction: 0x%X\n", Chip8_LoadInstruction(chip, chip->pc));
    }
    
}

/* Op 4XKK - skip next instruction if VX != KK */
void Chip8_Op4XKK(Chip8 *chip, uint16_t instruction)
{
    /* Get the register value to check */
    int regX = chip->registers[Instruction_X(instruction)];
    
    /* Get the byte to check */
    int kk = Instruction_KK(instruction);
    
    /* Skip the next instruction if the values aren't equal */
    if (regX != kk) {
        chip->pc += 2;
        //printf("Chip8_Sneq: skipping instruction: 0x%X\n", Chip8_LoadInstruction(chip, chip->pc));
    }
}

/* Op 2NNN - call subroutine at NNN */
void Chip8_Op2NNN(Chip8 *chip, uint16_t instruction)
{
    /* What the PC will be set to */
    int addr = Instruction_NNN(instruction);

    /* Increment the stack pointer, then put the pc on top
     * of the stack */
    chip->stack[++(chip->stackPointer)] = chip->pc;

    /* Set the PC equal to NNN,
     * compensating for our automatic pc += 2 in the main loop */
    //chip->pc = addr - 2;
    chip->pc = addr;

    /* DEBUG */
    //int i;
    //printf("Chip8_Op2NNN: pc: 0x%X\n", chip->pc + 2);
    //for (i = 0; i< CHIP8_STACK_SIZE; i++)
    //{
        //printf("Stack %d: 0x%X\n", i, chip->stack[i]);
    //}
}

/* Op 8XY0 - Store the value at register Y into register X */
void Chip8_Op8XY0(Chip8 *chip, uint16_t instruction)
{
    /* Get the Y register value */
    int val = chip->registers[Instruction_Y(instruction)];

    /* Store the value in register x */
    chip->registers[Instruction_X(instruction)] = val;
}

/* Op FX65 - Memory locations starting at address I 
 * into registers V0-VX */
void Chip8_OpFX65(Chip8 *chip, uint16_t instruction)
{
    int i;
    
    /* Get the highest register to copy into */
    int highestReg = Instruction_X(instruction);
    
    /* Get the starting memory location */
    int memStart = chip->I;
    
    /*for (i = 0; i <= highestReg; i++)
    {
        chip->registers[i] = chip->ram[memStart + i];
    }*/
    memcpy(chip->registers, &(chip->ram[memStart]), highestReg+1);
    
    //printf("Sizeof uint8_t: %d\n", sizeof(uint8_t));
    
    // DEBUG
    //for(i = 0; i<= highestReg; i++)
    //{
        //printf("%d: mem: %d    reg: %d\n", i, chip->ram[memStart+i], chip->registers[i]);
    //}
}

/* 00E0 - clear the screen */
void Chip8_Op00E0(Chip8 *chip, uint16_t instruction)
{
    /* Fill the chip8 screen memory with zeros */
    memset(chip->screen, 0, CHIP8_SCREEN_WIDTH*CHIP8_SCREEN_HEIGHT);
    
    /* Tell the screen to clear */
    DisplayClearScreen();
    
    /* Tell the screen we need to update */
    DisplaySetNeedsUpdate();
}

/* 00EE - Return from a subroutine */
void Chip8_Op00EE(Chip8 *chip, uint16_t instruction)
{
    /* Set the PC to the address at the top of the stack,
     * then decrement the stack pointer by 1 */
    chip->pc = chip->stack[(chip->stackPointer)--];
    
    /* DEBUG */
    int i;
    //printf("Chip8_Op2NNN: pc: 0x%X\n", chip->pc + 2);
    //for (i = 0; i< CHIP8_STACK_SIZE; i++)
    //{
        //printf("Stack %d: 0x%X\n", i, chip->stack[i]);
    //}
    //printf("pc: 0x%X\n", chip->pc);
}

/* 8XY1 - VX = VX | VY */
void Chip8_Op8XY1   (Chip8 *chip, uint16_t instruction)
{
    /* Get the two values to or */
    int regX = chip->registers[Instruction_X(instruction)];
    int regY = chip->registers[Instruction_Y(instruction)];
    
    /* Or the values and set the register */
    chip->registers[Instruction_X(instruction)] = regX | regY;
}

/* 8XY2 - VX = VX AND VY */
void Chip8_Op8XY2  (Chip8 *chip, uint16_t instruction)
{
    /* Get the two values to and */
    int regX = chip->registers[Instruction_X(instruction)];
    int regY = chip->registers[Instruction_Y(instruction)];
    
    /* AND the values and set the register */
    chip->registers[Instruction_X(instruction)] = regX & regY;
}

/* 8XY3 - VX = VX XOR VY */
void Chip8_Op8XY3  (Chip8 *chip, uint16_t instruction)
{
    /* Get the two values to XOR */
    int regX = chip->registers[Instruction_X(instruction)];
    int regY = chip->registers[Instruction_Y(instruction)];
    
    /* AND the values and set the register */
    chip->registers[Instruction_X(instruction)] = regX ^ regY;
}

/* 8XY4 - VX = VX + VY, VF = carry - if the result > 8 bits (255), 
 * VF set to 1, otherwise 0. Only the lowest 8 bits of the result are 
 * kept, and stored in VX */
void Chip8_Op8XY4  (Chip8 *chip, uint16_t instruction)
{
    /* Get the two values to add */
    int regX = chip->registers[Instruction_X(instruction)];
    int regY = chip->registers[Instruction_Y(instruction)];
    
    /* Get the resulting addition */
    int result = regX + regY;
    
    /* If we're going to carry, set the flag register */
    chip->registers[0xF] = (result > 255);
    
    /* Set register X, chopping off any extra bits */
    chip->registers[Instruction_X(instruction)] = (result & 0xFF);
}

/* 8XY5 - VX = VX - VY, VF = NOT borrow
 * if VX > VY, then VF = 1, else 0 */
void Chip8_Op8XY5  (Chip8 *chip, uint16_t instruction)
{
    /* Get the two values to subtract */
    int regX = chip->registers[Instruction_X(instruction)];
    int regY = chip->registers[Instruction_Y(instruction)];
    
    /* Set the flag register */
    chip->registers[0xF] = (regX > regY);
    
    /* Store the subtraction in VX */
    chip->registers[Instruction_X(instruction)] = (uint8_t)(regX - regY);
}

/* 8XY6 - shift VX right by 1 (divide by 2) 
 * if the LSB of VX == 1, then VF = 1, else 0 */
void Chip8_Op8XY6  (Chip8 *chip, uint16_t instruction)
{
    /* Get the value to shift */
    int regX = chip->registers[Instruction_X(instruction)];
    
    /* Set the flag register if the LSB of X is 1*/
    chip->registers[0xF] = (regX & 0x1);
    
    /* Divide Vx by 2 / shift right 1 */
    chip->registers[Instruction_X(instruction)] = regX >> 1;
}

/* 8XY7 - VX = VY - VX, VF = NOT borrow 
 * if VY > VX, VF = 1, else 0 */
void Chip8_Op8XY7 (Chip8 *chip, uint16_t instruction)
{
    /* Get the values to subtract */
    int regX = chip->registers[Instruction_X(instruction)];
    int regY = chip->registers[Instruction_Y(instruction)];
    
    /* Set the flag register if NOT borrow */
    chip->registers[0xF] = (regY > regX);
    
    /* Store the difference in regX */
    chip->registers[Instruction_X(instruction)] = (uint8_t)(regY - regX);
}

/* 8XYE - VX = VX shifted left 1, VF = carry
 * if the MSB of VX = 1, then VF = 1, else 0,
 * then VF multiplied by 2 */
void Chip8_Op8XYE  (Chip8 *chip, uint16_t instruction)
{
    /* Get the value to shift */
    int regX = chip->registers[Instruction_X(instruction)];
    
    /* Set the flag if there will be loss (the MSB -> 8 bits -> 0b10000000) */
    chip->registers[0xF] = ((regX & 0x80) == 0x80);
    
    /* Set the shifted value, chopping off extra bits */
    chip->registers[Instruction_X(instruction)] = (regX << 1) & 0xFF;
}

/* 9XY0 - Skip next instruction if VX != VY */
void Chip8_Op9XY0(Chip8 *chip, uint16_t instruction)
{
    /* Get the two values to compare */
    int val1 = chip->registers[Instruction_X(instruction)];
    int val2 = chip->registers[Instruction_Y(instruction)];
    
    /* Skip next instruction if the two values aren't equal */
    if (val1 != val2) {
        chip->pc += 2;
    }
}

/* Op EX9E - Skip next instruction if the key with value
 * of register X IS pressed */
void Chip8_OpEX9E(Chip8 *chip, uint16_t instruction)
{
    /* Get the key to check if not pressed */
    int key = chip->registers[Instruction_X(instruction)];

    /* SNE if key IS pressed */
    if (chip->keys[key]) {
        chip->pc += 2;
        //printf("Chip8_OpEX9E: pc: 0x%X; skipping instruction 0x%X\n", chip->pc - 2, chip->pc);
    }
}

/* Op EXA1 - skip next instruction if key with value
 * of register X NOT pressed */
void Chip8_OpEXA1(Chip8 *chip, uint16_t instruction)
{
    /* Get the key to check if not pressed */
    int key = chip->registers[Instruction_X(instruction)];

    /* SNE if key NOT pressed */
    if (! chip->keys[key]) {
        chip->pc += 2;
        //printf("Chip8_OpEXA1: pc: 0x%X; skipping instruction 0x%X\n", chip->pc - 2, chip->pc);
    }
}

/* FX07 - VX = Delay Timer */
void Chip8_OpFX07   (Chip8 *chip, uint16_t instruction)
{
    /* Copy the delay timer value into VX */
    chip->registers[Instruction_X(instruction)] = chip->delay;
}

/* FX0A - Waits a keypress and stores it in VX */
void Chip8_OpFX0A   (Chip8 *chip, uint16_t instruction)
{
    /* Wait for any key to be pressed */
    uint16_t button = 0;
    do
    {
        button = PAD_AnyPressed();
        
    } while(button == 0);
    
    /* Map the button pressed to the chip8 button 
     * 
     * Space invaders controls: 
     * move: 4,6 shoot: 5 */
    uint8_t chip8Button = PAD_MapButton(button);
    
    /* Store the button in VX */
    if (chip8Button != 0xFF)
        chip->registers[Instruction_X(instruction)] = chip8Button;
}

/* FX15 - Set delay timer = VX */
void Chip8_OpFX15   (Chip8 *chip, uint16_t instruction)
{
    /* Get the value from register X */
    uint8_t val = chip->registers[Instruction_X(instruction)];
    
    /* Set the delay timer equal to the value */
    chip->delay = val;
}

/* FX18 - Sound Timer = VX */
void Chip8_OpFX18   (Chip8 *chip, uint16_t instruction)
{
    /* Get the value to use */
    uint8_t val = chip->registers[Instruction_X(instruction)];
    
    /* Set the sound timer */
    chip->sound = val;
}

/* FX1E - adds register X to Instruction Address */
void Chip8_OpFX1E(Chip8 *chip, uint16_t instruction)
{
    /* Get the Value to add */
    int reg = Instruction_X(instruction);

    /* Add the value to I */
    chip->I += chip->registers[reg];
}

/* FX29 - I points to the 4x5 font sprite of hex char 
 * in VX */
void Chip8_OpFX29   (Chip8 *chip, uint16_t instruction)
{
    /* Get the character to point to from VX */
    uint8_t val = chip->registers[Instruction_X(instruction)];
    
    /* Set the location 
     * 
     * each character uses 5 bytes of memory */
    chip->I = val * 5;
}

/* FX33 - store BCD representation of VX in M(I)...M(I+2) */
void Chip8_OpFX33   (Chip8 *chip, uint16_t instruction)
{
    /* Get the value in register X */
    int val = chip->registers[Instruction_X(instruction)];
    
    /* Store the 100s, 10s, and 1s in addresses I...I+2 */
    uint8_t I = chip->I;
    chip->ram[I+0] = val / 100;
    chip->ram[I+1] = (val / 10) % 10;
    chip->ram[I+2] = val % 10;
}

/* FX55 - Save V0-VX in memory starting at M(I) */
void Chip8_OpFX55   (Chip8 *chip, uint16_t instruction)
{
    /* Get how far to loop to */
    int numVals = Instruction_X(instruction);
    
    /* Store V0-VX in memory starting at M(I) */
    memcpy(&(chip->ram[chip->I]), chip->registers, numVals);
}

/*  The two below are for the SCHIP, unimplemented... */
void Chip8_OpFX75   (Chip8 *chip, uint16_t instruction)
{
    printf("Chip8_OpFX75 - Unimplemented!!!\n");
}
void Chip8_OpFX85   (Chip8 *chip, uint16_t instruction)
{
    printf("Chip8_OpFX85 - Unimplemented!!!\n");
}
