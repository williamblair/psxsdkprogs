/* Chip8 main file */
#include "Chip8.h"
#include "Pad.h"
#include "Display.h"
#include "Vector.h"

#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include <psx.h>

/* To be called in the drawing loop */
void drawFunc(void)
{
    //GsPrintFont(70, 120, "Op: 0x%X", op);
}

void readDirectory()
{
    int i;
    
    /* A vector to hold all of the roms on the CD */
    Vector gamesList;
    VectorInit(&gamesList);
    
    /* Directory structs are defined in psxbios.h */
    DIR *dp = NULL;
    struct dirent *ep = NULL;
    
    /* Open the ROM directory */
    dp = opendir("ROMS");
    
    /* Error check */
    if (!dp) {
        printf("Failed to open dir!\n");
        return;
    }
    
    /* Read all entries */
    while(ep = readdir(dp))
    {
        VectorAddItem(&gamesList, (void*)ep->d_name);
    }
    
    /* See what we got... */
    for (i = 0; i < gamesList.size; i++)
    {
        printf("Game: %s\n", VectorGetItem(&gamesList, i));
    }
    
    /* Free our vector */
    VectorFree(&gamesList);
}

int main(int argc, char *argv[])
{
    /* How many frames we've ran */
    int numRunFrames = 0;
    
    /* The current instruction */
    uint16_t instr = 0;

    /* The emulator data struct */
    Chip8 chip;

    /* Init the PSX and graphics */
    DisplayInit();

    /* Init memory */
    Chip8_Init(&chip);

    /* Load the ROM */
    //if (!Chip8_LoadRom(&chip, "cdrom:ROMS\\INVADERS.CH8;1")) return -1;

    //printf("Chip pc: 0x%X\n", chip.pc);

    /* Load the first OP! */
    //uint32_t op0 = Chip8_LoadOp(&chip, chip.pc);
    //printf("Loaded op: 0x%04X\n", op0);

    /*Vector v;
    VectorInit(&v);
    VectorAddItem(&v, "Hello World");
    VectorAddItem(&v, "Hello World 2");
    VectorAddItem(&v, "Hello World 3");
    
    int i;
    for (i = 0; i < v.size; i++)
    {
        printf("Vector item %d: %s\n", i, VectorGetItem(&v, i));
    }
    
    VectorFree(&v);*/
    
    readDirectory();

// vector test...
#if 0
    /* Main Loop */
    for(;;)
    {
        
        DisplayUpdate(chip.screen);

        //DisplayVSync();

        /* Get the operation */
        instr = Chip8_LoadInstruction(&chip, chip.pc);
        //printf("Loaded op: 0x%X\n", instr);
        
        /* Increment the program counter by the size
         * of an op */
        chip.pc += 2;
        
        /* Read the Pad and map it to the chip8 keyboard */
        uint16_t button = PAD_AnyPressed();
        if (button) {
            uint8_t chip8Button = PAD_MapButton(button);
            if (chip8Button != 0xFF)
                chip.keys[chip8Button] = 1;
        }

        if(!Chip8_ExecuteInstruction(&chip, instr))
            break;
            
        /* The delay timer counts down at roughly 60 times per sec... */
        if (((numRunFrames++) % 4 == 0) && chip.delay > 0) {
            //if (chip.delay < 2) chip.delay = 0;
            //else                chip.delay -= 1;
            chip.delay--;
        }
        if (chip.sound > 0) chip.sound--;

        /* Wait till button pressed */
        //while(!PAD_IsClicked(PAD_CROSS));
        
        /* All keys should be 0 by default */
        memset(chip.keys, 0, CHIP8_NUM_KEYS);
    }
#endif
    return 0;
}
