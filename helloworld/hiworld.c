/* Hello World Example
 * from here: https://github.com/zargener/psxsdkdoc/blob/master/psxsdk.adoc */

#include <psx.h>
#include <stdio.h>

unsigned int prim_list[0x4000];
volatile int display_is_old = 1;
volatile int time_counter = 0;
int dbuf = 0;

void prog_vblank_handler() {
    display_is_old = 1;
    time_counter++;
}

int main(void)
{
    /* Basic init */
    PSX_Init();
    GsInit();
    GsSetList(prim_list);
    GsClearMem();

    /* Set Video Mode */
    GsSetVideoMode(320, 240, VMODE_NTSC);

    /* Load bios Font */
    GsLoadFont(768, 0, 768, 256);

    /* Function to be called each time image
     * is sent to tv */
    SetVBlankHandler(prog_vblank_handler);

    /* main loop */
    for(;;)
    {
        if (display_is_old)
        {
            /* Switch display buffers */
            dbuf = !dbuf;

            /* Switch the drawing and display areas */
            GsSetDispEnvSimple(0, dbuf ? 0 : 256);
            GsSetDrawEnvSimple(0, dbuf ? 256 : 0, 320, 240);

            /* Print a message */
            GsPrintFont(70, 120, "Hello World!");

            /* Draw the primitive list */
            GsDrawList();
            while(GsIsDrawing());

            /* Set flag to prevent drawing until image
             * has been set on TV */
            display_is_old = 0;

            /* TEST - can I get pcsxr to do this? */
            printf("HELLO WORLD!\n");
        }
    }

    return 0;
}

