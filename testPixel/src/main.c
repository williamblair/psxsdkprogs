/* Chip8 main file */

#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include <psx.h>

/* To be called in the drawing loop */
void drawFunc(void)
{
    //GsPrintFont(70, 120, "Op: 0x%X", op);
}

uint8_t data[64*32];

int main(int argc, char *argv[])
{
    int i;

    /* Init the PSX and graphics */
    DisplayInit();

    memset(data, 0, sizeof(data));
    
    data[16*32 + 32] = 1;
    
    /* Cannot directly access VRAM - 
     * must be done through dma, with GPU
     * commands */
    GsRectangle rect;
    rect.r = 255; rect.g = 255; rect.b = 255;
    rect.x = 32; rect.y = 16;
    rect.w = 50; rect.h = 50;
    rect.attribute = 0;
    //GsSortDot(&dot);
    bool displayNeedsUpdate = true;
    
    int curBuf = 0;
    
    /* Main Loop */
    for(;;)
    {
        
        //DisplayUpdate();

        if (DisplayCanDraw()) {
        
            /* Switch display buffers */
            curBuf = !curBuf;

            /* Switch drawing and display area */
            //GsSetDispEnvSimple(0, curBuf ? 0 : 256);
            //GsSetDrawEnvSimple(0, curBuf ? 256 : 0, 320, 240);
            //GsSetDispEnvSimple(0, 0);
            //GsSetDrawEnvSimple(0, 0, 320, 240); // disabling double buffer?

            /* Clear the background with a color */
            //GsSortCls(0, 0, 0);

            /* Call the supplied drawing function */
            //func();
            
            /* Switch the dot color */
            rect.r = rect.g = rect.b = 255 * curBuf;
            
            GsSortRectangle(&rect);
            
            /* Draw the primitive list */
            GsDrawList();

            /* VSync */
            while(GsIsDrawing());

            /* Prevent redrawing until picture sent to TV */
            //displayIsOld = 0;
            DisplaySetIsOld();
            
            /* Delay... */
            for(i=0; i<100000; i++){}
        }
    }

    return 0;
}

