/* PSXSDK stuff 
 * much code tfrom:
 * https://github.com/zargener/psxsdkdoc/blob/master/psxsdk.adoc */
#include "Display.h"

// DEBUG
extern uint16_t op;

static unsigned int  primList[0x4000];
static volatile bool displayIsOld = true;
static volatile int  timeCounter = 0;
static          int  curBuf = 0;

/* Called when graphics are sent to the tv */
static void progVblankHandler(void) 
{
    displayIsOld = 1;
    timeCounter++;
}

bool DisplayInit(void)
{
    /* Basic system initialization */
    PSX_Init();
    GsInit();
    GsSetList(primList);
    GsClearMem();

    /* Set the Video Mode 
     * Cool bios memory check to see if we're on a PAL or NTSC console */
    int vmode = (*(char *)0xbfc7ff52=='E') ? VMODE_PAL : VMODE_NTSC;
    GsSetVideoMode(320, 240, vmode);

    /* Load the Bios Font 
     * The args correspond to the location to store the
     * font in the frame buffer */
    GsLoadFont(769, 0, 768, 256);

    /* Set the above function to be called
     * on image sent to the TV */
    SetVBlankHandler(progVblankHandler);
    
    // TEST
    GsSetDispEnvSimple(0, 0);
    GsSetDrawEnvSimple(0, 0, 320, 240); // disabling double buffer?

    return true;
}

//bool DisplayUpdate(void (*func)(void))
bool DisplayUpdate(void)
{
    if (displayIsOld) 
    {
        /* Switch display buffers */
        //curBuf = !curBuf;

        /* Switch drawing and display area */
        //GsSetDispEnvSimple(0, curBuf ? 0 : 256);
        //GsSetDrawEnvSimple(0, curBuf ? 256 : 0, 320, 240);
        //GsSetDispEnvSimple(0, 0);
        //GsSetDrawEnvSimple(0, 0, 320, 240); // disabling double buffer?

        /* Clear the background with a color */
        GsSortCls(0, 0, 0);

        /* Call the supplied drawing function */
        //func();

#if 0
        /* Draw the primitive list */
        GsDrawList();

        /* VSync */
        while(GsIsDrawing());

        /* Prevent redrawing until picture sent to TV */
        displayIsOld = 0;
#endif
    }
}

bool DisplayVSync(void)
{
    if (displayIsOld) {
        /* Draw the primitive list */
        GsDrawList();

        /* VSync */
        while(GsIsDrawing());

        /* Prevent redrawing until picture sent to TV */
        displayIsOld = 0;
    }
}

bool DisplayCanDraw(void)
{
    return displayIsOld;
}

void DisplaySetIsOld(void)
{
    displayIsOld = 1;
}
