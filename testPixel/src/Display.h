/* PSXSDK stuff */
#include <psx.h>

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>

#ifndef DISPLAY_H_INCLUDED
#define DISPLAY_H_INCLUDED

/* Initialize the PSX system and graphics */
bool DisplayInit(void);

/* Update the screen,
 * Calling the given function - intended 
 * for drawing stuff */
//bool DisplayUpdate(void (*func)(void));
bool DisplayUpdate(void);

/* If displayIsOld, i.e. ready to draw */
bool DisplayCanDraw(void);

/* Draw the prim list, wait 
 * till done drawing */
bool DisplayVSync(void);

void DisplaySetIsOld(void);

#endif

