#include <psx.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>

#ifndef PAD_H_INCLUDED
#define PAD_H_INCLUDED

/* Returns true if the given button is pressed */
bool PAD_IsPressed(int button);

/* Returns true if the button is clicked */
bool PAD_IsClicked(int button);

/* Returns true if any button in pressed */
uint16_t PAD_AnyPressed(void);

/* Map the PSX button to a chip8 button 
 * returns 0xFF if not mapped */
uint8_t PAD_MapButton(uint16_t button);

#endif

