/* PSXSDK stuff 
 * much code tfrom:
 * https://github.com/zargener/psxsdkdoc/blob/master/psxsdk.adoc */
#include "Display.h"

static unsigned int   primList[0x4000];
static volatile bool  displayIsOld = true;
static volatile int   timeCounter = 0;
static          int   curBuf = 0;

/* We only want to update the screen if needed
 * for performance and also keeping pixels on the screen */
static volatile bool  displayNeedsUpdate = true;

/* Called when graphics are sent to the tv */
static void progVblankHandler(void) 
{
    displayIsOld = 1;
    timeCounter++;
}

bool DisplayInit(void)
{
    /* Basic system initialization */
    PSX_Init();
    GsInit();
    GsSetList(primList);
    GsClearMem();

    /* Set the Video Mode 
     * Cool bios memory check to see if we're on a PAL or NTSC console */
    int vmode = (*(char *)0xbfc7ff52=='E') ? VMODE_PAL : VMODE_NTSC;
    GsSetVideoMode(320, 240, vmode);

    /* Load the Bios Font 
     * The args correspond to the location to store the
     * font in the frame buffer */
    GsLoadFont(769, 0, 768, 256);

    /* Set the above function to be called
     * on image sent to the TV */
    SetVBlankHandler(progVblankHandler);
    
    /* We're using a single buffer at the moment,
     * which makes only updating the screen when we want
     * (as opposed to every frame) easier */
    GsSetDispEnvSimple(0, 0);
    GsSetDrawEnvSimple(0, 0, 320, 240);

    return true;
}

/* Send the chip 8's screen memory to draw */
bool DisplayUpdate(uint8_t *screen)
{   
    if (displayIsOld && displayNeedsUpdate) 
    {
        //printf("In display needs update!\n");
        
        int i, j;
      
/* We're not double buffering at the moment */      
#if 0
        /* Switch display buffers */
        curBuf = !curBuf;

        /* Switch drawing and display area */
        GsSetDispEnvSimple(0, curBuf ? 0 : 256);
        GsSetDrawEnvSimple(0, curBuf ? 256 : 0, 320, 240);
#endif

// TESTING not clearing the screen...
#if 0
        /* Clear the background with a color */
        GsSortCls(0, 0, 0);
        
        /* DRAW HERE! */
        for (i = 0; i < 64; i++) 
        {
            for (j = 0; j < 32; j++)
            {
                /* If there's a pixel there draw it, scaling to fit the
                 * size of the screen */
                if (screen[j*64 + i]) {
                    DisplayDrawRect(i*SCALE_X, j*SCALE_Y, SCALE_X, SCALE_Y, 255);
                }
            }
        }
#endif
        
        /* Draw the primitive list */
        GsDrawList();

        /* VSync */
        while(GsIsDrawing());

        /* Prevent redrawing until picture sent to TV */
        displayIsOld = 0;
        
        /* We've drawn so no need to update the screen */
        displayNeedsUpdate = false;
    }
}

bool DisplayVSync(void)
{
    if (displayIsOld && displayNeedsUpdate) {
        /* Draw the primitive list */
        GsDrawList();

        /* VSync */
        while(GsIsDrawing());

        /* Prevent redrawing until picture sent to TV */
        displayIsOld = 0;
        
        displayNeedsUpdate = false;
    }
}

bool DisplayCanDraw(void)
{
    return displayIsOld;
}

/* Draw a 'Pixel', relative to the size of the screen */
void DisplayDrawPixelRGB(int x, int y, int r, int g, int b)
{
    /* Test Single Pixels for now ... */
    GsDot dot;
    dot.r = r; dot.g = g; dot.b = b;
    dot.x = x; dot.y = y;
    dot.attribute = 0;
    GsSortDot(&dot);
    //displayNeedsUpdate = true;
}

void DisplayDrawRectRGB(int x, int y, int w, int h, int r, int g, int b)
{
    GsRectangle rectangle;
    rectangle.r = r; rectangle.g = g; rectangle.b = b;
    rectangle.x = x; rectangle.y = y;
    rectangle.w = w; rectangle.h = h;
    rectangle.attribute = 0;
    GsSortRectangle(&rectangle);
}

void DisplaySetNeedsUpdate(void)
{
    displayNeedsUpdate = true;
}

void DisplayClearScreen(void)
{
    /* Clear the background with a color */
    GsSortCls(0, 0, 0);
}