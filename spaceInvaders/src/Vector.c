#include "Vector.h"

void VectorInit(Vector *vector)
{
    vector->size = 0;
    vector->items = NULL;
}

void VectorAddItem(Vector *vector, void *item)
{
    LinkedList *lastItem; 
    
    /* Increase the vector size */
    vector->size++;
    
    /* Create a new linked list item to add the to vector */
    LinkedList *newItem = (LinkedList *) malloc(sizeof(LinkedList));
    
    /* Set the new linked list values */
    newItem->item = item;
    newItem->next = NULL;
    
    /* See if this is the first entry in the list */
    if (vector->items == NULL) {
        vector->items = newItem;
        return;
    }
    
    /* Get the current pointer to the last linked list entry */
    for (lastItem = vector->items; lastItem->next != NULL; lastItem = lastItem->next) {}
    
    /* Add the new item to the linked list */
    lastItem->next = newItem;
}

void *VectorGetItem(Vector *vector, int index)
{
    LinkedList *listEntry = vector->items;
    int i;
    
    for(i = 1; i <= index; i++)
    {
        listEntry = listEntry->next;
    }
    
    return listEntry->item;
}

void VectorFree(Vector *vector)
{
    LinkedList *listEntry = vector->items;
    LinkedList *temp;
    
    while(listEntry != NULL)
    {
        temp = listEntry->next;
        free(listEntry);
        listEntry = temp;
    }
    
    vector->items = NULL;
}
