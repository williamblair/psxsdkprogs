#include "8080.h"

/* Init the CPU struct */
void Init_8080(Cpu8080 *cpu)
{
	/* Set pointers/register to 0 */
	cpu->pc = 0;
	cpu->sp = 0;
	
	cpu->a = cpu->psw = 0;
	cpu->b = cpu->c = 0;
	cpu->d = cpu->e = 0;
	cpu->h = cpu->l = 0;
    
    memset(cpu->sr, 0, sizeof(cpu->sr));
	
	/* Copy the ROMs into memory */
	memcpy(&cpu->memory[ROM_H_START], ROM_H, ROM_H_SIZE);
	memcpy(&cpu->memory[ROM_G_START], ROM_G, ROM_G_SIZE);
	memcpy(&cpu->memory[ROM_F_START], ROM_F, ROM_F_SIZE);
	memcpy(&cpu->memory[ROM_E_START], ROM_E, ROM_E_SIZE);

#if 0
	// test
	int i;
	printf("ROM H:\n");
	for(i=0; i<ROM_G_START; i++)
	{
		printf("%02X ", cpu->memory[i]);
		if ((i+1)%16 == 0) {
			printf("\n");
		}
	}
	printf("\n");
	printf("ROM G:\n");
	for(i=ROM_G_START; i<ROM_F_START; i++)
	{
		printf("%02X ", cpu->memory[i]);
		if ((i+1)%16 == 0) {
			printf("\n");
		}
	}
	printf("\n");
	printf("ROM F:\n");
	for(i=ROM_F_START; i<ROM_E_START; i++)
	{
		printf("%02X ", cpu->memory[i]);
		if ((i+1)%16 == 0) {
			printf("\n");
		}
	}
	printf("\n");
	printf("ROM E:\n");
	for(i=ROM_E_START; i<ROM_E_START+ROM_E_SIZE; i++)
	{
		printf("%02X ", cpu->memory[i]);
		if ((i+1)%16 == 0) {
			printf("\n");
		}
	}
	printf("\n");
#endif

}

void Reset_8080(Cpu8080 *cpu)
{
    /* Set pointers/register to 0 */
	cpu->pc = 0;
	cpu->sp = 0;
	
	cpu->a = cpu->psw = 0;
	cpu->b = cpu->c = 0;
	cpu->d = cpu->e = 0;
	cpu->h = cpu->l = 0;
    
    memset(cpu->sr, 0, sizeof(cpu->sr));
	
	/* Clear memory */
	memset(cpu->memory, 0, sizeof(cpu->memory));
	
	/* Copy the ROMs into memory */
	memcpy(&cpu->memory[ROM_H_START], ROM_H, ROM_H_SIZE);
	memcpy(&cpu->memory[ROM_G_START], ROM_G, ROM_G_SIZE);
	memcpy(&cpu->memory[ROM_F_START], ROM_F, ROM_F_SIZE);
	memcpy(&cpu->memory[ROM_E_START], ROM_E, ROM_E_SIZE);
}

/* Get the next instruction from memory, run it, and
 * increment the PC  */
bool  RunNextInstruction_8080(Cpu8080 *cpu)
{
	/* Get the next opcode */
	uint8_t op = cpu->memory[cpu->pc];
	
	/* Run it */
	bool result = ops[op](cpu);
	
	/* Increment the PC */
	cpu->pc++;
    
    return result;
}

/* Placeholder instruction! */
bool dummy(Cpu8080 *cpu)
{
	printf("Unhandled instruction: pc: 0x%X\n", cpu->pc);
	return false;
}

/* Returns 1 if the number of 1s is odd 
 * from https://www.microchip.com/forums/m587239.aspx */
uint8_t getOddParity(uint8_t num)
{
    num ^= (num >> 4);
    num ^= (num >> 2);
    num ^= (num >> 1);
    
    return num & 1;
}

/* NOP (No op) - no operation is performed 
 * Cycles: 1, States: 4, Flags: none */
bool NOP_8080(Cpu8080 *cpu)
{
	printf("NOP!\n");
    return true;
}

/* STAX B - the accumulator value is stored in memory
 * location given by register pair (BC) 
 * 
 * cycles: 2
 * states: 7 */
bool STAX_B_8080(Cpu8080 *cpu)
{
    /* Get the accumulator value */
    uint8_t accum = cpu->a;
    
    /* Store it in memory location (BC) */
    cpu->memory[(cpu->b << 8) | cpu->c] = accum;
    
    return true;
}

/* INX B - register pair BC is incremented by 1
 * no flags are set 
 *
 * cycles: 1
 * states: 5 */
bool INX_B_8080(Cpu8080 *cpu)
{
    /* Get the current register value  */
    uint16_t curVal = (cpu->b << 8) | cpu->c;

    /* Get the new value */
    uint16_t newVal = curVal + 1;

    /* Split the new value between the two registers */
    cpu->b = newVal >> 8;
    cpu->c = newVal & 0xFF;

    return true;
}

/* INR B - increment the given register by 1 
 *
 * Z,S,P,AC flags are set (NOT the carry flag) 
 * 
 * cycles: 1
 * states: 5 */
bool INR_B_8080(Cpu8080 *cpu)
{
    /* Get the current register value */
    uint8_t reg = cpu->b;

    /* Get the resulting value */
    uint8_t result = reg + 1;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
     
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the new register value */
    cpu->b = result;

    return true;
}


/* DCR B - decrement the given register by 1
 * 
 * all flags except carry set
 *
 * cycles: 5
 * states: */
bool DCR_B_8080(Cpu8080 *cpu)
{
    /* Get the current value of B */
    uint8_t val = cpu->b;
    
    /* Get the result of the subraction */
    uint8_t result = val - 1;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
     
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Update the register with the new value */
    cpu->b = result;
    
    return true;
}

/* RLC - shift the accumulator register left 1,
 * and move the previously last bit into the new
 * value's first bit 
 *
 * only the carry flag is set
 *
 * cycles: 4
 * states: */
bool RLC_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;
    
    /* Get the resulting value */
    uint8_t result = accum << 1;
    
    /* Set the results first bit as the old
     * accum's last bit */
    result |= (accum >> 7);
    
    /* Set the carry flag */
    cpu->sr[SR_FLAG_C] = accum >> 7;
    
    /* Set the accumulator as the result */
    cpu->a = result;
    
    return true;
}

/* DAD registerPair - (HL) += the given register pair 
 * 
 * Only the carry flag is affected
 * 
 * Cycles: 
 * States: */
bool DAD_B_8080(Cpu8080 *cpu)
{
    /* Get the current HL register pair value */
    uint16_t curVal = (cpu->h << 8) | cpu->l;

    /* Get the register pair value to add */
    uint16_t addVal = (cpu->b << 8) | cpu->c;

    /* Get the result */
    uint16_t result = curVal + addVal;

    /* Check for overflow */
    cpu->sr[SR_FLAG_C] = ((0xFFFF - curVal) < addVal);

    /* Set the new register pair */
    cpu->h = result >> 8;
    cpu->l = result & 0xFF;

    return true;
}

/* LDAX B - the memory value at the given register
 * pair is loaded into the accumulator 
 * 
 * no flags are affected 
 * 
 * Cycles: 
 * States: */
bool LDAX_B_8080(Cpu8080 *cpu)
{
    /* Get the memory value to store in A */
    uint8_t val = cpu->memory[(cpu->b << 8) | cpu->c];

    /* Store the value in A */
    cpu->a = val;

    return true;
}

/* DCX B - the given register pair is decremented by 1 
 * 
 * No flags are affected 
 *
 * Cycles: 
 * States: */
bool DCX_B_8080(Cpu8080 *cpu)
{
    /* Get the current pair value */
    uint16_t curVal = (cpu->b << 8) | cpu->c;

    /* Calculate the new value */
    uint16_t newVal = curVal - 1;

    /* Store it back into the registers */
    cpu->b = newVal >> 8;
    cpu->c = newVal & 0xFF;

    return true;
}

/* INR C - increment the C register by 1 
 * 
 * Every flag except carray affected 
 *
 * Cycles: 
 * States: */
bool INR_C_8080(Cpu8080 *cpu)
{
    /* Get the resulting value */
    uint8_t result = cpu->c+1;

    /* Set the zero flag */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* Set the parity flag */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Update the register */
    cpu->c = result;

    return true;
}

/* DCR C - decrement the given register by 1
 * 
 * all flags except carry set
 *
 * cycles: 5
 * states: */
bool DCR_C_8080(Cpu8080 *cpu)
{
    /* Get the current value of C */
    uint8_t val = cpu->c;
    
    /* Get the result of the subraction */
    uint8_t result = val - 1;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
     
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Update the register with the new value */
    cpu->c = result;
    
    return true;
}

/* MVI r, data - Move immediate 
 * byte 2 is stored in register r, in this case register C
 * cycles: 
 * states:  */
bool MVI_C_8080(Cpu8080 *cpu)
{
    /* Get byte 2 */
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    
    /* Store it in register C */
    cpu->c = byte2;
    
    /* a 2 byte instruction, so add the used byte to the pc */
    cpu->pc++;
    
    return true;
}

/* RRC - shift the accumulator register right 1,
 * and move the previously first bit into the new
 * value's last bit 
 *
 * only the carry flag is set
 *
 * cycles: 4
 * states: */
bool RRC_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;
    
    /* Get the resulting value */
    uint8_t result = accum >> 1;
    
    /* Set the results last bit as the old
     * accum's first bit */
    result |= (accum & 1) << 7;
    
    /* Set the carry flag */
    cpu->sr[SR_FLAG_C] = accum & 1;
    
    /* Set the accumulator as the result */
    cpu->a = result;
    
    return true;
}

/* LXI D, data 16 - load register pair immedite,
 * This time into the registry pair D,E
 * 
 * cycles: 3
 * states: 10 */
bool LXI_D_8080(Cpu8080* cpu)
{
    /* Get the two bytes */
    uint8_t byte3 = cpu->memory[cpu->pc+2];
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    
    /* Store them in the register pair */
    cpu->d = byte3;
    cpu->e = byte2;
    
    /* a 3 byte instruction */
    cpu->pc += 2;
    
    return true;
}

/* STAX D - the accumulator value is stored in memory
 * location given by register pair (DE) 
 * 
 * cycles: 2
 * states: 7 */
bool STAX_D_8080(Cpu8080 *cpu)
{
    /* Get the accumulator value */
    uint8_t accum = cpu->a;
    
    /* Store it in memory location (DE) */
    cpu->memory[(cpu->d << 8) | cpu->e] = accum;
    
    return true;
}

/* INX D - register pair DE is incremented by 1
 * no flags are set 
 *
 * cycles: 1
 * states: 5 */
bool INX_D_8080(Cpu8080 *cpu)
{
    /* Get the current register value  */
    uint16_t curVal = (cpu->d << 8) | cpu->e;

    /* Get the new value */
    uint16_t newVal = curVal + 1;

    /* Split the new value between the two registers */
    cpu->d = newVal >> 8;
    cpu->e = newVal & 0xFF;

    return true;
}

/* INR D - increment the given register by 1 
 *
 * Z,S,P,AC flags are set (NOT the carry flag) 
 * 
 * cycles: 1
 * states: 5 */
bool INR_D_8080(Cpu8080 *cpu)
{
    /* Get the current register value */
    uint8_t reg = cpu->d;

    /* Get the resulting value */
    uint8_t result = reg + 1;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
     
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the new register value */
    cpu->d = result;

    return true;
}

/* DCR D - decrement the given register by 1
 * 
 * all flags except carry set
 *
 * cycles: 5
 * states: */
bool DCR_D_8080(Cpu8080 *cpu)
{
    /* Get the current value of D */
    uint8_t val = cpu->d;
    
    /* Get the result of the subraction */
    uint8_t result = val - 1;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
     
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Update the register with the new value */
    cpu->d = result;
    
    return true;
}

/* MVI r, data - Move immediate 
 * byte 2 is stored in register r, in this case register D
 * cycles: 
 * states:  */
bool MVI_D_8080(Cpu8080 *cpu)
{
    /* Get byte 2 */
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    
    /* Store it in register D */
    cpu->d = byte2;
    
    /* a 2 byte instruction, so add the used byte to the pc */
    cpu->pc++;
    
    return true;
}

/* RAL - shift the accumulator register left 1,
 * bit 0 is set as the carry flag, the carry flag
 * is set as the accumulator's previous bit 7 
 *
 * only the carry flag is set
 *
 * cycles: 4
 * states: */
bool RAL_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;
    
    /* Get the resulting value */
    uint8_t result = accum << 1;
    
    /* Set the results first bit as the carry flag */
    result |= cpu->sr[SR_FLAG_C] & 1;
    
    /* Set the carry flag as the previous
     * accumulator's bit 7 */
    cpu->sr[SR_FLAG_C] = accum >> 7;
    
    /* Set the accumulator as the result */
    cpu->a = result;
    
    return true;
}

/* DAD registerPair - (HL) += the given register pair 
 * 
 * Only the carry flag is affected
 * 
 * Cycles: 
 * States: */
bool DAD_D_8080(Cpu8080 *cpu)
{
    /* Get the current HL register pair value */
    uint16_t curVal = (cpu->h << 8) | cpu->l;

    /* Get the register pair value to add */
    uint16_t addVal = (cpu->d << 8) | cpu->e;

    /* Get the result */
    uint16_t result = curVal + addVal;

    /* Check for overflow */
    cpu->sr[SR_FLAG_C] = ((0xFFFF - curVal) < addVal);

    /* Set the new register pair */
    cpu->h = result >> 8;
    cpu->l = result & 0xFF;

    return true;
}

/* LDAX D - the memory value at the given register
 * pair is loaded into the accumulator 
 * 
 * no flags are affected 
 * 
 * Cycles: 
 * States: */
bool LDAX_D_8080(Cpu8080 *cpu)
{
    /* Get the memory value to store in A */
    uint8_t val = cpu->memory[(cpu->d << 8) | cpu->e];

    /* Store the value in A */
    cpu->a = val;

    return true;
}

/* DCX D - the given register pair is decremented by 1 
 * 
 * No flags are affected 
 *
 * Cycles: 
 * States: */
bool DCX_D_8080(Cpu8080 *cpu)
{
    /* Get the current pair value */
    uint16_t curVal = (cpu->d << 8) | cpu->e;

    /* Calculate the new value */
    uint16_t newVal = curVal - 1;

    /* Store it back into the registers */
    cpu->d = newVal >> 8;
    cpu->e = newVal & 0xFF;

    return true;
}

/* INR E - increment the given register by 1 
 *
 * Z,S,P,AC flags are set (NOT the carry flag) 
 * 
 * cycles: 1
 * states: 5 */
bool INR_E_8080(Cpu8080 *cpu)
{
    /* Get the current register value */
    uint8_t reg = cpu->e;

    /* Get the resulting value */
    uint8_t result = reg + 1;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
     
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the new register value */
    cpu->e = result;

    return true;
}

/* DCR E - decrement the given register by 1
 * 
 * all flags except carry set
 *
 * cycles: 5
 * states: */
bool DCR_E_8080(Cpu8080 *cpu)
{
    /* Get the current value of E */
    uint8_t val = cpu->e;
    
    /* Get the result of the subraction */
    uint8_t result = val - 1;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
     
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Update the register with the new value */
    cpu->e = result;
    
    return true;
}

/* MVI r, data - Move immediate 
 * byte 2 is stored in register r, in this case register E
 * cycles: 
 * states:  */
bool MVI_E_8080(Cpu8080 *cpu)
{
    /* Get byte 2 */
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    
    /* Store it in register E */
    cpu->e = byte2;
    
    /* a 2 byte instruction, so add the used byte to the pc */
    cpu->pc++;
    
    return true;
}

/* RAR - shift the accumulator register right 1,
 * bit 7 is set as the previous bit 7 (same?), the carry flag
 * is set as the accumulator's previous bit 0
 *
 * only the carry flag is set
 *
 * cycles: 4
 * states: */
bool RAR_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;
    
    /* Get the resulting value */
    uint8_t result = accum >> 1;
    
    /* Set the results 7th bit as the prev 7th bit */
    result |= accum & 0x80;
    
    /* Set the carry flag as the previous
     * accumulator's bit 0 */
    cpu->sr[SR_FLAG_C] = accum & 1;
    
    /* Set the accumulator as the result */
    cpu->a = result;
    
    return true;
}

/* LXI H, data 16 - load register pair immedite,
 * This time into the registry pair H,L
 * 
 * cycles: 3
 * states: 10 */
bool LXI_H_8080(Cpu8080* cpu)
{
    /* Get the two bytes */
    uint8_t byte3 = cpu->memory[cpu->pc+2];
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    
    /* Store them in the register pair */
    cpu->h = byte3;
    cpu->l = byte2;
    
    /* a 3 byte instruction */
    cpu->pc += 2;
    
    return true;
}

/* SHLD adr - loads the HL register pair value into the 
 * given memory address 
 *
 * cycles: 3
 * states: 10 */
bool SHLD_8080(Cpu8080 *cpu)
{
    /* Get the address value */
    uint8_t byte3 = cpu->memory[cpu->pc + 2];
    uint8_t byte2 = cpu->memory[cpu->pc + 1];
    uint16_t addr = (byte3 << 8) | byte2;

    /* Set the values in memory */
    cpu->memory[addr] = cpu->l;
    cpu->memory[addr+1] = cpu->h;

    return true;
}

/* INX H - register pair HL is incremented by 1
 * no flags are set 
 *
 * cycles: 1
 * states: 5 */
bool INX_H_8080(Cpu8080 *cpu)
{
    /* Get the current register value  */
    uint16_t curVal = (cpu->h << 8) | cpu->l;

    /* Get the new value */
    uint16_t newVal = curVal + 1;

    /* Split the new value between the two registers */
    cpu->h = newVal >> 8;
    cpu->l = newVal & 0xFF;

    return true;
}

/* INR H - increment the given register by 1 
 *
 * Z,S,P,AC flags are set (NOT the carry flag) 
 * 
 * cycles: 1
 * states: 5 */
bool INR_H_8080(Cpu8080 *cpu)
{
    /* Get the current register value */
    uint8_t reg = cpu->h;

    /* Get the resulting value */
    uint8_t result = reg + 1;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
     
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the new register value */
    cpu->h = result;

    return true;
}

/* DCR H - decrement the given register by 1
 * 
 * all flags except carry set
 *
 * cycles: 5
 * states: */
bool DCR_H_8080(Cpu8080 *cpu)
{
    /* Get the current value of D */
    uint8_t val = cpu->h;
    
    /* Get the result of the subraction */
    uint8_t result = val - 1;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
     
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Update the register with the new value */
    cpu->h = result;
    
    return true;
}

/* MVI r, data - Move immediate 
 * byte 2 is stored in register r, in this case register D
 * cycles: 
 * states:  */
bool MVI_H_8080(Cpu8080 *cpu)
{
    /* Get byte 2 */
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    
    /* Store it in register D */
    cpu->h = byte2;
    
    /* a 2 byte instruction, so add the used byte to the pc */
    cpu->pc++;
    
    return true;
}

/* DAD registerPair - (HL) += the given register pair 
 * 
 * Only the carry flag is affected
 * 
 * Cycles: 
 * States: */
bool DAD_H_8080(Cpu8080 *cpu)
{
    /* Get the current HL register pair value */
    uint16_t curVal = (cpu->h << 8) | cpu->l;

    /* Get the register pair value to add */
    uint16_t addVal = (cpu->h << 8) | cpu->l;

    /* Get the result */
    uint16_t result = curVal + addVal;

    /* Check for overflow */
    cpu->sr[SR_FLAG_C] = ((0xFFFF - curVal) < addVal);

    /* Set the new register pair */
    cpu->h = result >> 8;
    cpu->l = result & 0xFF;

    return true;
}

/* LHLD - Load H and L direct - stores memory at the given address in L 
 * and the next address in H 
 *
 * No flags are affected
 *
 * Cycles: 5
 * States: 16 */
bool LHLD_8080(Cpu8080 *cpu)
{
    /* Get the address to read memory from */
    uint8_t byte3 = cpu->memory[cpu->pc+2];
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    uint16_t addr = (byte3 << 8) | byte2;

    /* Store the value at the given address in L */
    cpu->l = cpu->memory[addr];

    /* Store the value at the next address in H */
    cpu->h = cpu->memory[addr+1];

    /* 3 byte instruction */
    cpu->pc += 2;

    return true;
}

/* DCX H - the given register pair is decremented by 1 
 * 
 * No flags are affected 
 *
 * Cycles: 
 * States: */
bool DCX_H_8080(Cpu8080 *cpu)
{
    /* Get the current pair value */
    uint16_t curVal = (cpu->h << 8) | cpu->l;

    /* Calculate the new value */
    uint16_t newVal = curVal - 1;

    /* Store it back into the registers */
    cpu->h = newVal >> 8;
    cpu->l = newVal & 0xFF;

    return true;
}

/* INR H - increment the given register by 1 
 *
 * Z,S,P,AC flags are set (NOT the carry flag) 
 * 
 * cycles: 1
 * states: 5 */
bool INR_L_8080(Cpu8080 *cpu)
{
    /* Get the current register value */
    uint8_t reg = cpu->l;

    /* Get the resulting value */
    uint8_t result = reg + 1;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
     
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the new register value */
    cpu->l = result;

    return true;
}

/* DCR L - decrement the given register by 1
 * 
 * all flags except carry set
 *
 * cycles: 5
 * states: */
bool DCR_L_8080(Cpu8080 *cpu)
{
    /* Get the current value of L */
    uint8_t val = cpu->l;
    
    /* Get the result of the subraction */
    uint8_t result = val - 1;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
     
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Update the register with the new value */
    cpu->l = result;
    
    return true;
}
/* MVI r, data - Move immediate 
 * byte 2 is stored in register r, in this case register L
 * cycles: 
 * states:  */
bool MVI_L_8080(Cpu8080 *cpu)
{
    /* Get byte 2 */
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    
    /* Store it in register L */
    cpu->l = byte2;
    
    /* a 2 byte instruction, so add the used byte to the pc */
    cpu->pc++;
    
    return true;
}

/* CMA - Complement Accumulator - the accumulator is negated 
 * 
 * no flags are affected 
 *
 * Cycles: 1
 * States: 4 */
bool CMA_8080(Cpu8080 *cpu)
{
    /* Negate the accumulator */
    cpu->a = !cpu->a;

    return true;
}

/* ADC - Add register with carry 
 * 
 * All flags are affected 
 *
 * Cycles: 1
 * States: 4 */
bool ADC_B_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint8_t result = accum + cpu->b + cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result < accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ADC - Add register with carry 
 * 
 * All flags are affected 
 *
 * Cycles: 1
 * States: 4 */
bool ADC_C_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint8_t result = accum + cpu->c + cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result < accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
    
    return true;
}

/* ADC - Add register with carry 
 * 
 * All flags are affected 
 *
 * Cycles: 1
 * States: 4 */
bool ADC_D_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint8_t result = accum + cpu->d + cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result < accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ADC - Add register with carry 
 * 
 * All flags are affected 
 *
 * Cycles: 1
 * States: 4 */
bool ADC_E_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint8_t result = accum + cpu->e + cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result < accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ADC - Add register with carry 
 * 
 * All flags are affected 
 *
 * Cycles: 1
 * States: 4 */
bool ADC_H_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint8_t result = accum + cpu->h + cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result < accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ADC - Add register with carry 
 * 
 * All flags are affected 
 *
 * Cycles: 1
 * States: 4 */
bool ADC_L_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint8_t result = accum + cpu->l + cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result < accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ADC - Add register with carry 
 * 
 * All flags are affected 
 *
 * Cycles: 1
 * States: 4 */
bool ADC_M_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint16_t addr = (cpu->h << 8) | cpu->l;
    uint8_t result = accum + cpu->memory[addr] + cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result < accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ADC - Add register with carry 
 * 
 * All flags are affected 
 *
 * Cycles: 1
 * States: 4 */
bool ADC_A_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint8_t result = accum + cpu->a + cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result < accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ADD A - add the contents of register a (accumulator) to a 
 * cycles: 1 
 * states: 4 */
bool ADD_A_8080(Cpu8080 *cpu)
{
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* get the result of the addition */
    uint8_t result = accum+accum;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = ((0xFF - accum) < accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
    
    return true;
}


/* ADD B - add the contents of register B to the accumulator (register A) 
 * cycles: 1 
 * states: 4 */
bool ADD_B_8080(Cpu8080 *cpu)
{
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get the value from register B */
    uint8_t val = cpu->b;
    
    /* get the result of the addition */
    uint8_t result = accum + val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = ((0xFF - accum) < val);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
    
    return true;
}

/* ADD C - same as above but with register C 
 * cycles: 1 
 * states: 4 */
bool ADD_C_8080(Cpu8080 *cpu)
{
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get the value from register C */
    uint8_t val = cpu->c;
    
    /* get the result of the addition */
    uint8_t result = accum + val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = ((0xFF - accum) < val);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
    
    return true;
}

/* ADD D - same as above but with register D 
 * cycles: 1 
 * states: 4 */
bool ADD_D_8080(Cpu8080 *cpu)
{
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get the value from register D */
    uint8_t val = cpu->d;
    
    /* get the result of the addition */
    uint8_t result = accum + val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = ((0xFF - accum) < val);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
    
    return true;
}

/* ADD E - same as above but with register E 
 * cycles: 1 
 * states: 4 */
bool ADD_E_8080(Cpu8080 *cpu)
{
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get the value from register E */
    uint8_t val = cpu->e;
    
    /* get the result of the addition */
    uint8_t result = accum + val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = ((0xFF - accum) < val);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
    
    return true;
}

/* ADD H - same as above but with register H 
 * cycles: 1 
 * states: 4 */
bool ADD_H_8080(Cpu8080 *cpu)
{
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get the value from register H */
    uint8_t val = cpu->h;
    
    /* get the result of the addition */
    uint8_t result = accum + val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = ((0xFF - accum) < val);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
    
    return true;
}

/* ADD L - same as above but with register L 
 * cycles: 1 
 * states: 4 */
bool ADD_L_8080(Cpu8080 *cpu)
{
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get the value from register H */
    uint8_t val = cpu->l;
    
    /* get the result of the addition */
    uint8_t result = accum + val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = ((0xFF - accum) < val);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
    
    return true;
}

/* ADD M - same as above but you add using the memory
 * pointed to by offset HL 
 * Cycles: 1
 * states: 4 */
bool ADD_M_8080(Cpu8080 *cpu)
{
        /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get the value from the memory offset pointed
     * to by the HL register pair */
    uint8_t val = cpu->memory[(cpu->h << 8) | cpu->l];
    
    /* get the result of the addition */
    uint8_t result = accum + val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = ((0xFF - accum) < val);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
    
    return true;
}


/* ADI data - add immediate
 * the second byte of the instructino is added to the accumulator */
bool ADI_8080(Cpu8080 *cpu)
{
    /* Get the byte to add */
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get what the result will be */
    uint8_t result = accum + byte2;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = ((0xFF - accum) < byte2);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
    
    /* a 2 byte instruction */
    cpu->pc++;
    
    return true;
}

/* RST n - Restart, setting the pc to (8 * NNN)
 *
 * No flags are affected
 *
 * Cycles: 3 
 * States: 11 */
bool RST_0_8080(Cpu8080 *cpu)
{
    /* Move the high order bits of the PC to mem loc SP-1 */
    cpu->memory[cpu->sp - 1] = cpu->pc >> 8;

    /* Move the low order bits of the PC to mem loc SP-2 */
    cpu->memory[cpu->sp - 2] = cpu->pc & 0xFF;

    /* Decrease the SP by 2 */
    cpu->sp -= 2;

    /* Set the PC as 8*NNN, adjusting for the auto pc++  */
    cpu->pc = (8*0) - 1;

    return true;
}

/* RST n - Restart, setting the pc to (8 * NNN)
 *
 * No flags are affected
 *
 * Cycles: 3 
 * States: 11 */
bool RST_1_8080(Cpu8080 *cpu)
{
    /* Move the high order bits of the PC to mem loc SP-1 */
    cpu->memory[cpu->sp - 1] = cpu->pc >> 8;

    /* Move the low order bits of the PC to mem loc SP-2 */
    cpu->memory[cpu->sp - 2] = cpu->pc & 0xFF;

    /* Decrease the SP by 2 */
    cpu->sp -= 2;

    /* Set the PC as 8*NNN, adjusting for the auto pc++  */
    cpu->pc = (8*1) - 1;

    return true;
}

/* SUB B - subtract the value of register B from register A
 * and store it in register A
 * 
 * cycles: 1
 * states: 4 */
bool SUB_B_8080(Cpu8080 *cpu)
{
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get the contents of register B */
    uint8_t val = cpu->b;
    
    /* Get the difference */
    uint8_t result = accum - val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (accum < val);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
}

/* SUB C - subtract the value of register C from register A
 * and store it in register A
 * 
 * cycles: 1
 * states: 4 */
bool SUB_C_8080(Cpu8080 *cpu)
{
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get the contents of register C */
    uint8_t val = cpu->c;
    
    /* Get the difference */
    uint8_t result = accum - val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (accum < val);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
}

/* SUB D - subtract the value of register D from register A
 * and store it in register A
 * 
 * cycles: 1
 * states: 4 */
bool SUB_D_8080(Cpu8080 *cpu)
{
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get the contents of register D */
    uint8_t val = cpu->d;
    
    /* Get the difference */
    uint8_t result = accum - val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (accum < val);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
}

/* SUB E - subtract the value of register E from register A
 * and store it in register A
 * 
 * cycles: 1
 * states: 4 */
bool SUB_E_8080(Cpu8080 *cpu)
{
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get the contents of register E */
    uint8_t val = cpu->e;
    
    /* Get the difference */
    uint8_t result = accum - val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (accum < val);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
}

/* SUB H - subtract the value of register H from register A
 * and store it in register A
 * 
 * cycles: 1
 * states: 4 */
bool SUB_H_8080(Cpu8080 *cpu)
{
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get the contents of register H */
    uint8_t val = cpu->h;
    
    /* Get the difference */
    uint8_t result = accum - val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (accum < val);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
}

/* SUB L - subtract the value of register L from register A
 * and store it in register A
 * 
 * cycles: 1
 * states: 4 */
bool SUB_L_8080(Cpu8080 *cpu)
{
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get the contents of register L */
    uint8_t val = cpu->l;
    
    /* Get the difference */
    uint8_t result = accum - val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (accum < val);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
}

/* SUB M - subtract the value in memory at location register pair HL from register A
 * and store it in register A
 * 
 * cycles: 1
 * states: 4 */
bool SUB_M_8080(Cpu8080 *cpu)
{
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get the contents of memory location (HL) */
    uint8_t val = cpu->memory[(cpu->h << 8) | cpu->l];
    
    /* Get the difference */
    uint8_t result = accum - val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (accum < val);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
}

/* SUB A - subtract the value of register A from register A
 * and store it in register A
 * 
 * cycles: 1
 * states: 4 */
bool SUB_A_8080(Cpu8080 *cpu)
{
    /* Get the current value of the accumulator */
    uint8_t accum = cpu->a;
    
    /* Get the difference - this should be zero, yes? */
    uint8_t result = accum - accum;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag 
     * this is dumb, the result should be zero... */
    cpu->sr[SR_FLAG_C] = (accum < accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;
}

/* SBB reg - Subtract register with borrow 
 * The given register and the carry flag
 * are subtracted from the accumulator 
 *
 * All flags are affected 
 * 
 * Cycles: 2
 * States: 7 */
bool SBB_B_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint8_t result = accum - cpu->b - cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result > accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* SBB reg - Subtract register with borrow 
 * The given register and the carry flag
 * are subtracted from the accumulator 
 *
 * All flags are affected 
 * 
 * Cycles: 2
 * States: 7 */
bool SBB_C_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint8_t result = accum - cpu->c - cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result > accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* SBB reg - Subtract register with borrow 
 * The given register and the carry flag
 * are subtracted from the accumulator 
 *
 * All flags are affected 
 * 
 * Cycles: 2
 * States: 7 */
bool SBB_D_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint8_t result = accum - cpu->d - cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result > accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* SBB reg - Subtract register with borrow 
 * The given register and the carry flag
 * are subtracted from the accumulator 
 *
 * All flags are affected 
 * 
 * Cycles: 2
 * States: 7 */
bool SBB_E_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint8_t result = accum - cpu->e - cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result > accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* SBB reg - Subtract register with borrow 
 * The given register and the carry flag
 * are subtracted from the accumulator 
 *
 * All flags are affected 
 * 
 * Cycles: 2
 * States: 7 */
bool SBB_H_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint8_t result = accum - cpu->h - cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result > accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* SBB reg - Subtract register with borrow 
 * The given register and the carry flag
 * are subtracted from the accumulator 
 *
 * All flags are affected 
 * 
 * Cycles: 2
 * States: 7 */
bool SBB_L_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint8_t result = accum - cpu->l - cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result > accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* SBB reg - Subtract register with borrow 
 * The given register and the carry flag
 * are subtracted from the accumulator 
 *
 * All flags are affected 
 * 
 * Cycles: 2
 * States: 7 */
bool SBB_M_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint16_t addr = (cpu->h << 8) | cpu->l;
    uint8_t result = accum - cpu->memory[addr] - cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result > accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* SBB reg - Subtract register with borrow 
 * The given register and the carry flag
 * are subtracted from the accumulator 
 *
 * All flags are affected 
 * 
 * Cycles: 2
 * States: 7 */
bool SBB_A_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the resulting addition */
    uint16_t addr = (cpu->h << 8) | cpu->l;
    uint8_t result = accum - cpu->a - cpu->sr[SR_FLAG_C];

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag */
    cpu->sr[SR_FLAG_C] = (result > accum);
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Finally update the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ANA B - the accumulator is AND'd with register B
 * 
 * all flags are set
 * 
 * cycles: 1
 * states: 4
 * */
bool ANA_B_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;
    
    /* Get the register B value */
    uint8_t val = cpu->b;
    
    /* Get the anded result */
    uint8_t result = accum & val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag 
     * this will never happen b/c AND */
    cpu->sr[SR_FLAG_C] = 0;
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Set the accumulator with the new value */
    cpu->a = result;
    
    return true;
}

/* ANA C - the accumulator is AND'd with register C
 * 
 * all flags are set
 * 
 * cycles: 1
 * states: 4
 * */
bool ANA_C_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;
    
    /* Get the register C value */
    uint8_t val = cpu->c;
    
    /* Get the anded result */
    uint8_t result = accum & val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag 
     * this will never happen b/c AND */
    cpu->sr[SR_FLAG_C] = 0;
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Set the accumulator with the new value */
    cpu->a = result;
    
    return true;
}

/* ANA D - the accumulator is AND'd with register D
 * 
 * all flags are set
 * 
 * cycles: 1
 * states: 4
 * */
bool ANA_D_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;
    
    /* Get the register D value */
    uint8_t val = cpu->d;
    
    /* Get the anded result */
    uint8_t result = accum & val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag 
     * this will never happen b/c AND */
    cpu->sr[SR_FLAG_C] = 0;
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Set the accumulator with the new value */
    cpu->a = result;
    
    return true;
}

/* ANA E - the accumulator is AND'd with register E
 * 
 * all flags are set
 * 
 * cycles: 1
 * states: 4
 * */
bool ANA_E_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;
    
    /* Get the register E value */
    uint8_t val = cpu->e;
    
    /* Get the anded result */
    uint8_t result = accum & val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag 
     * this will never happen b/c AND */
    cpu->sr[SR_FLAG_C] = 0;
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Set the accumulator with the new value */
    cpu->a = result;
    
    return true;
}

/* ANA H - the accumulator is AND'd with register H
 * 
 * all flags are set
 * 
 * cycles: 1
 * states: 4
 * */
bool ANA_H_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;
    
    /* Get the register H value */
    uint8_t val = cpu->h;
    
    /* Get the anded result */
    uint8_t result = accum & val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag 
     * this will never happen b/c AND */
    cpu->sr[SR_FLAG_C] = 0;
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Set the accumulator with the new value */
    cpu->a = result;
    
    return true;
}

/* ANA L - the accumulator is AND'd with register L 
 * 
 * all flags are set
 * 
 * cycles: 1
 * states: 4
 * */
bool ANA_L_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;
    
    /* Get the register L value */
    uint8_t val = cpu->l;
    
    /* Get the anded result */
    uint8_t result = accum & val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag 
     * this will never happen b/c AND */
    cpu->sr[SR_FLAG_C] = 0;
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Set the accumulator with the new value */
    cpu->a = result;
    
    return true;
}

/* ANA M - the accumulator is AND'd with value at memory location (HL)
 * 
 * all flags are set
 * 
 * cycles: 1
 * states: 4
 * */
bool ANA_M_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;
    
    /* Get the memory value */
    uint8_t val = cpu->memory[(cpu->h << 8) | cpu->l];
    
    /* Get the anded result */
    uint8_t result = accum & val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag 
     * this will never happen b/c AND */
    cpu->sr[SR_FLAG_C] = 0;
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Set the accumulator with the new value */
    cpu->a = result;
    
    return true;
}

/* ANA A - the accumulator is AND'd with register A
 * 
 * all flags are set
 * 
 * cycles: 1
 * states: 4
 * */
bool ANA_A_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;
    
    /* Get the register B value */
    uint8_t val = cpu->a;
    
    /* Get the anded result */
    uint8_t result = accum & val;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
    
    /* If the result caused overflow set the carry flag 
     * this will never happen b/c AND */
    cpu->sr[SR_FLAG_C] = 0;
    
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Set the accumulator with the new value */
    cpu->a = result;
    
    return true;
}

/* XRA B - XORs the accumulator with register B
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool XRA_B_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to xor with */
    uint8_t val = cpu->b;

    /* Get the resulting value */
    uint8_t result = accum ^ val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c XOR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* XRA C - XORs the accumulator with register C
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool XRA_C_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to xor with */
    uint8_t val = cpu->c;

    /* Get the resulting value */
    uint8_t result = accum ^ val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c XOR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* XRA D - XORs the accumulator with register D
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool XRA_D_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to xor with */
    uint8_t val = cpu->d;

    /* Get the resulting value */
    uint8_t result = accum ^ val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c XOR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* XRA E - XORs the accumulator with register E
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool XRA_E_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to xor with */
    uint8_t val = cpu->e;

    /* Get the resulting value */
    uint8_t result = accum ^ val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c XOR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* XRA H - XORs the accumulator with register H
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool XRA_H_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to xor with */
    uint8_t val = cpu->h;

    /* Get the resulting value */
    uint8_t result = accum ^ val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c XOR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* XRA L - XORs the accumulator with register L
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool XRA_L_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to xor with */
    uint8_t val = cpu->l;

    /* Get the resulting value */
    uint8_t result = accum ^ val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c XOR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* XRA M - XORs the accumulator with memory value at (HL)
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool XRA_M_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to xor with */
    uint8_t val = cpu->memory[(cpu->h << 8) | cpu->l];

    /* Get the resulting value */
    uint8_t result = accum ^ val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c XOR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* XRA A - XORs the accumulator with the accumulator
 * (should always set the accumulator to zero, yes?)
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool XRA_A_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to xor with */
    uint8_t val = cpu->a;

    /* Get the resulting value */
    uint8_t result = accum ^ val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c XOR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ORA B - ORs the accumulator with the given register
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool ORA_B_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to OR with A */
    uint8_t val = cpu->b;

    /* Get the resulting value */
    uint8_t result = accum | val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c OR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ORA C - ORs the accumulator with the given register
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool ORA_C_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to OR with A */
    uint8_t val = cpu->c;

    /* Get the resulting value */
    uint8_t result = accum | val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c OR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ORA D - ORs the accumulator with the given register
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool ORA_D_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to OR with A */
    uint8_t val = cpu->d;

    /* Get the resulting value */
    uint8_t result = accum | val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c OR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ORA E - ORs the accumulator with the given register
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool ORA_E_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to OR with A */
    uint8_t val = cpu->e;

    /* Get the resulting value */
    uint8_t result = accum | val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c OR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ORA H - ORs the accumulator with the given register
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool ORA_H_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to OR with A */
    uint8_t val = cpu->h;

    /* Get the resulting value */
    uint8_t result = accum | val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c OR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ORA L - ORs the accumulator with the given register
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool ORA_L_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to OR with A */
    uint8_t val = cpu->l;

    /* Get the resulting value */
    uint8_t result = accum | val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c OR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ORA M - ORs the accumulator with the given register
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool ORA_M_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to OR with A */
    uint8_t val = cpu->memory[(cpu->h << 8) | cpu->l];

    /* Get the resulting value */
    uint8_t result = accum | val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c OR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* ORA A - ORs the accumulator with the given register
 *
 * all flags are set
 *
 * cycles: 1
 * states: 4 */
bool ORA_A_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the value to OR with A (this should just return A, yes?) */
    uint8_t val = cpu->a;

    /* Get the resulting value */
    uint8_t result = accum | val;

    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* If the result caused overflow set the carry flag
     * this will never happen b/c OR */
    cpu->sr[SR_FLAG_C] = 0;

    /* Calculate the parity - if the number of 1s is even or odd
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the accumulator with the new value */
    cpu->a = result;

    return true;
}

/* CMP B - The difference of the accumulator and the given
 * register are used to set the cpu status register flags
 *
 * All flags are set
 * 
 * cycles: 1
 * states: 4 */
bool CMP_B_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the register value to subtract */
    uint8_t val = cpu->b;

    /* Get the difference */
    uint8_t result = accum - val;

    /* The zero flag is set if the accumulator equals the register */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* The carry flag is set if the accumulator < register */
    cpu->sr[SR_FLAG_C] = (accum < val);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* Set the parity bit */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    return true;
}

/* CMP C - The difference of the accumulator and the given
 * register are used to set the cpu status register flags
 *
 * All flags are set
 * 
 * cycles: 1
 * states: 4 */
bool CMP_C_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the register value to subtract */
    uint8_t val = cpu->c;

    /* Get the difference */
    uint8_t result = accum - val;

    /* The zero flag is set if the accumulator equals the register */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* The carry flag is set if the accumulator < register */
    cpu->sr[SR_FLAG_C] = (accum < val);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* Set the parity bit */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    return true;
}

/* CMP D - The difference of the accumulator and the given
 * register are used to set the cpu status register flags
 * 
 * All flags are set
 * 
 * cycles: 1
 * states: 4 */
bool CMP_D_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the register value to subtract */
    uint8_t val = cpu->d;

    /* Get the difference */
    uint8_t result = accum - val;

    /* The zero flag is set if the accumulator equals the register */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* The carry flag is set if the accumulator < register */
    cpu->sr[SR_FLAG_C] = (accum < val);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* Set the parity bit */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    return true;
}

/* CMP E - The difference of the accumulator and the given
 * register are used to set the cpu status register flags 
 *
 * All flags are set
 * 
 * cycles: 1
 * states: 4 */
bool CMP_E_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the register value to subtract */
    uint8_t val = cpu->e;

    /* Get the difference */
    uint8_t result = accum - val;

    /* The zero flag is set if the accumulator equals the register */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* The carry flag is set if the accumulator < register */
    cpu->sr[SR_FLAG_C] = (accum < val);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* Set the parity bit */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    return true;
}

/* CMP H - The difference of the accumulator and the given
 * register are used to set the cpu status register flags
 *
 * All flags are set
 * 
 * cycles: 1
 * states: 4 */
bool CMP_H_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the register value to subtract */
    uint8_t val = cpu->h;

    /* Get the difference */
    uint8_t result = accum - val;

    /* The zero flag is set if the accumulator equals the register */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* The carry flag is set if the accumulator < register */
    cpu->sr[SR_FLAG_C] = (accum < val);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* Set the parity bit */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    return true;
}

/* CMP L - The difference of the accumulator and the given
 * register are used to set the cpu status register flags */
bool CMP_L_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the register value to subtract */
    uint8_t val = cpu->l;

    /* Get the difference */
    uint8_t result = accum - val;

    /* The zero flag is set if the accumulator equals the register */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* The carry flag is set if the accumulator < register */
    cpu->sr[SR_FLAG_C] = (accum < val);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* Set the parity bit */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    return true;
}

/* CMP M - The difference of the accumulator and the value
 * at memory location (HL) are used to set the cpu status register flags 
 *
 * All flags are set
 * 
 * cycles: 1
 * states: 4 */
bool CMP_M_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the register value to subtract */
    uint8_t val = cpu->memory[(cpu->h << 8) | cpu->l];

    /* Get the difference */
    uint8_t result = accum - val;

    /* The zero flag is set if the accumulator equals the register */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* The carry flag is set if the accumulator < register */
    cpu->sr[SR_FLAG_C] = (accum < val);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* Set the parity bit */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    return true;
}

/* CMP A - The difference of the accumulator and the given
 * register are used to set the cpu status register flags 
 * 
 * All flags are set
 * 
 * cycles: 1
 * states: 4 */
bool CMP_A_8080(Cpu8080 *cpu)
{
    /* Get the current accumulator value */
    uint8_t accum = cpu->a;

    /* Get the register value to subtract */
    uint8_t val = cpu->a;

    /* Get the difference */
    uint8_t result = accum - val;

    /* The zero flag is set if the accumulator equals the register */
    cpu->sr[SR_FLAG_Z] = (result == 0);

    /* The carry flag is set if the accumulator < register */
    cpu->sr[SR_FLAG_C] = (accum < val);

    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);

    /* Set the parity bit */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    return true;
}

/* MOV B,B - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_BB_8080(Cpu8080 *cpu)
{
    /* Move register B into Register B (probs wont happen) */
    cpu->b = cpu->b;
    
    return true;
}

/* MOV B,C - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_BC_8080(Cpu8080 *cpu)
{
    /* Move register C into Register B */
    cpu->b = cpu->c;
    
    return true;
}

/* MOV B,D - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_BD_8080(Cpu8080 *cpu)
{
    /* Move register D into Register B */
    cpu->b = cpu->d;
    
    return true;
}

/* MOV B,E - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_BE_8080(Cpu8080 *cpu)
{
    /* Move register E into Register B */
    cpu->b = cpu->e;
    
    return true;
}

/* MOV B,H - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_BH_8080(Cpu8080 *cpu)
{
    /* Move register H into Register B */
    cpu->b = cpu->h;
    
    return true;
}

/* MOV B,L - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_BL_8080(Cpu8080 *cpu)
{
    /* Move register L into Register B */
    cpu->b = cpu->l;
    
    return true;
}

/* MOV B,M - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_BM_8080(Cpu8080 *cpu)
{
    /* Move value at memory (HL) into Register B */
    cpu->b = cpu->memory[(cpu->h << 8) | cpu->l];
    
    return true;
}

/* MOV B,A - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_BA_8080(Cpu8080 *cpu)
{
    /* Move value at memory (HL) into Register B */
    cpu->b = cpu->a;
    
    return true;
}

/* MOV C,B - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_CB_8080(Cpu8080 *cpu)
{
    /* Move register B into Register C */
    cpu->c = cpu->b;
    
    return true;
}

/* MOV C,C - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_CC_8080(Cpu8080 *cpu)
{
    /* Move register C into Register C (Probs won't happen) */
    cpu->c = cpu->c;
    
    return true;
}

/* MOV C,D - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_CD_8080(Cpu8080 *cpu)
{
    /* Move register D into Register C */
    cpu->c = cpu->d;
    
    return true;
}

/* MOV C,E - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_CE_8080(Cpu8080 *cpu)
{
    /* Move register E into Register C */
    cpu->c = cpu->e;
    
    return true;
}

/* MOV C,H - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_CH_8080(Cpu8080 *cpu)
{
    /* Move register H into Register C */
    cpu->c = cpu->h;
    
    return true;
}

/* MOV C,L - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_CL_8080(Cpu8080 *cpu)
{
    /* Move register L into Register C */
    cpu->c = cpu->l;
    
    return true;
}

/* MOV C,M - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_CM_8080(Cpu8080 *cpu)
{
    /* Move value at memory (HL) into Register C */
    cpu->c = cpu->memory[(cpu->h << 8) | cpu->l];
    
    return true;
}

/* MOV C,A - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_CA_8080(Cpu8080 *cpu)
{
    /* Move value at memory (HL) into Register C */
    cpu->c = cpu->a;
    
    return true;
}

/* MOV D,B - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_DB_8080(Cpu8080 *cpu)
{
    /* Move register B into Register D */
    cpu->d = cpu->b;
    
    return true;
}

/* MOV D,C - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_DC_8080(Cpu8080 *cpu)
{
    /* Move register C into register D */
    cpu->d = cpu->c;
    
    return true;
}

/* MOV D,D - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_DD_8080(Cpu8080 *cpu)
{
    /* Move register D into Register D (probs won't happen) */
    cpu->d = cpu->d;
    
    return true;
}

/* MOV D,E - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_DE_8080(Cpu8080 *cpu)
{
    /* Move register E into Register D */
    cpu->d = cpu->e;
    
    return true;
}

/* MOV D,H - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_DH_8080(Cpu8080 *cpu)
{
    /* Move register H into Register D */
    cpu->d = cpu->h;
    
    return true;
}

/* MOV D,L - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_DL_8080(Cpu8080 *cpu)
{
    /* Move register L into Register D */
    cpu->d = cpu->l;
    
    return true;
}

/* MOV D,M - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_DM_8080(Cpu8080 *cpu)
{
    /* Move value at memory (HL) into Register D */
    cpu->d = cpu->memory[(cpu->h << 8) | cpu->l];
    
    return true;
}

/* MOV D,A - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_DA_8080(Cpu8080 *cpu)
{
    /* Move value at memory (HL) into Register D */
    cpu->d = cpu->a;
    
    return true;
}

/* MOV D,B - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_EB_8080(Cpu8080 *cpu)
{
    /* Move register B into Register E */
    cpu->e = cpu->b;
    
    return true;
}

/* MOV E,C - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_EC_8080(Cpu8080 *cpu)
{
    /* Move register C into register e */
    cpu->e = cpu->c;
    
    return true;
}

/* MOV E,D - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_ED_8080(Cpu8080 *cpu)
{
    /* Move register D into Register E (probs won't happen) */
    cpu->e = cpu->d;
    
    return true;
}

/* MOV E,E - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_EE_8080(Cpu8080 *cpu)
{
    /* Move register E into Register E (probs won't happen) */
    cpu->e = cpu->e;
    
    return true;
}

/* MOV E,H - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_EH_8080(Cpu8080 *cpu)
{
    /* Move register H into Register E */
    cpu->e = cpu->h;
    
    return true;
}

/* MOV E,L - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_EL_8080(Cpu8080 *cpu)
{
    /* Move register L into Register E */
    cpu->e = cpu->l;
    
    return true;
}

/* MOV D,M - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_EM_8080(Cpu8080 *cpu)
{
    /* Move value at memory (HL) into Register E */
    cpu->e = cpu->memory[(cpu->h << 8) | cpu->l];
    
    return true;
}

/* MOV E,A - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_EA_8080(Cpu8080 *cpu)
{
    /* Move accumulator value into Register E */
    cpu->e = cpu->a;
    
    return true;
}

/* MOV H,B - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_HB_8080(Cpu8080 *cpu)
{
    /* Move register B into Register H */
    cpu->h = cpu->b;
    
    return true;
}

/* MOV H,C - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_HC_8080(Cpu8080 *cpu)
{
    /* Move register C into register h */
    cpu->h = cpu->c;
    
    return true;
}

/* MOV E,D - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_HD_8080(Cpu8080 *cpu)
{
    /* Move register D into Register H */
    cpu->h = cpu->d;
    
    return true;
}

/* MOV H,E - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_HE_8080(Cpu8080 *cpu)
{
    /* Move register E into Register H */
    cpu->h = cpu->e;
    
    return true;
}

/* MOV E,H - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_HH_8080(Cpu8080 *cpu)
{
    /* Move register H into Register H (probs won't happen) */
    cpu->h = cpu->h;
    
    return true;
}

/* MOV H,L - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_HL_8080(Cpu8080 *cpu)
{
    /* Move register L into Register H */
    cpu->h = cpu->l;
    
    return true;
}

/* MOV H,M - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_HM_8080(Cpu8080 *cpu)
{
    /* Move value at memory (HL) into Register H */
    cpu->h = cpu->memory[(cpu->h << 8) | cpu->l];
    
    return true;
}

/* MOV H,A - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_HA_8080(Cpu8080 *cpu)
{
    /* Move accumulator value into Register H */
    cpu->h = cpu->a;
    
    return true;
}


/* MOV L,B - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_LB_8080(Cpu8080 *cpu)
{
    /* Move register B into Register L */
    cpu->l = cpu->b;
    
    return true;
}

/* MOV L,C - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_LC_8080(Cpu8080 *cpu)
{
    /* Move register C into register L */
    cpu->l = cpu->c;
    
    return true;
}

/* MOV L,D - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_LD_8080(Cpu8080 *cpu)
{
    /* Move register D into Register L */
    cpu->l = cpu->d;
    
    return true;
}

/* MOV L,E - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_LE_8080(Cpu8080 *cpu)
{
    /* Move register E into Register L */
    cpu->l = cpu->e;
    
    return true;
}

/* MOV L,H - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_LH_8080(Cpu8080 *cpu)
{
    /* Move register H into Register L */
    cpu->l = cpu->h;
    
    return true;
}

/* MOV L,L - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_LL_8080(Cpu8080 *cpu)
{
    /* Move register L into Register L (probs won't happen) */
    cpu->l = cpu->l;
    
    return true;
}

/* MOV L,M - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_LM_8080(Cpu8080 *cpu)
{
    /* Move value at memory (HL) into Register L */
    cpu->l = cpu->memory[(cpu->h << 8) | cpu->l];
    
    return true;
}

/* MOV L,A - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_LA_8080(Cpu8080 *cpu)
{
    /* Move accumulator value into Register L */
    cpu->l = cpu->a;
    
    return true;
}

/* MOV M,B - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_MB_8080(Cpu8080 *cpu)
{
    /* Move register B into memory (HL) */
    cpu->memory[(cpu->h << 8) | cpu->l] = cpu->b;
    
    return true;
}

/* MOV M,C - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_MC_8080(Cpu8080 *cpu)
{
    /* Move register C into memory (HL) */
    cpu->memory[(cpu->h << 8) | cpu->l] = cpu->c;
    
    return true;
}

/* MOV M,D - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_MD_8080(Cpu8080 *cpu)
{
    /* Move register D into memory (HL) */
    cpu->memory[(cpu->h << 8) | cpu->l] = cpu->d;
    
    return true;
}

/* MOV M,E - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_ME_8080(Cpu8080 *cpu)
{
    /* Move register E into memory (HL) */
    cpu->memory[(cpu->h << 8) | cpu->l] = cpu->e;
    
    return true;
}

/* MOV M,H - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_MH_8080(Cpu8080 *cpu)
{
    /* Move register H into memory (HL) */
    cpu->memory[(cpu->h << 8) | cpu->l] = cpu->h;
    
    return true;
}

/* MOV M,L - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_ML_8080(Cpu8080 *cpu)
{
    /* Move register L into memory (HL) */
    cpu->memory[(cpu->h << 8) | cpu->l] = cpu->l;
    
    return true;
}

/* MOV M,A - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_MA_8080(Cpu8080 *cpu)
{
    /* Move accumulator value into Memory (HL) */
    cpu->memory[(cpu->h << 8) | cpu->l] = cpu->a;
    
    return true;
}


/* MOV A,B - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_AB_8080(Cpu8080 *cpu)
{
    /* Move register B into Register A */
    cpu->a = cpu->b;
    
    return true;
}

/* MOV A,C - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_AC_8080(Cpu8080 *cpu)
{
    /* Move register C into register A */
    cpu->a = cpu->c;
    
    return true;
}

/* MOV A,D - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_AD_8080(Cpu8080 *cpu)
{
    /* Move register D into Register A */
    cpu->a = cpu->d;
    
    return true;
}

/* MOV A,E - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_AE_8080(Cpu8080 *cpu)
{
    /* Move register E into Register A */
    cpu->a = cpu->e;
    
    return true;
}

/* MOV A,H - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_AH_8080(Cpu8080 *cpu)
{
    /* Move register H into Register A */
    cpu->a = cpu->h;
    
    return true;
}

/* MOV A,L - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_AL_8080(Cpu8080 *cpu)
{
    /* Move register L into Register A */
    cpu->a = cpu->l;
    
    return true;
}

/* MOV A,M - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_AM_8080(Cpu8080 *cpu)
{
    /* Move value at memory (HL) into Register A */
    cpu->a = cpu->memory[(cpu->h << 8) | cpu->l];
    
    return true;
}

/* MOV L,A - move register - the content of register 2 is 
 * moved into register 1
 * 
 * no flags are set 
 * 
 * cycles: 1
 * states: 5 */
bool MOV_AA_8080(Cpu8080 *cpu)
{
    /* Move accumulator value into the accumulator (probs wont happen) */
    cpu->a = cpu->a;
    
    return true;
}

/* JMP addr (Jump) - control is transferred to the instruction 
 * who's address is specified in byte3 and byte2 of the current instruction */
bool JMP_8080(Cpu8080 *cpu)
{
	/* Get the bytes to set the PC as */
	uint8_t byte3 = cpu->memory[cpu->pc+2];
	uint8_t byte2 = cpu->memory[cpu->pc+1];
	
	/* Assign the PC, compensating for our pc++ in RunNextInstruction */
	cpu->pc = ((byte3 << 8) | byte2) - 1;

    return true;
}

/* JNZ addr (Jump if not zero) - if the zero flag isn't set, control is 
 * transferred to the instruction who's address is specified in byte3
 * and byte2 of the current instruction*/
bool JNZ_8080(Cpu8080 *cpu)
{
    if (!cpu->sr[SR_FLAG_Z]) {

    	/* Get the bytes to set the PC as */
    	uint8_t byte3 = cpu->memory[cpu->pc+2];
    	uint8_t byte2 = cpu->memory[cpu->pc+1];
    	
    	/* Assign the PC, compensating for our pc++ in RunNextInstruction */
    	cpu->pc = ((byte3 << 8) | byte2) - 1;
    }

    return true;
}

/* JZ addr (Jump if zero) - if the zero flag isn set, control is 
 * transferred to the instruction who's address is specified in byte3
 * and byte2 of the current instruction*/
bool JZ_8080(Cpu8080 *cpu)
{
    if (cpu->sr[SR_FLAG_Z]) {

    	/* Get the bytes to set the PC as */
    	uint8_t byte3 = cpu->memory[cpu->pc+2];
    	uint8_t byte2 = cpu->memory[cpu->pc+1];
    	
    	/* Assign the PC, compensating for our pc++ in RunNextInstruction */
    	cpu->pc = ((byte3 << 8) | byte2) - 1;
    }

    return true;
}


/* CALL addr - Adjust the stack pointer and set the program counter
 * cycles: 5
 * states: 17 */
bool CALL_8080(Cpu8080 *cpu)
{
    /* Get the bytes to store in the program counter */
    uint8_t byte3 = cpu->memory[cpu->pc+2];
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    
    /* Get the next instruction address to store in memory */
    uint16_t instr = cpu->pc+2;
    
    /* The high 8 bits of the next instruction are stored
     * in the memory location stack pointer minus 1 */
    cpu->memory[cpu->sp - 1] = (instr >> 8) & 0xFF;
    
    /* The low 8 bits of the next instruction are stored
     * in the memory location stack pointer - 2*/
    cpu->memory[cpu->sp - 2] = instr & 0xFF;
    
    /* Subtract two from the stack pointer */
    cpu->sp -= 2;
     
    /* Set the new program counter address, compensating
     * for the auto pc++  */
    cpu->pc = ((byte3 << 8) | byte2) - 1;
    
    return true;
    
}

/* CNZ addr - if the zero flag isn't set,
 * Adjust the stack pointer and set the program counter
 * cycles: 5
 * states: 17 */
bool CNZ_8080(Cpu8080 *cpu)
{
    if (!cpu->sr[SR_FLAG_Z]) {

        /* Get the bytes to store in the program counter */
        uint8_t byte3 = cpu->memory[cpu->pc+2];
        uint8_t byte2 = cpu->memory[cpu->pc+1];
        
        /* Get the next instruction address to store in memory */
        uint16_t instr = cpu->pc+2;
        
        /* The high 8 bits of the next instruction are stored
         * in the memory location stack pointer minus 1 */
        cpu->memory[cpu->sp - 1] = (instr >> 8) & 0xFF;
        
        /* The low 8 bits of the next instruction are stored
         * in the memory location stack pointer - 2*/
        cpu->memory[cpu->sp - 2] = instr & 0xFF;
        
        /* Subtract two from the stack pointer */
        cpu->sp -= 2;
         
        /* Set the new program counter address, compensating
         * for the auto pc++  */
        cpu->pc = ((byte3 << 8) | byte2) - 1;
    }

    return true;
}

/* CZ addr - if the zero flag isn set,
 * Adjust the stack pointer and set the program counter
 * cycles: 5
 * states: 17 */
bool CZ_8080(Cpu8080 *cpu)
{
    if (cpu->sr[SR_FLAG_Z]) {

        /* Get the bytes to store in the program counter */
        uint8_t byte3 = cpu->memory[cpu->pc+2];
        uint8_t byte2 = cpu->memory[cpu->pc+1];
        
        /* Get the next instruction address to store in memory */
        uint16_t instr = cpu->pc+2;
        
        /* The high 8 bits of the next instruction are stored
         * in the memory location stack pointer minus 1 */
        cpu->memory[cpu->sp - 1] = (instr >> 8) & 0xFF;
        
        /* The low 8 bits of the next instruction are stored
         * in the memory location stack pointer - 2*/
        cpu->memory[cpu->sp - 2] = instr & 0xFF;
        
        /* Subtract two from the stack pointer */
        cpu->sp -= 2;
         
        /* Set the new program counter address, compensating
         * for the auto pc++  */
        cpu->pc = ((byte3 << 8) | byte2) - 1;
    }

    return true;
}


/* POP register pair - pop the stack pointer, storing the value 
 * in the given register pair
 *
 * No flags are affected
 *
 * Cycles: 3
 * States: 10 */
bool POP_B_8080(Cpu8080 *cpu)
{
    /* Store the value at mem location SP in the lower register */
    cpu->c = cpu->memory[cpu->sp];

    /* Store the value at mem location SP+1 in the higher register */
    cpu->b = cpu->memory[cpu->sp+1];

    /* Increment the stack pointer */
    cpu->sp += 2;

    return true;
}

/* Push register pair - push the stack pointer, getting
 * the value from the given register pair 
 *
 * Cycles: 3
 * States: 11 */
bool PUSH_B_8080(Cpu8080 *cpu)
{
    /* Store high register in location SP-1 */
    cpu->memory[cpu->sp-1] = cpu->b;

    /* Store the low register in location SP-2 */
    cpu->memory[cpu->sp-2] = cpu->c;

    /* Decrement the stack pointer by 2 */
    cpu->sp -= 2;

    return true;
}

/* RNZ - return if not zero - run the `return` op if
 * the zero flag isn't set \
 *
 * No flags are set
 *
 * Cycles: 1/3
 * States: 5/11 */
bool RNZ_8080(Cpu8080 *cpu)
{
    /* Test the zero flag */
    if (!cpu->sr[SR_FLAG_Z]) {

        /* Store the value at address SP in the low order 
         * bits of the PC */
        cpu->pc = cpu->memory[cpu->sp];

        /* Store the value at address SP+1 in the high order bits
         * of the SP */
        cpu->pc |= cpu->memory[cpu->sp+1] << 8;

        /* Increment the stack pointer by 2 */
        cpu->sp += 2;

    }

    return true;
}

/* RZ - return if zero - run the `return` op if
 * the zero flag is set \
 *
 * No flags are set
 *
 * Cycles: 1/3
 * States: 5/11 */
bool RZ_8080(Cpu8080 *cpu)
{
    /* Test the zero flag */
    if (cpu->sr[SR_FLAG_Z]) {

        /* Store the value at address SP in the low order 
         * bits of the PC */
        cpu->pc = cpu->memory[cpu->sp];

        /* Store the value at address SP+1 in the high order bits
         * of the SP */
        cpu->pc |= cpu->memory[cpu->sp+1] << 8;

        /* Increment the stack pointer by 2 */
        cpu->sp += 2;

    }

    return true;
}

/* RET - return - move the PC back to the address
 * given by the stack pointer
 *
 * No flags are set
 *
 * Cycles: 3 
 * States: 10 */
bool RET_8080(Cpu8080 *cpu)
{
    /* Store the value at address SP in the low order 
     * bits of the PC */
    cpu->pc = cpu->memory[cpu->sp];

    /* Store the value at address SP+1 in the high order bits
     * of the SP */
    cpu->pc |= cpu->memory[cpu->sp+1] << 8;

    /* Increment the stack pointer by 2 */
    cpu->sp += 2;

    return true;
}

/* RNC - return if the carry flag isn't set
 *
 * No flags are set
 *
 * Cycles:  
 * States: */
bool RNC_8080(Cpu8080 *cpu)
{
    /* Test the carry flag */
    if (!cpu->sr[SR_FLAG_C]) {
    
        /* Store the value at address SP in the low order 
         * bits of the PC */
        cpu->pc = cpu->memory[cpu->sp];
    
        /* Store the value at address SP+1 in the high order bits
         * of the SP */
        cpu->pc |= cpu->memory[cpu->sp+1] << 8;
    
        /* Increment the stack pointer by 2 */
        cpu->sp += 2;
    }

    return true;
}
/* LXI rp, data 16 - Load register pair immediate 
 * byte 3 of the instruction goes into the high order 
 * register (rh) of the pair rp, byte 2 goes into 
 * the lower order register (rl) 
 *
 * in this case rp is the stack pointer (11) since the instruction is
 * 0b00RP0001
 * 0b00110001
 *
 * cycles: 3
 * states: 10
 */
bool LXI_SP_8080(Cpu8080 *cpu )
{
	/* Get the two bytes */
	uint8_t byte3 = cpu->memory[cpu->pc+2];
	uint8_t byte2 = cpu->memory[cpu->pc+1];
	
	/* Store the two bytes in the register */
	cpu->sp = (byte3 << 8) | byte2;
	
	/* This was a 3 byte instruction, so add the two 
	 * used bytes to the pc */
	cpu->pc += 2;
    
    return true;
}

/* STA adr - Store Accumulator Direct - the accumulator
 * value is copied into the given address 
 *
 * no flags are affected
 *
 * Cycles: 4
 * States: 13*/
bool STA_8080(Cpu8080 *cpu)
{
    /* Get the address */
    uint8_t byte3 = cpu->memory[cpu->pc+2];
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    uint16_t addr = (byte3 << 8) | byte2;
    
    /* Store the accumulator in the address */
    cpu->memory[addr] = cpu->a;

    /* A 3 byte instruction */
    cpu->pc += 2;
    
    return true;
}

/* INX SP - stack pointer is incremented by 1
 * no flags are set 
 *
 * cycles: 1
 * states: 5 */
bool INX_SP_8080(Cpu8080 *cpu)
{
    /* Increment the stack pointer by 1 */
    cpu->sp++;

    return true;
}

/* INR M - increment memory at (HL) by 1
 *
 * Z,S,P,AC flags are set (NOT the carry flag) 
 * 
 * cycles: 3
 * states: 10 */
bool INR_M_8080(Cpu8080 *cpu)
{
    /* Get the current value */
    uint16_t addr = (cpu->h << 8) | cpu->l;
    uint8_t val = cpu->memory[addr];

    /* Get the resulting value */
    uint8_t result = val + 1;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
     
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the new value */
    cpu->memory[addr] = result;

    return true;
}

/* DCR M - decrement the value at the address (HL) by 1
 * 
 * all flags except carry set
 *
 * cycles: 3
 * states: 10 */
bool DCR_M_8080(Cpu8080 *cpu)
{
    /* Get the current value */
    uint16_t addr = (cpu->h << 8) | cpu->l;
    uint8_t val = cpu->memory[addr];
    
    /* Get the result of the subraction */
    uint8_t result = val - 1;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
     
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Updatethe new value */
    cpu->memory[addr] = result;
    
    return true;
}

/* MVI r, data - Move immediate 
 * byte 2 is stored in register r, in this case memory location (HL)
 * cycles: 
 * states:  */
bool MVI_M_8080(Cpu8080 *cpu)
{
    /* Get byte 2 */
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    
    /* Store it in memory location HL */
    cpu->memory[(cpu->h << 8) | cpu->l] = byte2;
    
    /* a 2 byte instruction, so add the used byte to the pc */
    cpu->pc++;
    
    return true;
}

/* STC - set carry - the carry flag is set to 1
 * 
 * no flags (besides carry) are affected 
 * 
 * Cycles: 1
 * States: 4 */
bool STC_8080(Cpu8080 *cpu)
{
    cpu->sr[SR_FLAG_C] = 1;

    return true;
}

/* DAD registerPair - (HL) += the given register pair 
 * 
 * Only the carry flag is affected
 * 
 * Cycles: 3
 * States: 10 */
bool DAD_SP_8080(Cpu8080 *cpu)
{
    /* Get the current HL register pair value */
    uint16_t curVal = (cpu->h << 8) | cpu->l;

    /* Get the result */
    uint16_t result = curVal + cpu->sp;

    /* Check for overflow */
    cpu->sr[SR_FLAG_C] = ((0xFFFF - curVal) < cpu->sp);

    /* Set the new register pair */
    cpu->h = result >> 8;
    cpu->l = result & 0xFF;

    return true;
}

/* LDA adr - load the value at the given address
 * into the accumulator 
 *
 * No flags are affected
 *
 * Cycles: 4
 * States: 13 */
bool LDA_8080(Cpu8080 *cpu)
{
    /* Load the value at the given address
     * into the accumulator */
    uint8_t byte3 = cpu->memory[cpu->pc+2];
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    cpu->a = cpu->memory[(byte3 << 8) | byte2];

    /* A three byte instruction */
    cpu->pc += 2;

    return true;
}

/* DCX SP - the given register pair is decremented by 1 
 * 
 * No flags are affected 
 *
 * Cycles: 
 * States: */
bool DCX_SP_8080(Cpu8080 *cpu)
{
    /* Decrement the stack pointer */
    cpu->sp--;

    return true;
}

/* INR A - increment the given register by 1 
 *
 * Z,S,P,AC flags are set (NOT the carry flag) 
 * 
 * cycles: 1
 * states: 5 */
bool INR_A_8080(Cpu8080 *cpu)
{
    /* Get the current register value */
    uint8_t reg = cpu->a;

    /* Get the resulting value */
    uint8_t result = reg + 1;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
     
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);

    /* Set the new register value */
    cpu->a = result;

    return true;
}

/* DCR A - decrement the given register by 1
 * 
 * all flags except carry set
 *
 * cycles: 5
 * states: */
bool DCR_A_8080(Cpu8080 *cpu)
{
    /* Get the current value of A */
    uint8_t val = cpu->a;
    
    /* Get the result of the subraction */
    uint8_t result = val - 1;
    
    /* Set the zero flag if the result is 0 */
    cpu->sr[SR_FLAG_Z] = (result == 0);
    
    /* Set the sign flag if bit 7 is set (the highest bit) */
    cpu->sr[SR_FLAG_S] = ((result & 0x80) == 0x80);
     
    /* Calculate the parity - if the number of 1s is even or odd 
     * we flip the bit because we want even parity, not odd */
    cpu->sr[SR_FLAG_P] = !getOddParity(result);
    
    /* Update the register with the new value */
    cpu->a = result;
    
    return true;
}

/* MVI r, data - Move immediate 
 * byte 2 is stored in register r, in this case register C
 * cycles: 
 * states:  */
bool MVI_A_8080(Cpu8080 *cpu)
{
    /* Get byte 2 */
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    
    /* Store it in register A */
    cpu->a = byte2;
    
    /* a 2 byte instruction, so add the used byte to the pc */
    cpu->pc++;
    
    return true;
}

/* CMC - Complement Carry - the carry flag is negated 
 * 
 * No flags are affected 
 * 
 * Cycles: 1
 * States: 4 */
bool CMC_8080(Cpu8080 *cpu)
{
    /* Negate the carry flag */
    cpu->sr[SR_FLAG_C] = !cpu->sr[SR_FLAG_C];

    return true;
}

/* LXI B, data 16 - load register pair immedite,
 * This time into the registry pair B,C 
 * 
 * cycles: 3
 * states: 10 */
bool LXI_B_8080(Cpu8080* cpu)
{
    /* Get the two bytes */
    uint8_t byte3 = cpu->memory[cpu->pc+2];
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    
    /* Store them in the register pair */
    cpu->b = byte3;
    cpu->c = byte2;
    
    /* a 3 cycle instruction */
    cpu->pc += 2;
    
    return true;
}

/* MVI r, data - Move immediate 
 * byte 2 is stored in register r, in this case 000
 * which represents register B 
 * cycles: 2
 * states: 7 */
bool MVI_B_8080(Cpu8080 *cpu)
{
    /* Get byte 2 */
    uint8_t byte2 = cpu->memory[cpu->pc+1];
    
    /* Store it in register B */
    cpu->b = byte2;
    
    /* a 2 byte instruction, so add the used byte to the pc */
    cpu->pc++;
    
    return true;
}

