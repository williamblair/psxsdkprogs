#include "romE.h"
#include "romF.h"
#include "romG.h"
#include "romH.h"

#include "8080.h"

#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include <psx.h>

int main(int argc, char *argv[])
{
    Cpu8080 cpu;
	Init_8080(&cpu);
	
	for(;;)
	{
        if(!RunNextInstruction_8080(&cpu)) break;
	}

    return 0;
}