#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "romH.h"
#include "romG.h"
#include "romF.h"
#include "romE.h"

#ifndef _8080_H_INCLUDED_
#define _8080_H_INCLUDED_

#define BOARD_ROM_SIZE  0x2000 // 0x0000 - 0x1FFF 8K ROM
#define BOARD_RAM_SIZE  0x400  // 0x2000 - 0x23FF 1K RAM
#define BOARD_VRAM_SIZE 0x1C00 // 0x2400 - 0x3FFF 7K Video RAM

/* Info From http://computerarcheology.com/Arcade/SpaceInvaders/Hardware.html */
#define ROM_H_START 0x0000
#define ROM_G_START 0x0800
#define ROM_F_START 0x1000
#define ROM_E_START 0x1800

/* Status register flag indices */
enum
{
    SR_FLAG_S  = 0,
    SR_FLAG_Z  = 1,
    SR_FLAG_AC = 3,
    SR_FLAG_P  = 5,
    SR_FLAG_C  = 7
};

typedef struct 
{
	/* Program Counter */
	uint16_t pc;
	
	/* Stack Pointer */
	uint16_t sp;
	
	/* Registers */
    uint8_t a; uint8_t psw;
	uint8_t b; uint8_t c;
	uint8_t d; uint8_t e;
	uint8_t h; uint8_t l;
    
    /* Status register
     * technically should be 1 byte
     * but this is easier */
    uint8_t sr[8];
	
	/* Pointer to the motherboard's memory */
	uint8_t memory[BOARD_ROM_SIZE + BOARD_RAM_SIZE + BOARD_VRAM_SIZE];
	
} Cpu8080;

/* Init memory */
void Init_8080(Cpu8080 *);

/* Clear memory */
void Reset_8080(Cpu8080 *);

/* Fetch the next opcode, run it, and increment the PC */
bool RunNextInstruction_8080(Cpu8080 *);

/* Returns 1 if the number of 1s is odd */
uint8_t getOddParity(uint8_t num);

/* Ops */
bool dummy(Cpu8080 *);
bool NOP_8080(Cpu8080 *);

bool STAX_B_8080(Cpu8080 *);
bool STAX_D_8080(Cpu8080 *);

bool STA_8080(Cpu8080 *);

bool STC_8080(Cpu8080 *);

bool INX_B_8080(Cpu8080 *);
bool INX_D_8080(Cpu8080 *);
bool INX_H_8080(Cpu8080 *);
bool INX_SP_8080(Cpu8080 *);

bool INR_A_8080(Cpu8080 *);
bool INR_B_8080(Cpu8080 *);
bool INR_C_8080(Cpu8080 *);
bool INR_D_8080(Cpu8080 *);
bool INR_E_8080(Cpu8080 *);
bool INR_H_8080(Cpu8080 *);
bool INR_L_8080(Cpu8080 *);
bool INR_M_8080(Cpu8080 *);

bool DCR_A_8080(Cpu8080 *);
bool DCR_B_8080(Cpu8080 *);
bool DCR_C_8080(Cpu8080 *);
bool DCR_D_8080(Cpu8080 *);
bool DCR_E_8080(Cpu8080 *);
bool DCR_H_8080(Cpu8080 *);
bool DCR_L_8080(Cpu8080 *);
bool DCR_M_8080(Cpu8080 *);

bool RLC_8080(Cpu8080 *);
bool RRC_8080(Cpu8080 *);
bool RAL_8080(Cpu8080 *);
bool RAR_8080(Cpu8080 *);

bool DAD_B_8080(Cpu8080 *);
bool DAD_D_8080(Cpu8080 *);
bool DAD_H_8080(Cpu8080 *);
bool DAD_SP_8080(Cpu8080 *);

bool LDAX_B_8080(Cpu8080 *);
bool LDAX_D_8080(Cpu8080 *);

bool LDA_8080(Cpu8080 *);

bool LHLD_8080(Cpu8080 *);

bool DCX_B_8080(Cpu8080 *);
bool DCX_D_8080(Cpu8080 *);
bool DCX_H_8080(Cpu8080 *);
bool DCX_SP_8080(Cpu8080 *);

bool CMA_8080(Cpu8080 *);
bool CMC_8080(Cpu8080 *);

/* ADD ops */
bool ADD_A_8080(Cpu8080 *);
bool ADD_B_8080(Cpu8080 *);
bool ADD_C_8080(Cpu8080 *);
bool ADD_D_8080(Cpu8080 *);
bool ADD_E_8080(Cpu8080 *);
bool ADD_H_8080(Cpu8080 *);
bool ADD_L_8080(Cpu8080 *);
bool ADD_M_8080(Cpu8080 *);
bool ADI_8080(Cpu8080 *);

/* ADC ops */
bool ADC_A_8080(Cpu8080 *);
bool ADC_B_8080(Cpu8080 *);
bool ADC_C_8080(Cpu8080 *);
bool ADC_D_8080(Cpu8080 *);
bool ADC_E_8080(Cpu8080 *);
bool ADC_H_8080(Cpu8080 *);
bool ADC_L_8080(Cpu8080 *);
bool ADC_M_8080(Cpu8080 *);

/* SUB ops */
bool SUB_A_8080(Cpu8080 *);
bool SUB_B_8080(Cpu8080 *);
bool SUB_C_8080(Cpu8080 *);
bool SUB_D_8080(Cpu8080 *);
bool SUB_E_8080(Cpu8080 *);
bool SUB_H_8080(Cpu8080 *);
bool SUB_L_8080(Cpu8080 *);
bool SUB_M_8080(Cpu8080 *);

/* SBB ops */
bool SBB_A_8080(Cpu8080 *);
bool SBB_B_8080(Cpu8080 *);
bool SBB_C_8080(Cpu8080 *);
bool SBB_D_8080(Cpu8080 *);
bool SBB_E_8080(Cpu8080 *);
bool SBB_H_8080(Cpu8080 *);
bool SBB_L_8080(Cpu8080 *);
bool SBB_M_8080(Cpu8080 *);

/* AND ops */
bool ANA_B_8080(Cpu8080 *);
bool ANA_C_8080(Cpu8080 *);
bool ANA_D_8080(Cpu8080 *);
bool ANA_E_8080(Cpu8080 *);
bool ANA_H_8080(Cpu8080 *);
bool ANA_L_8080(Cpu8080 *);
bool ANA_M_8080(Cpu8080 *);
bool ANA_A_8080(Cpu8080 *);

/* XOR ops */
bool XRA_B_8080(Cpu8080 *);
bool XRA_C_8080(Cpu8080 *);
bool XRA_D_8080(Cpu8080 *);
bool XRA_E_8080(Cpu8080 *);
bool XRA_H_8080(Cpu8080 *);
bool XRA_L_8080(Cpu8080 *);
bool XRA_M_8080(Cpu8080 *);
bool XRA_A_8080(Cpu8080 *);

/* ORA ops */
bool ORA_B_8080(Cpu8080 *);
bool ORA_C_8080(Cpu8080 *);
bool ORA_D_8080(Cpu8080 *);
bool ORA_E_8080(Cpu8080 *);
bool ORA_H_8080(Cpu8080 *);
bool ORA_L_8080(Cpu8080 *);
bool ORA_M_8080(Cpu8080 *);
bool ORA_A_8080(Cpu8080 *);

/* CMP ops */
bool CMP_B_8080(Cpu8080 *);
bool CMP_C_8080(Cpu8080 *);
bool CMP_D_8080(Cpu8080 *);
bool CMP_E_8080(Cpu8080 *);
bool CMP_H_8080(Cpu8080 *);
bool CMP_L_8080(Cpu8080 *);
bool CMP_M_8080(Cpu8080 *);
bool CMP_A_8080(Cpu8080 *);

/* Reset Ops */
bool RST_0_8080(Cpu8080 *);
bool RST_1_8080(Cpu8080 *);

/* MOV ops */
bool MOV_BA_8080(Cpu8080 *);
bool MOV_BB_8080(Cpu8080 *);
bool MOV_BC_8080(Cpu8080 *);
bool MOV_BD_8080(Cpu8080 *);
bool MOV_BE_8080(Cpu8080 *);
bool MOV_BH_8080(Cpu8080 *);
bool MOV_BL_8080(Cpu8080 *);
bool MOV_BM_8080(Cpu8080 *);

bool MOV_CA_8080(Cpu8080 *);
bool MOV_CB_8080(Cpu8080 *);
bool MOV_CC_8080(Cpu8080 *);
bool MOV_CD_8080(Cpu8080 *);
bool MOV_CE_8080(Cpu8080 *);
bool MOV_CH_8080(Cpu8080 *);
bool MOV_CL_8080(Cpu8080 *);
bool MOV_CM_8080(Cpu8080 *);

bool MOV_DA_8080(Cpu8080 *);
bool MOV_DB_8080(Cpu8080 *);
bool MOV_DC_8080(Cpu8080 *);
bool MOV_DD_8080(Cpu8080 *);
bool MOV_DE_8080(Cpu8080 *);
bool MOV_DH_8080(Cpu8080 *);
bool MOV_DL_8080(Cpu8080 *);
bool MOV_DM_8080(Cpu8080 *);

bool MOV_EA_8080(Cpu8080 *);
bool MOV_EB_8080(Cpu8080 *);
bool MOV_EC_8080(Cpu8080 *);
bool MOV_ED_8080(Cpu8080 *);
bool MOV_EE_8080(Cpu8080 *);
bool MOV_EH_8080(Cpu8080 *);
bool MOV_EL_8080(Cpu8080 *);
bool MOV_EM_8080(Cpu8080 *);

bool MOV_HA_8080(Cpu8080 *);
bool MOV_HB_8080(Cpu8080 *);
bool MOV_HC_8080(Cpu8080 *);
bool MOV_HD_8080(Cpu8080 *);
bool MOV_HE_8080(Cpu8080 *);
bool MOV_HH_8080(Cpu8080 *);
bool MOV_HL_8080(Cpu8080 *);
bool MOV_HM_8080(Cpu8080 *);

bool MOV_LA_8080(Cpu8080 *);
bool MOV_LB_8080(Cpu8080 *);
bool MOV_LC_8080(Cpu8080 *);
bool MOV_LD_8080(Cpu8080 *);
bool MOV_LE_8080(Cpu8080 *);
bool MOV_LH_8080(Cpu8080 *);
bool MOV_LL_8080(Cpu8080 *);
bool MOV_LM_8080(Cpu8080 *);

bool MOV_MA_8080(Cpu8080 *);
bool MOV_MB_8080(Cpu8080 *);
bool MOV_MC_8080(Cpu8080 *);
bool MOV_MD_8080(Cpu8080 *);
bool MOV_ME_8080(Cpu8080 *);
bool MOV_MH_8080(Cpu8080 *);
bool MOV_ML_8080(Cpu8080 *);
//bool MOV_MM_8080(Cpu8080 *); // this is a special op...

bool MOV_AA_8080(Cpu8080 *);
bool MOV_AB_8080(Cpu8080 *);
bool MOV_AC_8080(Cpu8080 *);
bool MOV_AD_8080(Cpu8080 *);
bool MOV_AE_8080(Cpu8080 *);
bool MOV_AH_8080(Cpu8080 *);
bool MOV_AL_8080(Cpu8080 *);
bool MOV_AM_8080(Cpu8080 *);

/* Conditional Returns */
bool RNZ_8080(Cpu8080 *);
bool RZ_8080(Cpu8080 *);
bool RET_8080(Cpu8080 *);
bool RNC_8080(Cpu8080 *);

bool JMP_8080(Cpu8080 *);
bool JNZ_8080(Cpu8080 *);
bool JZ_8080(Cpu8080 *);
bool CALL_8080(Cpu8080 *);
bool CNZ_8080(Cpu8080 *);
bool CZ_8080(Cpu8080 *);

bool PUSH_B_8080(Cpu8080 *);
bool POP_B_8080(Cpu8080 *);

bool LXI_B_8080(Cpu8080 *);
bool LXI_D_8080(Cpu8080 *);
bool LXI_SP_8080(Cpu8080 *);
bool LXI_H_8080(Cpu8080 *);

bool SHLD_8080(Cpu8080 *);

bool MVI_A_8080(Cpu8080 *);
bool MVI_B_8080(Cpu8080 *);
bool MVI_C_8080(Cpu8080 *);
bool MVI_D_8080(Cpu8080 *);
bool MVI_E_8080(Cpu8080 *);
bool MVI_H_8080(Cpu8080 *);
bool MVI_L_8080(Cpu8080 *);
bool MVI_M_8080(Cpu8080 *);

/* Function lookup table */
typedef bool (* Op_8080)(Cpu8080 *);

/* 256 possible ops 0x00 - 0xff */
static Op_8080 ops[256] = {
	NOP_8080,    LXI_B_8080,  STAX_B_8080, INX_B_8080,  INR_B_8080,  DCR_B_8080,  MVI_B_8080,  RLC_8080,    dummy,       DAD_B_8080,  // 0-9
	LDAX_B_8080, DCX_B_8080,  INR_C_8080,  DCR_C_8080,  MVI_C_8080,  RRC_8080,    dummy,       LXI_D_8080,  STAX_D_8080, INX_D_8080,  // 10-19
	INR_D_8080,  DCR_D_8080,  MVI_D_8080,  RAL_8080,    dummy,       DAD_D_8080,  LDAX_D_8080, DCX_D_8080,  INR_E_8080,  DCR_E_8080,  // 20-39
	MVI_E_8080,  RAR_8080,    dummy,       LXI_H_8080,  SHLD_8080,   INX_H_8080,  INR_H_8080,  DCR_H_8080,  MVI_H_8080,  dummy,       // 30-49
	dummy,       DAD_H_8080,  LHLD_8080,   DCX_H_8080,  INR_L_8080,  DCR_L_8080,  MVI_L_8080,  CMA_8080,    dummy,       LXI_SP_8080, // 40-49
	STA_8080,    INX_SP_8080, INR_M_8080,  DCR_M_8080,  MVI_M_8080,  STC_8080,    dummy,       DAD_SP_8080, LDA_8080,    DCX_SP_8080, // 50-59
	INR_A_8080,  DCR_A_8080,  MVI_A_8080,  CMC_8080,    MOV_BB_8080, MOV_BC_8080, MOV_BD_8080, MOV_BE_8080, MOV_BH_8080, MOV_BL_8080, // 60-69
	MOV_BM_8080, MOV_BA_8080, MOV_CB_8080, MOV_CC_8080, MOV_CD_8080, MOV_CE_8080, MOV_CH_8080, MOV_CL_8080, MOV_CM_8080, MOV_CA_8080, // 70-79
	MOV_DB_8080, MOV_DC_8080, MOV_DD_8080, MOV_DE_8080, MOV_DH_8080, MOV_DL_8080, MOV_DM_8080, MOV_DA_8080, MOV_EB_8080, MOV_EC_8080, // 80-89
	MOV_ED_8080, MOV_EE_8080, MOV_EH_8080, MOV_EL_8080, MOV_EM_8080, MOV_EA_8080, MOV_HB_8080, MOV_HC_8080, MOV_HD_8080, MOV_HE_8080, // 90-99
	MOV_HH_8080, MOV_HL_8080, MOV_HM_8080, MOV_HA_8080, MOV_LB_8080, MOV_LC_8080, MOV_LD_8080, MOV_LE_8080, MOV_LH_8080, MOV_LL_8080, // 100-109
	MOV_LM_8080, MOV_LA_8080, MOV_MB_8080, MOV_MC_8080, MOV_MD_8080, MOV_ME_8080, MOV_MH_8080, MOV_ML_8080, dummy,       MOV_MA_8080, // 110-119
	MOV_AB_8080, MOV_AC_8080, MOV_AD_8080, MOV_AE_8080, MOV_AH_8080, MOV_AL_8080, MOV_AM_8080, MOV_AA_8080, ADD_B_8080,  ADD_C_8080,  // 120-129
	ADD_D_8080,  ADD_E_8080,  ADD_H_8080,  ADD_L_8080,  ADD_M_8080,  ADD_A_8080,  ADC_B_8080,  ADC_C_8080,  ADC_D_8080,  ADC_E_8080,  // 130-139
	ADC_H_8080,  ADC_L_8080,  ADC_M_8080,  ADC_A_8080,  SUB_B_8080,  SUB_C_8080,  SUB_D_8080,  SUB_E_8080,  SUB_H_8080,  SUB_L_8080,  // 140-149
	SUB_M_8080,  SUB_A_8080,  SBB_B_8080,  SBB_C_8080,  SBB_D_8080,  SBB_E_8080,  SBB_H_8080,  SBB_L_8080,  SBB_M_8080,  SBB_A_8080,  // 150-159
	ANA_B_8080,  ANA_C_8080,  ANA_D_8080,  ANA_E_8080,  ANA_H_8080,  ANA_L_8080,  ANA_M_8080,  ANA_A_8080,  XRA_B_8080,  XRA_C_8080,  // 160-169
	XRA_D_8080,  XRA_E_8080,  XRA_H_8080,  XRA_L_8080,  XRA_M_8080,  XRA_A_8080,  ORA_B_8080,  ORA_C_8080,  ORA_D_8080,  ORA_E_8080,  // 170-179
	ORA_H_8080,  ORA_L_8080,  ORA_M_8080,  ORA_A_8080,  CMP_B_8080,  CMP_C_8080,  CMP_D_8080,  CMP_E_8080,  CMP_H_8080,  CMP_L_8080,  // 180-189
	CMP_M_8080,  CMP_A_8080,  RNZ_8080,    POP_B_8080,  JNZ_8080,    JMP_8080,    CNZ_8080,    PUSH_B_8080, ADI_8080,    RST_0_8080,  // 190-199
	RZ_8080,     RET_8080,    JZ_8080,     dummy,       CZ_8080,     CALL_8080,   dummy,       RST_1_8080,  RNC_8080,     dummy,       // 200-209
	dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       // 210-219
	dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       // 220-229
	dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       // 230-239
	dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       dummy,       // 240-249
	dummy,       dummy,       dummy,       dummy,       dummy,       dummy
};

#endif

