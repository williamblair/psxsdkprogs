#include "test.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

int passedTestsCount = 0;

const int MOV_REG_VALUE = 10;

static inline void testMoveBOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg = MOV_REG_VALUE;
    op(cpu);
    assert(cpu->b == MOV_REG_VALUE);
    printf("MOV_B%c passed\n", regChar);
    passedTestsCount++;
}

static inline void testMoveCOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg = MOV_REG_VALUE;
    op(cpu);
    assert(cpu->c == MOV_REG_VALUE);
    printf("MOV_C%c passed\n", regChar);
    passedTestsCount++;
}

static inline void testMoveDOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg = MOV_REG_VALUE;
    op(cpu);
    assert(cpu->d == MOV_REG_VALUE);
    printf("MOV_D%c passed\n", regChar);
    passedTestsCount++;
}

static inline void testMoveEOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg = MOV_REG_VALUE;
    op(cpu);
    assert(cpu->e == MOV_REG_VALUE);
    printf("MOV_E%c passed\n", regChar);
    passedTestsCount++;
}

static inline void testMoveHOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg = MOV_REG_VALUE;
    op(cpu);
    assert(cpu->h == MOV_REG_VALUE);
    printf("MOV_E%c passed\n", regChar);
    passedTestsCount++;
}

static inline void testMoveLOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg = MOV_REG_VALUE;
    op(cpu);
    assert(cpu->l == MOV_REG_VALUE);
    printf("MOV_E%c passed\n", regChar);
    passedTestsCount++;
}

static inline void testMoveAOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg = MOV_REG_VALUE;
    op(cpu);
    assert(cpu->a == MOV_REG_VALUE);
    printf("MOV_A%c passed\n", regChar);
    passedTestsCount++;
}

static inline void testMoveMOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg = MOV_REG_VALUE;
    cpu->h = 0x20;
    cpu->l = 0x01;
    op(cpu);
    assert(cpu->memory[0x2001] == MOV_REG_VALUE);
    printf("MOV_M%c passed\n", regChar);
    passedTestsCount++;
}

void testMoveOps(Cpu8080 *cpu)
{
    testMoveBOp(cpu, &cpu->a, MOV_BA_8080, 'A');
    testMoveBOp(cpu, &cpu->b, MOV_BB_8080, 'B');
    testMoveBOp(cpu, &cpu->c, MOV_BC_8080, 'C');
    testMoveBOp(cpu, &cpu->d, MOV_BD_8080, 'D');
    testMoveBOp(cpu, &cpu->e, MOV_BE_8080, 'E');
    testMoveBOp(cpu, &cpu->h, MOV_BH_8080, 'H');
    testMoveBOp(cpu, &cpu->l, MOV_BL_8080, 'L');
    
    /* Have to set memory for this one... */
    Reset_8080(cpu);
    cpu->h = 0x20; // BOARD_ROM_SIZE + 1 == 0x2001
    cpu->l = 0x01;
    cpu->memory[BOARD_ROM_SIZE + 1] = 0x12;
    MOV_BM_8080(cpu);
    assert(cpu->b == 0x12);
    printf("MOV_bm: b: 0x%02X  mem: 0x%02X\n", cpu->b, cpu->memory[BOARD_ROM_SIZE+1]);
    passedTestsCount++;
    
    testMoveCOp(cpu, &cpu->a, MOV_CA_8080, 'A');
    testMoveCOp(cpu, &cpu->b, MOV_CB_8080, 'B');
    testMoveCOp(cpu, &cpu->c, MOV_CC_8080, 'C');
    testMoveCOp(cpu, &cpu->d, MOV_CD_8080, 'D');
    testMoveCOp(cpu, &cpu->e, MOV_CE_8080, 'E');
    testMoveCOp(cpu, &cpu->h, MOV_CH_8080, 'H');
    testMoveCOp(cpu, &cpu->l, MOV_CL_8080, 'L');
    
    /* Have to set memory for this one... */
    Reset_8080(cpu);
    cpu->h = 0x20; // BOARD_ROM_SIZE + 1 == 0x2001
    cpu->l = 0x01;
    cpu->memory[BOARD_ROM_SIZE + 1] = 0x12;
    MOV_CM_8080(cpu);
    assert(cpu->c == 0x12);
    printf("MOV_cm: c: 0x%02X  mem: 0x%02X\n", cpu->c, cpu->memory[BOARD_ROM_SIZE+1]);
    passedTestsCount++;
    
    testMoveDOp(cpu, &cpu->a, MOV_DA_8080, 'A');
    testMoveDOp(cpu, &cpu->b, MOV_DB_8080, 'B');
    testMoveDOp(cpu, &cpu->c, MOV_DC_8080, 'C');
    testMoveDOp(cpu, &cpu->d, MOV_DD_8080, 'D');
    testMoveDOp(cpu, &cpu->e, MOV_DE_8080, 'E');
    testMoveDOp(cpu, &cpu->h, MOV_DH_8080, 'H');
    testMoveDOp(cpu, &cpu->l, MOV_DL_8080, 'L');
    
    /* Have to set memory for this one... */
    Reset_8080(cpu);
    cpu->h = 0x20; // BOARD_ROM_SIZE + 1 == 0x2001
    cpu->l = 0x01;
    cpu->memory[BOARD_ROM_SIZE + 1] = 0x12;
    MOV_DM_8080(cpu);
    assert(cpu->d == 0x12);
    printf("MOV_dm: d: 0x%02X  mem: 0x%02X\n", cpu->d, cpu->memory[BOARD_ROM_SIZE+1]);
    passedTestsCount++;
    
    testMoveEOp(cpu, &cpu->a, MOV_EA_8080, 'A');
    testMoveEOp(cpu, &cpu->b, MOV_EB_8080, 'B');
    testMoveEOp(cpu, &cpu->c, MOV_EC_8080, 'C');
    testMoveEOp(cpu, &cpu->d, MOV_ED_8080, 'D');
    testMoveEOp(cpu, &cpu->e, MOV_EE_8080, 'E');
    testMoveEOp(cpu, &cpu->h, MOV_EH_8080, 'H');
    testMoveEOp(cpu, &cpu->l, MOV_EL_8080, 'L');
    
    /* Have to set memory for this one... */
    Reset_8080(cpu);
    cpu->h = 0x20; // BOARD_ROM_SIZE + 1 == 0x2001
    cpu->l = 0x01;
    cpu->memory[BOARD_ROM_SIZE + 1] = 0x12;
    MOV_EM_8080(cpu);
    assert(cpu->e == 0x12);
    printf("MOV_em: e: 0x%02X  mem: 0x%02X\n", cpu->e, cpu->memory[BOARD_ROM_SIZE+1]);
    passedTestsCount++;
    
    testMoveHOp(cpu, &cpu->a, MOV_HA_8080, 'A');
    testMoveHOp(cpu, &cpu->b, MOV_HB_8080, 'B');
    testMoveHOp(cpu, &cpu->c, MOV_HC_8080, 'C');
    testMoveHOp(cpu, &cpu->d, MOV_HD_8080, 'D');
    testMoveHOp(cpu, &cpu->e, MOV_HE_8080, 'E');
    testMoveHOp(cpu, &cpu->h, MOV_HH_8080, 'H');
    testMoveHOp(cpu, &cpu->l, MOV_HL_8080, 'L');
    
    /* Have to set memory for this one... */
    Reset_8080(cpu);
    cpu->h = 0x20; // BOARD_ROM_SIZE + 1 == 0x2001
    cpu->l = 0x01;
    cpu->memory[BOARD_ROM_SIZE + 1] = 0x12;
    MOV_HM_8080(cpu);
    assert(cpu->h == 0x12);
    printf("MOV_hm: h: 0x%02X  mem: 0x%02X\n", cpu->h, cpu->memory[BOARD_ROM_SIZE+1]);
    passedTestsCount++;
    
    testMoveLOp(cpu, &cpu->a, MOV_LA_8080, 'A');
    testMoveLOp(cpu, &cpu->b, MOV_LB_8080, 'B');
    testMoveLOp(cpu, &cpu->c, MOV_LC_8080, 'C');
    testMoveLOp(cpu, &cpu->d, MOV_LD_8080, 'D');
    testMoveLOp(cpu, &cpu->e, MOV_LE_8080, 'E');
    testMoveLOp(cpu, &cpu->h, MOV_LH_8080, 'H');
    testMoveLOp(cpu, &cpu->l, MOV_LL_8080, 'L');
    
    /* Have to set memory for this one... */
    Reset_8080(cpu);
    cpu->h = 0x20; // BOARD_ROM_SIZE + 1 == 0x2001
    cpu->l = 0x01;
    cpu->memory[BOARD_ROM_SIZE + 1] = 0x12;
    MOV_LM_8080(cpu);
    assert(cpu->l == 0x12);
    printf("MOV_lm: l: 0x%02X  mem: 0x%02X\n", cpu->l, cpu->memory[BOARD_ROM_SIZE+1]);
    passedTestsCount++;
    
    testMoveMOp(cpu, &cpu->a, MOV_MA_8080, 'A');
    testMoveMOp(cpu, &cpu->b, MOV_MB_8080, 'B');
    testMoveMOp(cpu, &cpu->c, MOV_MC_8080, 'C');
    testMoveMOp(cpu, &cpu->d, MOV_MD_8080, 'D');
    testMoveMOp(cpu, &cpu->e, MOV_ME_8080, 'E');
    //testMoveMOp(cpu, &cpu->h, MOV_MH_8080, 'H'); // these are special since their values are used in memory loc calculation
    //testMoveMOp(cpu, &cpu->l, MOV_ML_8080, 'L');
    
    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    MOV_MH_8080(cpu);
    assert(cpu->memory[0x2001] == 0x20);
    printf("MOV_MH passed\n");
    passedTestsCount++;
    
    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    MOV_ML_8080(cpu);
    assert(cpu->memory[0x2001] == 0x01);
    printf("MOV_ML passed\n");
    passedTestsCount++;
    
    testMoveAOp(cpu, &cpu->a, MOV_AA_8080, 'A');
    testMoveAOp(cpu, &cpu->b, MOV_AB_8080, 'B');
    testMoveAOp(cpu, &cpu->c, MOV_AC_8080, 'C');
    testMoveAOp(cpu, &cpu->d, MOV_AD_8080, 'D');
    testMoveAOp(cpu, &cpu->e, MOV_AE_8080, 'E');
    testMoveAOp(cpu, &cpu->h, MOV_AH_8080, 'H');
    testMoveAOp(cpu, &cpu->l, MOV_AL_8080, 'L');
    
    /* Have to set memory for this one... */
    Reset_8080(cpu);
    cpu->h = 0x20; // BOARD_ROM_SIZE + 1 == 0x2001
    cpu->l = 0x01;
    cpu->memory[BOARD_ROM_SIZE + 1] = 0x12;
    MOV_AM_8080(cpu);
    assert(cpu->a == 0x12);
    printf("MOV_am: a: 0x%02X  mem: 0x%02X\n", cpu->a, cpu->memory[BOARD_ROM_SIZE+1]);
    passedTestsCount++;
}

static inline void testMviOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    /* Basic functionality */
    Reset_8080(cpu);
    cpu->pc = 0x2001;
    cpu->memory[0x2002] = 0x12;
    op(cpu);
    assert(*reg == 0x12);

    printf("MVI_%c passed\n", regChar);
    passedTestsCount++;
}

void testMviOps(Cpu8080 *cpu)
{
    testMviOp(cpu, &cpu->a, MVI_A_8080, 'A');
    testMviOp(cpu, &cpu->b, MVI_B_8080, 'B');
    testMviOp(cpu, &cpu->c, MVI_C_8080, 'C');
    testMviOp(cpu, &cpu->d, MVI_D_8080, 'D');
    testMviOp(cpu, &cpu->e, MVI_E_8080, 'E');
    testMviOp(cpu, &cpu->h, MVI_H_8080, 'H');
    testMviOp(cpu, &cpu->l, MVI_L_8080, 'L');

    /* Special case */
    Reset_8080(cpu);
    cpu->pc = 0x1234;
    cpu->memory[0x1235] = 10;
    cpu->h = 0x20;
    cpu->l = 0x01;
    MVI_M_8080(cpu);
    assert(cpu->memory[0x2001] == 10);
    printf("MVI_M passed\n");
    passedTestsCount++;
}

static inline void testAddcOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg = 1;
    if (isMemory) {
        cpu->h = 0x20; // for ADC_M only
        cpu->l = 0x01;
    }
    cpu->a = 1;
    cpu->sr[SR_FLAG_C] = 1;
    op(cpu);
    assert(cpu->a == 3);
    printf("ADC_%c passed\n", regChar);
    passedTestsCount++;
}

static inline void testAddcSignFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg = (uint8_t)(-1);
    if (isMemory) {
        cpu->h = 0x20; // for ADC_M only
        cpu->l = 0x01;
    }
    cpu->a = (uint8_t)(-2);
    cpu->sr[SR_FLAG_C] = 1;
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 1);
    
    Reset_8080(cpu);
    *reg = 1;
    if (isMemory) {
        cpu->h = 0x20; // for ADC_M only
        cpu->l = 0x01;
    }
    cpu->a = 1;
    cpu->sr[SR_FLAG_C] = 1;
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 0);
    
    printf("ADC_%c signed flag passed\n", regChar);
    
    passedTestsCount++;
}

static inline void testAddcZeroFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg = (uint8_t)(1);
    if (isMemory) {
        cpu->h = 0x20; // for ADC_M only
        cpu->l = 0x01;
    }
    cpu->a = (uint8_t)(-2);
    cpu->sr[SR_FLAG_C] = 1;
    op(cpu);
    
    if (reg == &cpu->a) assert(cpu->sr[SR_FLAG_Z] == 0);
    else                assert(cpu->sr[SR_FLAG_Z] == 1);
    
    Reset_8080(cpu);
    *reg = 1;
    if (isMemory) {
        cpu->h = 0x20; // for ADC_M only
        cpu->l = 0x01;
    }
    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 0);

    /* For ADC_A */
    if (reg == &cpu->a) {
        Reset_8080(cpu);
        *reg = 0x0;
        cpu->sr[SR_FLAG_C] = 0;
        op(cpu);
        assert(cpu->sr[SR_FLAG_Z] == 1);
    }

    printf("ADC_%c zero flag passed\n", regChar);
    
    passedTestsCount++;
}

static inline void testAddcParityFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg   = 0b0010;
    if (isMemory) {
        cpu->h = 0x20; // for ADC_M only
        cpu->l = 0x01;
    }
    cpu->a = 0b0001; // 0010 + 0001 = 0011 -> even parity
    op(cpu);

    /* For ADC A */
    if (reg == &cpu->a) assert(cpu->sr[SR_FLAG_P] == 0);
    else               assert(cpu->sr[SR_FLAG_P] == 1);
        
    
    Reset_8080(cpu);
    *reg   = 0b0100; // 0100 + 0010 + 0001 = 0111 -> odd parity
    if (isMemory) {
        cpu->h = 0x20; // for ADC_M only
        cpu->l = 0x01;
    }
    cpu->a = 0b0010;
    cpu->sr[SR_FLAG_C] = 0b0001;
    op(cpu);
    if (reg == &cpu->a) assert(cpu->sr[SR_FLAG_P] == 1);
    else               assert(cpu->sr[SR_FLAG_P] == 0);
    
    printf("ADC_%c parity flag passed\n", regChar);
    
    passedTestsCount++;
}

static inline void testAddcCarryFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg = 1;
    if (isMemory) {
        cpu->h = 0x20; // for ADC_M only
        cpu->l = 0x01;
    }
    cpu->sr[SR_FLAG_C] = 1;
    cpu->a = 0xFE;
    op(cpu);
    assert(cpu->sr[SR_FLAG_C] == 1);
    
    Reset_8080(cpu);
    *reg = 1;
    cpu->sr[SR_FLAG_C] = 1;
    cpu->a = 0xFD;
    op(cpu);
    
    if (reg != &cpu->a) assert(cpu->sr[SR_FLAG_C] == 0);
    
    /* For ADC A only */
    else {
        Reset_8080(cpu);
        cpu->a = 1;
        op(cpu);
        assert(cpu->sr[SR_FLAG_C] == 0);
    }
    
    printf("ADC_%c carry flag passed\n", regChar);
}

void testAddcOps(Cpu8080 *cpu)
{
    /* Basic functionality */
    testAddcOp(cpu, &cpu->a, ADC_A_8080, 'A', false);
    testAddcOp(cpu, &cpu->b, ADC_B_8080, 'B', false);
    testAddcOp(cpu, &cpu->c, ADC_C_8080, 'C', false);
    testAddcOp(cpu, &cpu->d, ADC_D_8080, 'D', false);
    testAddcOp(cpu, &cpu->e, ADC_E_8080, 'E', false);
    testAddcOp(cpu, &cpu->h, ADC_H_8080, 'H', false);
    testAddcOp(cpu, &cpu->l, ADC_L_8080, 'L', false);
    testAddcOp(cpu, &cpu->memory[0x2001], ADC_M_8080, 'M', true);

    /* Sign Flag */
    testAddcSignFlag(cpu, &cpu->a, ADC_A_8080, 'A', false);
    testAddcSignFlag(cpu, &cpu->b, ADC_B_8080, 'B', false);
    testAddcSignFlag(cpu, &cpu->c, ADC_C_8080, 'C', false);
    testAddcSignFlag(cpu, &cpu->d, ADC_D_8080, 'D', false);
    testAddcSignFlag(cpu, &cpu->e, ADC_E_8080, 'E', false);
    testAddcSignFlag(cpu, &cpu->h, ADC_H_8080, 'H', false);
    testAddcSignFlag(cpu, &cpu->l, ADC_L_8080, 'L', false);
    testAddcSignFlag(cpu, &cpu->memory[0x2001], ADC_M_8080, 'M', true);

    /* Parity Flag */
    testAddcParityFlag(cpu, &cpu->a, ADC_A_8080, 'A', false);
    testAddcParityFlag(cpu, &cpu->b, ADC_B_8080, 'B', false);
    testAddcParityFlag(cpu, &cpu->c, ADC_C_8080, 'C', false);
    testAddcParityFlag(cpu, &cpu->d, ADC_D_8080, 'D', false);
    testAddcParityFlag(cpu, &cpu->e, ADC_E_8080, 'E', false);
    testAddcParityFlag(cpu, &cpu->h, ADC_H_8080, 'H', false);
    testAddcParityFlag(cpu, &cpu->l, ADC_L_8080, 'L', false);
    testAddcParityFlag(cpu, &cpu->memory[0x2001], ADC_M_8080, 'M', true);

    /* Zero Flag */
    testAddcZeroFlag(cpu, &cpu->a, ADC_A_8080, 'A', false);
    testAddcZeroFlag(cpu, &cpu->b, ADC_B_8080, 'B', false);
    testAddcZeroFlag(cpu, &cpu->c, ADC_C_8080, 'C', false);
    testAddcZeroFlag(cpu, &cpu->d, ADC_D_8080, 'D', false);
    testAddcZeroFlag(cpu, &cpu->e, ADC_E_8080, 'E', false);
    testAddcZeroFlag(cpu, &cpu->h, ADC_H_8080, 'H', false);
    testAddcZeroFlag(cpu, &cpu->l, ADC_L_8080, 'L', false);
    testAddcZeroFlag(cpu, &cpu->memory[0x2001], ADC_M_8080, 'M', true);

    /* Carry Flag */
    testAddcCarryFlag(cpu, &cpu->a, ADC_A_8080, 'A', false);
    testAddcCarryFlag(cpu, &cpu->b, ADC_B_8080, 'B', false);
    testAddcCarryFlag(cpu, &cpu->c, ADC_C_8080, 'C', false);
    testAddcCarryFlag(cpu, &cpu->d, ADC_D_8080, 'D', false);
    testAddcCarryFlag(cpu, &cpu->e, ADC_E_8080, 'E', false);
    testAddcCarryFlag(cpu, &cpu->h, ADC_H_8080, 'H', false);
    testAddcCarryFlag(cpu, &cpu->l, ADC_L_8080, 'L', false);
    testAddcCarryFlag(cpu, &cpu->memory[0x2001], ADC_M_8080, 'M', true);
}

static inline void testSbbOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg = 1;
    if (isMemory) {
        cpu->h = 0x20; // for ADC_M only
        cpu->l = 0x01;
    }
    cpu->a = 5;
    cpu->sr[SR_FLAG_C] = 1;
    op(cpu);

    if (reg == &cpu->a) assert(cpu->a == (uint8_t)(-1));
    else                assert(cpu->a == 3);

    printf("SBB_%c passed\n", regChar);
    passedTestsCount++;
}

static inline void testSbbSignFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg = 1;
    if (isMemory) {
        cpu->h = 0x20; // for ADC_M only
        cpu->l = 0x01;
    }
    cpu->a = 1;
    cpu->sr[SR_FLAG_C] = 1;
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 1);
    
    Reset_8080(cpu);
    *reg = 1;
    if (isMemory) {
        cpu->h = 0x20; // for ADC_M only
        cpu->l = 0x01;
    }
    cpu->a = 5;

    if  (reg == &cpu->a) cpu->sr[SR_FLAG_C] = 0;
    else                 cpu->sr[SR_FLAG_C] = 1;

    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 0);
    
    printf("SBB_%c signed flag passed\n", regChar);
    
    passedTestsCount++;
}

static inline void testSbbZeroFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg = 1;
    if (isMemory) {
        cpu->h = 0x20; // for ADC_M only
        cpu->l = 0x01;
    }
    cpu->a = 2;
    
    if (reg == &cpu->a) cpu->sr[SR_FLAG_C] = 0;
    else                cpu->sr[SR_FLAG_C] = 1;

    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 1);
    
    Reset_8080(cpu);
    *reg = 1;
    if (isMemory) {
        cpu->h = 0x20; // for ADC_M only
        cpu->l = 0x01;
    }
    cpu->a = 5;
    cpu->sr[SR_FLAG_C] = 1;
    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 0);

    printf("SBB_%c zero flag passed\n", regChar);
    
    passedTestsCount++;
}

static inline void testSbbParityFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg   = 0b0001;
    if (isMemory) {
        cpu->h = 0x20; // for SBB_M only
        cpu->l = 0x01;
    }
    cpu->a = 0b0100; // 0100 - 0001 = 0011 -> even parity
    op(cpu);

    assert(cpu->sr[SR_FLAG_P] == 1);
        
    
    Reset_8080(cpu);
    *reg   = 0b0001; // 0010 - 0001 = 0001 -> odd parity
    if (isMemory) {
        cpu->h = 0x20; // for SBB_M only
        cpu->l = 0x01;
    }
    cpu->a = 0b0010;
    op(cpu);
    if (reg == &cpu->a) assert(cpu->sr[SR_FLAG_P] == 1);
    else               assert(cpu->sr[SR_FLAG_P] == 0);
    
    printf("SBB_%c parity flag passed\n", regChar);
    
    passedTestsCount++;
}

static inline void testSbbCarryFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg = 1;
    if (isMemory) {
        cpu->h = 0x20; // for SBB_M only
        cpu->l = 0x01;
    }
    cpu->sr[SR_FLAG_C] = 1;
    cpu->a = 0x00;
    op(cpu);
    assert(cpu->sr[SR_FLAG_C] == 1);
    
    Reset_8080(cpu);
    *reg = 1;
    cpu->a = 0xFD;
    op(cpu);
    
    if (reg != &cpu->a) assert(cpu->sr[SR_FLAG_C] == 0);
    
    printf("ADC_%c carry flag passed\n", regChar);
}

void testSbbOps(Cpu8080 *cpu)
{
    /* Basic functionality */
    testSbbOp(cpu, &cpu->a, SBB_A_8080, 'A', false);
    testSbbOp(cpu, &cpu->b, SBB_B_8080, 'B', false);
    testSbbOp(cpu, &cpu->c, SBB_C_8080, 'C', false);
    testSbbOp(cpu, &cpu->d, SBB_D_8080, 'D', false);
    testSbbOp(cpu, &cpu->e, SBB_E_8080, 'E', false);
    testSbbOp(cpu, &cpu->h, SBB_H_8080, 'H', false);
    testSbbOp(cpu, &cpu->l, SBB_L_8080, 'L', false);
    testSbbOp(cpu, &cpu->memory[0x2001], SBB_M_8080, 'M', true);

    /* Sign Flag */
    testSbbSignFlag(cpu, &cpu->a, SBB_A_8080, 'A', false);
    testSbbSignFlag(cpu, &cpu->b, SBB_B_8080, 'B', false);
    testSbbSignFlag(cpu, &cpu->c, SBB_C_8080, 'C', false);
    testSbbSignFlag(cpu, &cpu->d, SBB_D_8080, 'D', false);
    testSbbSignFlag(cpu, &cpu->e, SBB_E_8080, 'E', false);
    testSbbSignFlag(cpu, &cpu->h, SBB_H_8080, 'H', false);
    testSbbSignFlag(cpu, &cpu->l, SBB_L_8080, 'L', false);
    testSbbSignFlag(cpu, &cpu->memory[0x2001], SBB_M_8080, 'M', true);

    /* Parity Flag */
    testSbbParityFlag(cpu, &cpu->a, SBB_A_8080, 'A', false);
    testSbbParityFlag(cpu, &cpu->b, SBB_B_8080, 'B', false);
    testSbbParityFlag(cpu, &cpu->c, SBB_C_8080, 'C', false);
    testSbbParityFlag(cpu, &cpu->d, SBB_D_8080, 'D', false);
    testSbbParityFlag(cpu, &cpu->e, SBB_E_8080, 'E', false);
    testSbbParityFlag(cpu, &cpu->h, SBB_H_8080, 'H', false);
    testSbbParityFlag(cpu, &cpu->l, SBB_L_8080, 'L', false);
    testSbbParityFlag(cpu, &cpu->memory[0x2001], SBB_M_8080, 'M', true);

    /* Zero Flag */
    testSbbZeroFlag(cpu, &cpu->a, SBB_A_8080, 'A', false);
    testSbbZeroFlag(cpu, &cpu->b, SBB_B_8080, 'B', false);
    testSbbZeroFlag(cpu, &cpu->c, SBB_C_8080, 'C', false);
    testSbbZeroFlag(cpu, &cpu->d, SBB_D_8080, 'D', false);
    testSbbZeroFlag(cpu, &cpu->e, SBB_E_8080, 'E', false);
    testSbbZeroFlag(cpu, &cpu->h, SBB_H_8080, 'H', false);
    testSbbZeroFlag(cpu, &cpu->l, SBB_L_8080, 'L', false);
    testSbbZeroFlag(cpu, &cpu->memory[0x2001], SBB_M_8080, 'M', true);

    /* Carry Flag */
    testSbbCarryFlag(cpu, &cpu->a, SBB_A_8080, 'A', false);
    testSbbCarryFlag(cpu, &cpu->b, SBB_B_8080, 'B', false);
    testSbbCarryFlag(cpu, &cpu->c, SBB_C_8080, 'C', false);
    testSbbCarryFlag(cpu, &cpu->d, SBB_D_8080, 'D', false);
    testSbbCarryFlag(cpu, &cpu->e, SBB_E_8080, 'E', false);
    testSbbCarryFlag(cpu, &cpu->h, SBB_H_8080, 'H', false);
    testSbbCarryFlag(cpu, &cpu->l, SBB_L_8080, 'L', false);
    testSbbCarryFlag(cpu, &cpu->memory[0x2001], SBB_M_8080, 'M', true);
}

static inline void testAddOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg = 1;
    cpu->a = 1;
    op(cpu);
    assert(cpu->a == 2);
    printf("ADD_%c passed\n", regChar);
    passedTestsCount++;
}

static inline void testAddSignFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg = (uint8_t)(-1);
    cpu->a = (uint8_t)(-2);
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 1);
    
    Reset_8080(cpu);
    *reg = 1;
    cpu->a = 1;
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 0);
    
    printf("ADD_%c signed flag passed\n", regChar);
    
    passedTestsCount++;
}

static inline void testAddZeroFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg = (uint8_t)(1);
    cpu->a = (uint8_t)(-1);
    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 1);
    
    Reset_8080(cpu);
    *reg = 1;
    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 0);

    printf("ADD_%c zero flag passed\n", regChar);
    
    passedTestsCount++;
}

static inline void testAddParityFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg   = 0b0010;
    cpu->a = 0b0001; // 0010 + 0001 = 0011 -> even parity
    op(cpu);
    assert(cpu->sr[SR_FLAG_P] == 1);
    
    Reset_8080(cpu);
    *reg = 0b0010; // 0010 + 0000 = 0010 -> odd parity
    op(cpu);
    assert(cpu->sr[SR_FLAG_P] == 0);
    
    printf("ADD_%c parity flag passed\n", regChar);
    
    passedTestsCount++;
}

static inline void testAddCarryFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg = 1;
    cpu->a = 0xFF;
    op(cpu);
    assert(cpu->sr[SR_FLAG_C] == 1);
    
    Reset_8080(cpu);
    *reg = 1;
    cpu->a = 0xFE;
    op(cpu);
    assert(cpu->sr[SR_FLAG_C] == 0);
    
    printf("ADD_%c carry flag passed\n", regChar);
}

void testAddOps(Cpu8080 *cpu)
{
    /* Make sure the basic functionality works */
    printf("\n");
    testAddOp(cpu, &cpu->a, ADD_A_8080, 'A');
    testAddOp(cpu, &cpu->b, ADD_B_8080, 'B');
    testAddOp(cpu, &cpu->c, ADD_C_8080, 'C');
    testAddOp(cpu, &cpu->d, ADD_D_8080, 'D');
    testAddOp(cpu, &cpu->e, ADD_E_8080, 'E');
    testAddOp(cpu, &cpu->h, ADD_H_8080, 'H');
    testAddOp(cpu, &cpu->l, ADD_L_8080, 'L');
    
    /* Make sure the sign flag is set properly */
    testAddSignFlag(cpu, &cpu->a, ADD_A_8080, 'A');
    testAddSignFlag(cpu, &cpu->b, ADD_B_8080, 'B');
    testAddSignFlag(cpu, &cpu->c, ADD_C_8080, 'C');
    testAddSignFlag(cpu, &cpu->d, ADD_D_8080, 'D');
    testAddSignFlag(cpu, &cpu->e, ADD_E_8080, 'E');
    testAddSignFlag(cpu, &cpu->h, ADD_H_8080, 'H');
    testAddSignFlag(cpu, &cpu->l, ADD_L_8080, 'L');
    
    /* Make sure the zero flag is set properly */
    //testAddZeroFlag(cpu, &cpu->a, ADD_A_8080, 'A'); // skip a+a since it will only be zero if a is zero
    testAddZeroFlag(cpu, &cpu->b, ADD_B_8080, 'B');
    testAddZeroFlag(cpu, &cpu->c, ADD_C_8080, 'C');
    testAddZeroFlag(cpu, &cpu->d, ADD_D_8080, 'D');
    testAddZeroFlag(cpu, &cpu->e, ADD_E_8080, 'E');
    testAddZeroFlag(cpu, &cpu->h, ADD_H_8080, 'H');
    testAddZeroFlag(cpu, &cpu->l, ADD_L_8080, 'L');
    
    /* Make sure the parity flag is set properly */
    //testAddParityFlag(cpu, &cpu->a, ADD_A_8080, 'A'); // a we need to test separately
    Reset_8080(cpu);
    cpu->a = 0b0001;
    ADD_A_8080(cpu);
    assert(cpu->sr[SR_FLAG_P] == 0);
    passedTestsCount++;
    printf("ADD_A parity flag passed\n");
    testAddParityFlag(cpu, &cpu->b, ADD_B_8080, 'B');
    testAddParityFlag(cpu, &cpu->c, ADD_C_8080, 'C');
    testAddParityFlag(cpu, &cpu->d, ADD_D_8080, 'D');
    testAddParityFlag(cpu, &cpu->e, ADD_E_8080, 'E');
    testAddParityFlag(cpu, &cpu->h, ADD_H_8080, 'H');
    testAddParityFlag(cpu, &cpu->l, ADD_L_8080, 'L');
    
    /* Make sure the carry flag is set properly */
    Reset_8080(cpu);
    cpu->a = 150;
    ADD_A_8080(cpu);
    assert(cpu->sr[SR_FLAG_C] == 1);
    cpu->a = 1;
    ADD_A_8080(cpu);
    assert(cpu->sr[SR_FLAG_C] == 0);
    printf("ADD_A carry flag passed\n");
    passedTestsCount++;
    testAddCarryFlag(cpu, &cpu->b, ADD_B_8080, 'B');
    testAddCarryFlag(cpu, &cpu->c, ADD_C_8080, 'C');
    testAddCarryFlag(cpu, &cpu->d, ADD_D_8080, 'D');
    testAddCarryFlag(cpu, &cpu->e, ADD_E_8080, 'E');
    testAddCarryFlag(cpu, &cpu->h, ADD_H_8080, 'H');
    testAddCarryFlag(cpu, &cpu->l, ADD_L_8080, 'L');
}

static inline void testAndOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg   = 0b0011;
    cpu->a = 0b0110;
    op(cpu);
    assert(cpu->a == 0b0010);
    printf("ANA_%c passed\n", regChar);
    passedTestsCount++;
}

static inline void testAndParityFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg   = 0b0111;
    cpu->a = 0b0101; // 0111 & 0101 = 0101 -> even parity
    op(cpu);
    assert(cpu->sr[SR_FLAG_P] == 1);
    
    Reset_8080(cpu);
    *reg   = 0b0010;
    cpu->a = 0b0010; // 0010 & 0010 = 0010 -> odd parity
    op(cpu);
    assert(cpu->sr[SR_FLAG_P] == 0);
    
    printf("ANA_%c parity flag passed\n", regChar);
    
    passedTestsCount++;
}

static inline void testAndZeroFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg   = 0b0101;
    cpu->a = 0b0000; // 0000 & 0101 = 0 -> zero flag
    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 1);
    
    Reset_8080(cpu);
    *reg   = 0b0010;
    cpu->a = 0b0010; // 0010 & 0010 = 0010 -> ! zero flag
    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 0);
    
    printf("ANA_%c zero flag passed\n", regChar);
    
    passedTestsCount++;
}

static inline void testAndSignFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg  = 0b10001011;
    cpu->a= 0b10000001;
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 1);
    
    Reset_8080(cpu);
    *reg   = 0b00001011;
    cpu->a = 0b10000011;
    assert(cpu->sr[SR_FLAG_S] == 0);
    
    printf("ANA_%c sign flag passed\n", regChar);
    
    passedTestsCount++;
}

void testAndOps(Cpu8080 *cpu)
{
    /* Make sure basic functionality works */
    printf("\n");
    Reset_8080(cpu);
    cpu->a = 0b0010;
    ANA_A_8080(cpu);
    assert(cpu->a == 0b0010);
    printf("ANA_A passed\n");
    passedTestsCount++;
    testAndOp(cpu, &cpu->b, ANA_B_8080, 'B');
    testAndOp(cpu, &cpu->c, ANA_C_8080, 'C');
    testAndOp(cpu, &cpu->d, ANA_D_8080, 'D');
    testAndOp(cpu, &cpu->e, ANA_E_8080, 'E');
    testAndOp(cpu, &cpu->h, ANA_H_8080, 'H');
    testAndOp(cpu, &cpu->l, ANA_L_8080, 'L');
    
    /* Special memory case */
    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b0011;
    cpu->a              = 0b0110;
    ANA_M_8080(cpu);
    assert(cpu->a == 0b0010);
    printf("ANA_M passed\n");
    passedTestsCount++;
    
    /* Test parity flag */
    testAndParityFlag(cpu, &cpu->a, ANA_A_8080, 'A');
    testAndParityFlag(cpu, &cpu->b, ANA_B_8080, 'B');
    testAndParityFlag(cpu, &cpu->c, ANA_C_8080, 'C');
    testAndParityFlag(cpu, &cpu->d, ANA_D_8080, 'D');
    testAndParityFlag(cpu, &cpu->e, ANA_E_8080, 'E');
    testAndParityFlag(cpu, &cpu->h, ANA_H_8080, 'H');
    testAndParityFlag(cpu, &cpu->l, ANA_L_8080, 'L');
    
    /* Special memory case */
    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b0011; // 0b0011 & 0b0010 = 0b0010 -> odd parity
    cpu->a              = 0b0010;
    ANA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_P] == 0);
    
    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b0111; // 0b0111 & 0b0110 = 0b0110 -> even parity
    cpu->a              = 0b0110;
    ANA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_P] == 1);
    printf("ANA_M parity flag passed\n");
    passedTestsCount++;
    
    /* Test zero flag */
    testAndZeroFlag(cpu, &cpu->a, ANA_A_8080, 'A');
    testAndZeroFlag(cpu, &cpu->b, ANA_B_8080, 'B');
    testAndZeroFlag(cpu, &cpu->c, ANA_C_8080, 'C');
    testAndZeroFlag(cpu, &cpu->d, ANA_D_8080, 'D');
    testAndZeroFlag(cpu, &cpu->e, ANA_E_8080, 'E');
    testAndZeroFlag(cpu, &cpu->h, ANA_H_8080, 'H');
    testAndZeroFlag(cpu, &cpu->l, ANA_L_8080, 'L');
    
    /* Special memory case */
    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b0011;
    cpu->a = 0b0000;
    ANA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 1);
    
    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b0011;
    cpu->a = 0b0011;
    ANA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 0);
    
    printf("ANA_M zero flag passed\n");
    passedTestsCount++;
    
    testAndSignFlag(cpu, &cpu->b, ANA_B_8080, 'B');
    testAndSignFlag(cpu, &cpu->c, ANA_C_8080, 'C');
    testAndSignFlag(cpu, &cpu->d, ANA_D_8080, 'D');
    testAndSignFlag(cpu, &cpu->e, ANA_E_8080, 'E');
    testAndSignFlag(cpu, &cpu->h, ANA_H_8080, 'H');
    testAndSignFlag(cpu, &cpu->l, ANA_L_8080, 'L');
    
    /* Special A&A case */
    Reset_8080(cpu);
    cpu->a = 0b10001011;
    ANA_A_8080(cpu);
    assert(cpu->sr[SR_FLAG_S] == 1);
    
    Reset_8080(cpu);
    cpu->a = 0b00001011;
    ANA_A_8080(cpu);
    assert(cpu->sr[SR_FLAG_S] == 0);
    
    printf("ANA_A sign flag passed\n");
    passedTestsCount++;
    
    /* Special memory case */
    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b10001011;
    cpu->a              = 0b10000001;
    ANA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_S] == 1);
    
    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b10001011;
    cpu->a              = 0b00000001;
    ANA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_S] == 0);
    
    printf("ANA_M sign flag passed\n");
    passedTestsCount++;
}

static inline void testXorOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg =   0b1011;
    cpu->a = 0b1101;
    op(cpu);
    assert(cpu->a == 0b0110);

    printf("XRA_%c passed\n", regChar);

    passedTestsCount++;
}

static inline void testXorParityFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg   = 0b0011; // 0b0011 ^ 0b1100 = 0b1111 -> even parity
    cpu->a = 0b1100;
    op(cpu);
    assert(cpu->sr[SR_FLAG_P] == 1);

    Reset_8080(cpu);
    *reg   = 0b1110; // 0b1110 ^ 0b1111 = 0b0001 -> odd parity
    cpu->a = 0b1111;
    op(cpu);
    assert(cpu->sr[SR_FLAG_P] == 0);

    printf("XRA_%c parity flag passed\n", regChar);

    passedTestsCount++;
}

static inline void testXorZeroFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg   = 0b1011; // 0b1011 ^ 0b1011 = 0
    cpu->a = 0b1011;
    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 1);

    Reset_8080(cpu);
    *reg   = 0b1110; // 0b1110 ^ 0b1111 = 1
    cpu->a = 0b1111;
    op(cpu);
    assert(cpu->sr[SR_FLAG_P] == 0);

    printf("XRA_%c zero flag passed\n", regChar);

    passedTestsCount++;
}

static inline void testXorSignFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg   = 0b10010110;
    cpu->a = 0b01001011;
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 1);

    Reset_8080(cpu);
    *reg   = 0b10010110;
    cpu->a = 0b11001011;
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 0);

    printf("XRA_%c sign flag passed\n", regChar);
    passedTestsCount++;
}

void testXorOps(Cpu8080 *cpu)
{
    printf("\n");

    /* Basic functionality */
    Reset_8080(cpu);
    cpu->a = 0b00101101;
    XRA_A_8080(cpu); // XOR a with itself should be 0
    assert(cpu->a == 0);
    printf("XRA_A passed\n");
    passedTestsCount++;
    testXorOp(cpu, &cpu->b, XRA_B_8080, 'B');
    testXorOp(cpu, &cpu->c, XRA_C_8080, 'C');
    testXorOp(cpu, &cpu->d, XRA_D_8080, 'D');
    testXorOp(cpu, &cpu->e, XRA_E_8080, 'E');
    testXorOp(cpu, &cpu->h, XRA_H_8080, 'H');
    testXorOp(cpu, &cpu->l, XRA_L_8080, 'L');

    /* Special memory case */
    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b0011;
    cpu->a = 0b1110;
    XRA_M_8080(cpu);
    assert(cpu->a == 0b1101);
    printf("XRA_M passed\n");
    passedTestsCount++;

    /* Parity flag */
    Reset_8080(cpu);
    cpu->a = 0b00101101;
    XRA_A_8080(cpu);
    assert(cpu->sr[SR_FLAG_P] == 1); // 0 has even parity
    printf("XRA_A parity flag passed\n");
    passedTestsCount++;
    testXorParityFlag(cpu, &cpu->b, XRA_B_8080, 'B');
    testXorParityFlag(cpu, &cpu->c, XRA_C_8080, 'C');
    testXorParityFlag(cpu, &cpu->d, XRA_D_8080, 'D');
    testXorParityFlag(cpu, &cpu->e, XRA_E_8080, 'E');
    testXorParityFlag(cpu, &cpu->h, XRA_H_8080, 'H');
    testXorParityFlag(cpu, &cpu->l, XRA_L_8080, 'L');

    /* Special memory case */
    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b0011; // 0b0011 ^ 0b1110 = 1101 -> odd parity
    cpu->a              = 0b1110;
    XRA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_P] == 0);

    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b1010; // 0b1010 ^ 0b1111 = 0101 -> even parity
    cpu->a              = 0b1111;
    XRA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_P] == 1);

    printf("XRA_M parity flag passed\n");
    passedTestsCount++;testXorZeroFlag(cpu, &cpu->b, XRA_B_8080, 'B');

    /* Test zero flag */
    Reset_8080(cpu);
    cpu->a = 0b0110;
    XRA_A_8080(cpu); // Zero flag should always be set since A XOR A = 0
    assert(cpu->sr[SR_FLAG_Z] == 1);
    printf("XRA_A zero flag passed\n");
    passedTestsCount++;

    testXorZeroFlag(cpu, &cpu->b, XRA_B_8080, 'B');
    testXorZeroFlag(cpu, &cpu->c, XRA_C_8080, 'C');
    testXorZeroFlag(cpu, &cpu->d, XRA_D_8080, 'D');
    testXorZeroFlag(cpu, &cpu->e, XRA_E_8080, 'E');
    testXorZeroFlag(cpu, &cpu->h, XRA_H_8080, 'H');
    testXorZeroFlag(cpu, &cpu->l, XRA_L_8080, 'L');

    /* Special memory case */
    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b1101;
    cpu->a              = 0b1101;
    XRA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 1);

    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b1101;
    cpu->a              = 0b1111;
    XRA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 0);

    printf("XRA_M zero flag passed\n");
    passedTestsCount++;

    /* Sign flag */
    Reset_8080(cpu);
    cpu->a = 0b10110111; // sign flag for A XOR A should always be 0
    XRA_A_8080(cpu);
    assert(cpu->a == 0);
    printf("XRA_A sign flag passed\n");
    passedTestsCount++;

    testXorSignFlag(cpu, &cpu->b, XRA_B_8080, 'B');
    testXorSignFlag(cpu, &cpu->c, XRA_C_8080, 'C');
    testXorSignFlag(cpu, &cpu->d, XRA_D_8080, 'D');
    testXorSignFlag(cpu, &cpu->e, XRA_E_8080, 'E');
    testXorSignFlag(cpu, &cpu->h, XRA_H_8080, 'H');
    testXorSignFlag(cpu, &cpu->l, XRA_L_8080, 'L');

    /* Special memory case */
    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b10101111;
    cpu->a              = 0b01011111;
    XRA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_S] == 1);

    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b10101111;
    cpu->a              = 0b11011111;
    XRA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_S] == 0);

    printf("XRA_M sign flag passed\n");
    passedTestsCount++;
}

static inline void testOrOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg   = 0b0101;
    cpu->a = 0b1100;
    op(cpu);
    assert(cpu->a == 0b1101);

    printf("ORA_%c passed\n", regChar);
}

static inline void testOrSignFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg   = 0b00001011;
    cpu->a = 0b11101111;
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 1);

    Reset_8080(cpu);
    *reg   = 0b00001011;
    cpu->a = 0b01101111;
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 0);

    printf("ORA_%c sign flag passed\n", regChar);
    passedTestsCount++;
}

static inline void testOrParityFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg   = 0b00011011; // odd parity
    cpu->a = 0b00001110;
    op(cpu);
    assert(cpu->sr[SR_FLAG_P] == 0);

    Reset_8080(cpu);
    *reg   = 0b01010011; // even parity
    cpu->a = 0b00001100;
    op(cpu);
    assert(cpu->sr[SR_FLAG_P] == 1);

    printf("ORA_%c parity flag passed\n", regChar);
    passedTestsCount++;
}

static inline void testOrZeroFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    Reset_8080(cpu);
    *reg   = 0b00000000;
    cpu->a = 0b00000000;
    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 1);

    Reset_8080(cpu);
    *reg   = 0b00000000;
    cpu->a = 0b00001000;
    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 0);
}

void testOrOps(Cpu8080 *cpu)
{
    /* Basic functionality */
    printf("\n");
    Reset_8080(cpu);
    cpu->a = 0b1101;
    ORA_A_8080(cpu);
    assert(cpu->a == 0b1101);
    printf("ORA_A passed\n");
    passedTestsCount++;
    testOrOp(cpu, &cpu->b, ORA_B_8080, 'B');
    testOrOp(cpu, &cpu->c, ORA_C_8080, 'C');
    testOrOp(cpu, &cpu->d, ORA_D_8080, 'D');
    testOrOp(cpu, &cpu->e, ORA_E_8080, 'E');
    testOrOp(cpu, &cpu->h, ORA_H_8080, 'H');
    testOrOp(cpu, &cpu->l, ORA_L_8080, 'L');

    /* Special memory case */
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b0101;
    cpu->a              = 0b1100;
    ORA_M_8080(cpu);
    assert(cpu->a == 0b1101);
    printf("ORA_M passed\n");
    passedTestsCount++;

    /* Zero flag */
    testOrZeroFlag(cpu, &cpu->a, ORA_A_8080, 'A');
    testOrZeroFlag(cpu, &cpu->b, ORA_B_8080, 'B');
    testOrZeroFlag(cpu, &cpu->c, ORA_C_8080, 'C');
    testOrZeroFlag(cpu, &cpu->d, ORA_D_8080, 'D');
    testOrZeroFlag(cpu, &cpu->e, ORA_E_8080, 'E');
    testOrZeroFlag(cpu, &cpu->h, ORA_H_8080, 'H');
    testOrZeroFlag(cpu, &cpu->l, ORA_L_8080, 'L');

    /* Special memory case */
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b0000;
    cpu->a              = 0b0000;
    ORA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 1);

    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b1000;
    cpu->a              = 0b0000;
    ORA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 0);

    printf("ORA_M zero flag passed\n");
    passedTestsCount++;

    /* Sign flag*/
    testOrSignFlag(cpu, &cpu->a, ORA_A_8080, 'A');
    testOrSignFlag(cpu, &cpu->b, ORA_B_8080, 'B');
    testOrSignFlag(cpu, &cpu->c, ORA_C_8080, 'C');
    testOrSignFlag(cpu, &cpu->d, ORA_D_8080, 'D');
    testOrSignFlag(cpu, &cpu->e, ORA_E_8080, 'E');
    testOrSignFlag(cpu, &cpu->h, ORA_H_8080, 'H');
    testOrSignFlag(cpu, &cpu->l, ORA_L_8080, 'L');

    /* Special memory case */
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b10001011;
    cpu->a              = 0b00000100;
    ORA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_S] == 1);

    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b00011000;
    cpu->a              = 0b01100000;
    ORA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_S] == 0);

    printf("ORA_M sign flag passed\n");
    passedTestsCount++;

    /* Parity flag */
    testOrParityFlag(cpu, &cpu->a, ORA_A_8080, 'A');
    testOrParityFlag(cpu, &cpu->b, ORA_B_8080, 'B');
    testOrParityFlag(cpu, &cpu->c, ORA_C_8080, 'C');
    testOrParityFlag(cpu, &cpu->d, ORA_D_8080, 'D');
    testOrParityFlag(cpu, &cpu->e, ORA_E_8080, 'E');
    testOrParityFlag(cpu, &cpu->h, ORA_H_8080, 'H');
    testOrParityFlag(cpu, &cpu->l, ORA_L_8080, 'L');

    /* Special memory case */
    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b0010; // odd parity
    cpu->a              = 0b1100;
    ORA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_P] == 0);

    cpu->h = 0x20;
    cpu->l = 0x01;
    cpu->memory[0x2001] = 0b0010; // even parity
    cpu->a              = 0b1000;
    ORA_M_8080(cpu);
    assert(cpu->sr[SR_FLAG_P] == 1);

    printf("ORA_M parity flag passed\n");
    passedTestsCount++;
}

static inline void testCmpOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar)
{
    /* Test the zero flag */
    if(regChar != 'M') Reset_8080(cpu);
    *reg   = 1;
    cpu->a = 1;
    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 1);


    if (regChar != 'M') Reset_8080(cpu);
    *reg = 10;
    cpu->a = 20;
    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 0);

    /* Test Sign flag */
    if (regChar != 'M') Reset_8080(cpu);
    *reg   = 0b00000001; // 0b10000001 - 0b00000001 = 0b10000000
    cpu->a = 0b10000001;
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 1);

    if (regChar != 'M') Reset_8080(cpu);
    *reg   = 0b00000001; // 0b00000011 - 0b00000001 = 0b00000010
    cpu->a = 0b00000011;
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 0);

    /* Test the Parity flag */
    if (regChar != 'M') Reset_8080(cpu);
    *reg   = 0b0001; // odd parity
    cpu->a = 0b0011;
    op(cpu);
    assert(cpu->sr[SR_FLAG_P] == 0);

    if (regChar != 'M') Reset_8080(cpu);
    *reg   = 0b0001; // even parity
    cpu->a = 0b0111;
    op(cpu);
    assert(cpu->sr[SR_FLAG_P] == 1);

    /* Test the carry flag */
    if (regChar != 'M') Reset_8080(cpu);
    *reg   = 0b0010; // A < r
    cpu->a = 0b0001;
    op(cpu);
    assert(cpu->sr[SR_FLAG_C] == 1);

    if (regChar != 'M') Reset_8080(cpu);
    *reg   = 0b0001; // A >= r
    cpu->a = 0b0010;
    op(cpu);
    assert(cpu->sr[SR_FLAG_C] == 0);

    printf("CMP_%c passed\n", regChar);
    passedTestsCount++;
}

void testCmpOps(Cpu8080 *cpu)
{
    printf("\n");
    testCmpOp(cpu, &cpu->b, CMP_B_8080, 'B');
    testCmpOp(cpu, &cpu->c, CMP_C_8080, 'C');
    testCmpOp(cpu, &cpu->d, CMP_D_8080, 'D');
    testCmpOp(cpu, &cpu->e, CMP_E_8080, 'E');
    testCmpOp(cpu, &cpu->h, CMP_H_8080, 'H');
    testCmpOp(cpu, &cpu->l, CMP_L_8080, 'L');

    /* Special cases */
    Reset_8080(cpu);
    cpu->a = 1;
    CMP_A_8080(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 1); // A-A = 0
    assert(cpu->sr[SR_FLAG_P] == 1); // 0 -> even parity
    assert(cpu->sr[SR_FLAG_S] == 0); // 0 != negative
    assert(cpu->sr[SR_FLAG_C] == 0); // 0 -> no carry
    printf("CMP_A passed\n");
    passedTestsCount++;

    Reset_8080(cpu);
    cpu->h = 0x20;
    cpu->l = 0x01;
    testCmpOp(cpu, &cpu->memory[(cpu->h << 8) | cpu->l], CMP_M_8080, 'M');
}

static inline void testPopOp(Cpu8080 *cpu, uint8_t *reg1, uint8_t *reg2, Op_8080 op, char *regPair)
{
    Reset_8080(cpu);
    cpu->sp = 0x2001;
    cpu->memory[0x2001] = 0x12;
    cpu->memory[0x2002] = 0x34;
    op(cpu);
    assert(*reg1 == 0x34);
    assert(*reg2 == 0x12);
    assert(cpu->sp == 0x2003);

    printf("POP_%s passed\n", regPair);
    passedTestsCount++;
}

void testPopOps(Cpu8080 *cpu)
{
    testPopOp(cpu, &cpu->b, &cpu->c, POP_B_8080, "BC");
}

static inline void testPushOp(Cpu8080 *cpu, uint8_t *reg1, uint8_t *reg2, Op_8080 op, char *regPair)
{
    Reset_8080(cpu);
    *reg1 = 0x12;
    *reg2 = 0x34;
    cpu->sp = 0x2004;
    op(cpu);
    assert(cpu->memory[0x2003] == 0x12);
    assert(cpu->memory[0x2002] == 0x34);
    assert(cpu->sp == 0x2002);

    printf("PUSH %s passed\n", regPair);
    passedTestsCount++;
}

void testPushOps(Cpu8080 *cpu)
{
    testPushOp(cpu, &cpu->b, &cpu->c, PUSH_B_8080, "BC");
}

static inline void testStaxOp(Cpu8080 *cpu, uint8_t *reg1, uint8_t *reg2, Op_8080 op, char *regChars)
{
    //printf("\n");
    Reset_8080(cpu);
    *reg1 = 0x20;
    *reg2 = 0x01;
    cpu->a = 1;
    //STAX_B_8080(cpu);
    op(cpu);
    assert(cpu->memory[0x2001] == 1);
    printf("STAX_%s passed\n", regChars);
    passedTestsCount++;
}

void testStaxOps(Cpu8080 *cpu)
{
    testStaxOp(cpu, &cpu->b, &cpu->c, STAX_B_8080, "BC");
    testStaxOp(cpu, &cpu->d, &cpu->e, STAX_D_8080, "DE");
}

void testStaOp(Cpu8080 *cpu)
{
    /* Basic functionality */
    Reset_8080(cpu);
    cpu->pc = 0x2001;
    cpu->a = 10;
    cpu->memory[0x2002] = 0x34;
    cpu->memory[0x2003] = 0x12;
    STA_8080(cpu);
    assert(cpu->memory[0x1234] == 10);
    
    printf("STA passed\n");
    passedTestsCount++;
}

static inline void testInxOp(Cpu8080 *cpu, uint8_t *reg1, uint8_t *reg2, Op_8080 op, char *regChars)
{
    Reset_8080(cpu);
    *reg1 = 0x12;
    *reg2 = 0x34;
    op(cpu);
    assert(*reg1 == 0x12);
    assert(*reg2 == 0x35);

    printf("INX_%s passed\n", regChars);
    passedTestsCount++;
}

void testInxOps(Cpu8080 *cpu)
{
    testInxOp(cpu, &cpu->b, &cpu->c, INX_B_8080, "BC");
    testInxOp(cpu, &cpu->d, &cpu->e, INX_D_8080, "DE");
    testInxOp(cpu, &cpu->h, &cpu->l, INX_H_8080, "HL");
    
    /* Special case since the stack pointer is 2 bytes already */
    Reset_8080(cpu);
    cpu->sp = 0xFFFF; // overflow to zero
    INX_SP_8080(cpu);
    assert(cpu->sp == 0);
    printf("INX_SP passed\n");
    passedTestsCount++;
}

static inline void testInrOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg = 1;
    if (isMemory) { cpu->h = 0x12; cpu->l = 0x34; }// for INR_M only
    op(cpu);
    assert(*reg == 2);
    printf("INR_%c passed\n", regChar);
    passedTestsCount++;
}

static inline void testInrZeroFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg = (uint8_t)(-1);
    if (isMemory) { cpu->h = 0x12; cpu->l = 0x34; }// for INR_M only
    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 1);

    Reset_8080(cpu);
    *reg = (uint8_t)(1);
    if (isMemory) { cpu->h = 0x12; cpu->l = 0x34; }// for INR_M only
    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 0);

    printf("INR_%c zero flag passed\n", regChar);
    passedTestsCount++;
}

static inline void testInrSignFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg = (uint8_t)(-2);
    if (isMemory) { cpu->h = 0x12; cpu->l = 0x34; }// for INR_M only
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 1);

    Reset_8080(cpu);
    *reg = (uint8_t)(1);
    if (isMemory) { cpu->h = 0x12; cpu->l = 0x34; }// for INR_M only
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 0);

    printf("INR_%c sign flag passed\n", regChar);
    passedTestsCount++;
}

static inline void testInrParityFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg = 0b0010;    // even parity
    if (isMemory) { cpu->h = 0x12; cpu->l = 0x34; }// for INR_M only
    op(cpu);
    assert(cpu->sr[SR_FLAG_P] == 1);

    Reset_8080(cpu);
    *reg = 0b0000;    // odd parity
    if (isMemory) { cpu->h = 0x12; cpu->l = 0x34; }// for INR_M only
    op(cpu);
    assert(cpu->sr[SR_FLAG_P] == 0);

    printf("INR_%c parity flag passed\n", regChar);
    passedTestsCount++;
}

void testInr(Cpu8080 *cpu)
{
    /* Basic functionality */
    testInrOp(cpu, &cpu->a, INR_A_8080, 'A', false);
    testInrOp(cpu, &cpu->b, INR_B_8080, 'B', false);
    testInrOp(cpu, &cpu->c, INR_C_8080, 'C', false);
    testInrOp(cpu, &cpu->d, INR_D_8080, 'D', false);
    testInrOp(cpu, &cpu->e, INR_E_8080, 'E', false);
    testInrOp(cpu, &cpu->h, INR_H_8080, 'H', false);
    testInrOp(cpu, &cpu->l, INR_L_8080, 'L', false);
    testInrOp(cpu, &cpu->memory[0x1234], INR_M_8080, 'M', true);
    
    /* Sign flag test */
    testInrSignFlag(cpu, &cpu->b, INR_B_8080, 'B', false);
    testInrSignFlag(cpu, &cpu->b, INR_B_8080, 'B', false);
    testInrSignFlag(cpu, &cpu->c, INR_C_8080, 'C', false);
    testInrSignFlag(cpu, &cpu->d, INR_D_8080, 'D', false);
    testInrSignFlag(cpu, &cpu->e, INR_E_8080, 'E', false);
    testInrSignFlag(cpu, &cpu->h, INR_H_8080, 'H', false);
    testInrSignFlag(cpu, &cpu->l, INR_L_8080, 'L', false);
    testInrSignFlag(cpu, &cpu->memory[0x1234], INR_M_8080, 'M', true);

    /* Zero flag test */
    testInrZeroFlag(cpu, &cpu->a, INR_A_8080, 'A', false);
    testInrZeroFlag(cpu, &cpu->b, INR_B_8080, 'B', false);
    testInrZeroFlag(cpu, &cpu->c, INR_C_8080, 'C', false);
    testInrZeroFlag(cpu, &cpu->d, INR_D_8080, 'D', false);
    testInrZeroFlag(cpu, &cpu->e, INR_E_8080, 'E', false);
    testInrZeroFlag(cpu, &cpu->h, INR_H_8080, 'H', false);
    testInrZeroFlag(cpu, &cpu->l, INR_L_8080, 'L', false);
    testInrZeroFlag(cpu, &cpu->memory[0x1234], INR_M_8080, 'M', true);
    
    /* Parity flag test */
    testInrParityFlag(cpu, &cpu->a, INR_A_8080, 'A', false);
    testInrParityFlag(cpu, &cpu->b, INR_B_8080, 'B', false);
    testInrParityFlag(cpu, &cpu->c, INR_C_8080, 'C', false);
    testInrParityFlag(cpu, &cpu->d, INR_D_8080, 'D', false);
    testInrParityFlag(cpu, &cpu->e, INR_E_8080, 'E', false);
    testInrParityFlag(cpu, &cpu->h, INR_H_8080, 'H', false);
    testInrParityFlag(cpu, &cpu->l, INR_L_8080, 'L', false);
    testInrParityFlag(cpu, &cpu->memory[0x1234], INR_M_8080, 'M', true);
}

static inline void testDcrOp(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    if (isMemory) { cpu->h = 0x12; cpu->l = 0x34; } // for DCR_M only
    *reg = 2;
    op(cpu);
    assert(*reg == 1);
    
    printf("DCR_%c passed\n", regChar);
    passedTestsCount++;
}

static inline void testDcrSignFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg = 0;
    if (isMemory) { cpu->h = 0x12; cpu->l = 0x34; } // for DCR_M only
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 1);
    
    Reset_8080(cpu);
    *reg = 1;
    if (isMemory) { cpu->h = 0x12; cpu->l = 0x34; } // for DCR_M only
    op(cpu);
    assert(cpu->sr[SR_FLAG_S] == 0);
    
    printf("DCR_%c sign flag passed\n", regChar);
    passedTestsCount++;
}

static inline void testDcrZeroFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg = 1;
    if (isMemory) { cpu->h = 0x12; cpu->l = 0x34; } // for DCR_M only
    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 1);
    
    Reset_8080(cpu);
    *reg = 2;
    op(cpu);
    assert(cpu->sr[SR_FLAG_Z] == 0);
    
    printf("DCR_%c zero flag passed\n", regChar);
    passedTestsCount++;
}

static inline void testDcrParityFlag(Cpu8080 *cpu, uint8_t *reg, Op_8080 op, char regChar, bool isMemory)
{
    Reset_8080(cpu);
    *reg = 0b0111;   // even parity
    if (isMemory) { cpu->h = 0x12; cpu->l = 0x34; } // for DCR_M only
    op(cpu);
    assert(cpu->sr[SR_FLAG_P] == 1);
    
    Reset_8080(cpu);
    *reg = 0b0010;   // odd parity
    if (isMemory) { cpu->h = 0x12; cpu->l = 0x34; } // for DCR_M only
    op(cpu);
    assert(cpu->sr[SR_FLAG_P] == 0);
    
    printf("DCR_%c parity flag passed\n", regChar);
    passedTestsCount++;
}

void testDcr(Cpu8080 *cpu)
{
    /* Basic functionality */
    testDcrOp(cpu, &cpu->a, DCR_A_8080, 'A', false);
    testDcrOp(cpu, &cpu->b, DCR_B_8080, 'B', false);
    testDcrOp(cpu, &cpu->c, DCR_C_8080, 'C', false);
    testDcrOp(cpu, &cpu->d, DCR_D_8080, 'D', false);
    testDcrOp(cpu, &cpu->e, DCR_E_8080, 'E', false);
    testDcrOp(cpu, &cpu->h, DCR_H_8080, 'H', false);
    testDcrOp(cpu, &cpu->l, DCR_L_8080, 'L', false);
    testDcrOp(cpu, &cpu->memory[0x1234], DCR_M_8080, 'M', true);
    
    /* Sign flag */
    testDcrSignFlag(cpu, &cpu->a, DCR_A_8080, 'A', false);
    testDcrSignFlag(cpu, &cpu->b, DCR_B_8080, 'B', false);
    testDcrSignFlag(cpu, &cpu->c, DCR_C_8080, 'C', false);
    testDcrSignFlag(cpu, &cpu->d, DCR_D_8080, 'D', false);
    testDcrSignFlag(cpu, &cpu->e, DCR_E_8080, 'E', false);
    testDcrSignFlag(cpu, &cpu->h, DCR_H_8080, 'H', false);
    testDcrSignFlag(cpu, &cpu->l, DCR_L_8080, 'L', false);
    testDcrSignFlag(cpu, &cpu->memory[0x1234], DCR_M_8080, 'M', true);
    
    /* Zero flag */
    testDcrZeroFlag(cpu, &cpu->a, DCR_A_8080, 'A', false);
    testDcrZeroFlag(cpu, &cpu->b, DCR_B_8080, 'B', false);
    testDcrZeroFlag(cpu, &cpu->c, DCR_C_8080, 'C', false);
    testDcrZeroFlag(cpu, &cpu->d, DCR_D_8080, 'D', false);
    testDcrZeroFlag(cpu, &cpu->e, DCR_E_8080, 'E', false);
    testDcrZeroFlag(cpu, &cpu->h, DCR_H_8080, 'H', false);
    testDcrZeroFlag(cpu, &cpu->l, DCR_L_8080, 'L', false);
    testDcrZeroFlag(cpu, &cpu->memory[0x1234], DCR_M_8080, 'M', true);
    
    /* Parity flag */
    testDcrParityFlag(cpu, &cpu->a, DCR_A_8080, 'A', false);
    testDcrParityFlag(cpu, &cpu->b, DCR_B_8080, 'B', false);
    testDcrParityFlag(cpu, &cpu->c, DCR_C_8080, 'C', false);
    testDcrParityFlag(cpu, &cpu->d, DCR_D_8080, 'D', false);
    testDcrParityFlag(cpu, &cpu->e, DCR_E_8080, 'E', false);
    testDcrParityFlag(cpu, &cpu->h, DCR_H_8080, 'H', false);
    testDcrParityFlag(cpu, &cpu->l, DCR_L_8080, 'L', false);
    testDcrParityFlag(cpu, &cpu->memory[0x1234], DCR_M_8080, 'M', true);
}

void testRlc(Cpu8080 *cpu)
{
    /* Basic functionality */
    Reset_8080(cpu);
    cpu->a = 0b10010110;
    RLC_8080(cpu);
    assert(cpu->a == 0b00101101);
    
    /* Carry flag */
    assert(cpu->sr[SR_FLAG_C] == 1);
    
    /* Basic functionality */
    Reset_8080(cpu);
    cpu->a = 0b00111011;
    RLC_8080(cpu);
    assert(cpu->a == 0b01110110);
    
    /* Carry flag */
    assert(cpu->sr[SR_FLAG_C] == 0);
    
    printf("RLC passed\n");
    passedTestsCount++;
}

void testRrc(Cpu8080 *cpu)
{
    /* Basic functionality */
    Reset_8080(cpu);
    cpu->a = 0b10010110;
    RRC_8080(cpu);
    assert(cpu->a == 0b01001011);
    
    /* Carry flag */
    assert(cpu->sr[SR_FLAG_C] == 0);
    
    /* Basic functionality */
    Reset_8080(cpu);
    cpu->a = 0b00111011;
    RRC_8080(cpu);
    assert(cpu->a == 0b10011101);
    
    /* Carry flag */
    assert(cpu->sr[SR_FLAG_C] == 1);
    
    printf("RRC passed\n");
    passedTestsCount++;
}

void testRal(Cpu8080 *cpu)
{
    /* Basic functionality */
    Reset_8080(cpu);
    cpu->a = 0b11100111;
    cpu->sr[SR_FLAG_C] = 0;
    RAL_8080(cpu);
    assert(cpu->a == 0b11001110);
    
    /* Carry flag */
    assert(cpu->sr[SR_FLAG_C] == 1);

    /* Basic functionality */
    Reset_8080(cpu);
    cpu->a = 0b00110100;
    cpu->sr[SR_FLAG_C] = 1;
    RAL_8080(cpu);
    assert(cpu->a == 0b01101001);

    /* Carry flag */
    assert(cpu->sr[SR_FLAG_C] == 0);

    printf("RAL passed\n");
    passedTestsCount++;
}

void testRar(Cpu8080 *cpu)
{
    /* Basic functionality */
    Reset_8080(cpu);
    cpu->a = 0b10010110;
    RAR_8080(cpu);
    assert(cpu->a == 0b11001011);
    assert(cpu->sr[SR_FLAG_C] == 0);

    Reset_8080(cpu);
    cpu->a = 0b01101011;
    RAR_8080(cpu);
    assert(cpu->a == 0b00110101);
    assert(cpu->sr[SR_FLAG_C] == 1);

    printf("RAR passed\n");
    passedTestsCount++;
}

static inline void testLxiOp(Cpu8080 *cpu, uint8_t *reg1, uint8_t *reg2, Op_8080 op, char *regChars)
{
    /* Basic functionality */
    Reset_8080(cpu);
    cpu->pc = 0x2001;
    cpu->memory[0x2002] = 0x12;
    cpu->memory[0x2003] = 0x34;
    op(cpu);
    assert(*reg1 == 0x34);
    assert(*reg2 == 0x12);

    printf("LXI_%s passed\n", regChars);
    passedTestsCount++;
}

void testLxi(Cpu8080 *cpu)
{
    /* Basic functionality */
    testLxiOp(cpu, &cpu->b, &cpu->c, LXI_B_8080, "BC");
    testLxiOp(cpu, &cpu->d, &cpu->e, LXI_D_8080, "DE");
    testLxiOp(cpu, &cpu->h, &cpu->l, LXI_H_8080, "HL");
}

void testDadOp(Cpu8080 *cpu, uint8_t *reg1, uint8_t *reg2, Op_8080 op, char *regChars)
{
    /* Basic functionality */
    Reset_8080(cpu);
    cpu->h = 0x12;
    cpu->l = 0x34;
    *reg1 = 0x56;
    *reg2 = 0x78;
    op(cpu);
    assert(cpu->h == 0x68);
    assert(cpu->l == 0xAC);

    /* Carry Flag */
    Reset_8080(cpu);
    cpu->h = 0xFF;
    cpu->l = 0xFF;
    *reg1 = 0x00;
    *reg2 = 0x01;
    op(cpu);
    assert(cpu->sr[SR_FLAG_C] == 1);

    Reset_8080(cpu);
    cpu->h = 0xFF;
    cpu->l = 0xFE;
    *reg1 = 0x00;
    *reg2 = 0x01;
    op(cpu);
    assert(cpu->sr[SR_FLAG_C] == 0);

    printf("DAD_%s passed\n", regChars);
    passedTestsCount++;
}

void testDadOps(Cpu8080 *cpu)
{
    testDadOp(cpu, &cpu->b, &cpu->c, DAD_B_8080, "BC");
    testDadOp(cpu, &cpu->d, &cpu->e, DAD_D_8080, "DE");

    /* HL case is special since its adding to itself */
    Reset_8080(cpu);
    cpu->h = 0x12;
    cpu->l = 0x34;
    DAD_H_8080(cpu);
    assert(cpu->h == 0x24);
    assert(cpu->l == 0x68);
    printf("DAD_HL passed\n");
    passedTestsCount++;

    /* SP special since it's only 1 register */
    Reset_8080(cpu);
    cpu->h = 0x12;
    cpu->l = 0x34;
    cpu->sp = 0x1111;
    DAD_SP_8080(cpu);
    assert(cpu->h == 0x23);
    assert(cpu->l == 0x45);
    printf("DAD_SP passed\n");
    passedTestsCount++;
}

void testLdaxOp(Cpu8080 *cpu, uint8_t *reg1, uint8_t *reg2, Op_8080 op, char *regChars)
{
    /* Basic functionality */ 
    Reset_8080(cpu);
    *reg1 = 0x20;
    *reg2 = 0x01;
    cpu->memory[0x2001] = 10;
    op(cpu);
    assert(cpu->a == 10);

    printf("LDAX_%s passed\n", regChars);
    passedTestsCount++;
    
}

void testLdaxOps(Cpu8080 *cpu)
{
    testLdaxOp(cpu, &cpu->b, &cpu->c, LDAX_B_8080, "BC");
    testLdaxOp(cpu, &cpu->d, &cpu->e, LDAX_D_8080, "DE");
}

void testLda(Cpu8080 *cpu)
{
    Reset_8080(cpu);
    cpu->memory[0x1234] = 20;
    cpu->pc = 0x2001;
    cpu->memory[0x2003] = 0x12;
    cpu->memory[0x2002] = 0x34;

    LDA_8080(cpu);
    assert(cpu->a == 20);

    printf("LDA passed\n");
    passedTestsCount++;
}

void testDcxOp(Cpu8080 *cpu, uint8_t *reg1, uint8_t *reg2, Op_8080 op, char *regChars)
{
    /* Basic functionality */
    Reset_8080(cpu);
    *reg1 = 0x12;
    *reg2 = 0x34;
    op(cpu);
    assert(*reg1 == 0x12);
    assert(*reg2 == 0x33);

    printf("DCX_%s passed\n", regChars);
    passedTestsCount++;
}

void testDcxOps(Cpu8080 *cpu)
{
    testDcxOp(cpu, &cpu->b, &cpu->c, DCX_B_8080, "BC");
    testDcxOp(cpu, &cpu->d, &cpu->e, DCX_D_8080, "DE");
    testDcxOp(cpu, &cpu->h, &cpu->l, DCX_H_8080, "HL");

    /* Special case */
    Reset_8080(cpu);
    cpu->sp = 0; // test underflow
    DCX_SP_8080(cpu);
    assert(cpu->sp == 0xFFFF);
    printf("DCX_SP passed\n");
    passedTestsCount++;
}

void testShld(Cpu8080 *cpu)
{
    Reset_8080(cpu);
    cpu->pc = 0x2001;
    cpu->memory[0x2002] = 0x05;
    cpu->memory[0x2003] = 0x20;
    cpu->h = 0x12;
    cpu->l = 0x34;
    SHLD_8080(cpu);
    assert(cpu->memory[0x2005] == 0x34);
    assert(cpu->memory[0x2006] == 0x12);

    printf("SHLD passed\n");
    passedTestsCount++;
}

void testLhld(Cpu8080 *cpu)
{
    Reset_8080(cpu);
    cpu->pc = 0x2001;
    cpu->memory[0x2002] = 0x34;
    cpu->memory[0x2003] = 0x12;
    cpu->memory[0x1234] = 10;
    cpu->memory[0x1235] = 20;
    LHLD_8080(cpu);
    assert(cpu->h == 20);
    assert(cpu->l == 10);

    printf("LHLD passed\n");
    passedTestsCount++;
}

void testStc(Cpu8080 *cpu)
{
    Reset_8080(cpu);
    STC_8080(cpu);
    assert(cpu->sr[SR_FLAG_C] == 1);
    printf("STC passed\n");
    passedTestsCount++;
}

void testComplements(Cpu8080 *cpu)
{
    Reset_8080(cpu);
    cpu->sr[SR_FLAG_C] = 0;
    CMC_8080(cpu);
    assert(cpu->sr[SR_FLAG_C] == 1);

    Reset_8080(cpu);
    cpu->sr[SR_FLAG_C] = 1;
    CMC_8080(cpu);
    assert(cpu->sr[SR_FLAG_C] == 0);

    printf("CMC passed\n");
    passedTestsCount++;

    
    Reset_8080(cpu);
    cpu->a = 0;
    CMA_8080(cpu);
    assert(cpu->a == 1);

    Reset_8080(cpu);
    cpu->a = 1;
    CMA_8080(cpu);
    assert(cpu->a == 0);

    printf("CMA passed\n");
    passedTestsCount++;
}

void testReturnOps(Cpu8080 *cpu)
{
    /* RNZ */
    Reset_8080(cpu);
    cpu->memory[0x2001] = 0x12;
    cpu->memory[0x2002] = 0x34;
    cpu->sp = 0x2001;
    cpu->sr[SR_FLAG_Z] = 0; // return should happen
    RNZ_8080(cpu);
    assert(cpu->pc == 0x3412);
    assert(cpu->sp == 0x2003);
    
    Reset_8080(cpu);
    cpu->memory[0x2001] = 0x12;
    cpu->memory[0x2002] = 0x34;
    cpu->sp = 0x2001;
    cpu->sr[SR_FLAG_Z] = 1; // return shouldn't happen
    RNZ_8080(cpu);
    assert(cpu->pc == 0x0000);
    assert(cpu->sp == 0x2001);

    printf("RNZ passed\n");
    passedTestsCount++;

    /* RZ */
    Reset_8080(cpu);
    cpu->memory[0x2001] = 0x12;
    cpu->memory[0x2002] = 0x34;
    cpu->sp = 0x2001;
    cpu->sr[SR_FLAG_Z] = 1; // return should happen
    RZ_8080(cpu);
    assert(cpu->pc == 0x3412);
    assert(cpu->sp == 0x2003);
    
    Reset_8080(cpu);
    cpu->memory[0x2001] = 0x12;
    cpu->memory[0x2002] = 0x34;
    cpu->sp = 0x2001;
    cpu->sr[SR_FLAG_Z] = 0; // return shouldn't happen
    RZ_8080(cpu);
    assert(cpu->pc == 0x0000);
    assert(cpu->sp == 0x2001);

    printf("RZ passed\n");
    passedTestsCount++;
    
    /* RET */
    Reset_8080(cpu);
    cpu->memory[0x2001] = 0x12;
    cpu->memory[0x2002] = 0x34;
    cpu->sp = 0x2001;
    RET_8080(cpu);
    assert(cpu->pc == 0x3412);
    assert(cpu->sp == 0x2003);
    
    printf("RET passed\n");
    passedTestsCount++;

    /* RNC */
    Reset_8080(cpu);
    cpu->memory[0x2001] = 0x12;
    cpu->memory[0x2002] = 0x34;
    cpu->sp = 0x2001;
    cpu->sr[SR_FLAG_C] = 0; // return should happen
    RNC_8080(cpu);
    assert(cpu->pc == 0x3412);
    assert(cpu->sp == 0x2003);
    
    Reset_8080(cpu);
    cpu->memory[0x2001] = 0x12;
    cpu->memory[0x2002] = 0x34;
    cpu->sp = 0x2001;
    cpu->sr[SR_FLAG_C] = 1; // return shouldn't happen
    RNC_8080(cpu);
    assert(cpu->pc == 0x0000);
    assert(cpu->sp == 0x2001);

    printf("RNC passed\n");
    passedTestsCount++;
}

void testJmpOps(Cpu8080 *cpu)
{
    /* JMP */
    Reset_8080(cpu);
    cpu->pc = 0x2001;
    cpu->memory[0x2002] = 0x12;
    cpu->memory[0x2003] = 0x34;
    JMP_8080(cpu);

    /* +1 to account for our -1 in the op */
    assert(cpu->pc+1 == 0x3412);

    printf("JMP passed\n");
    passedTestsCount++;

    /* JNZ */
    Reset_8080(cpu);
    cpu->pc = 0x2001;
    cpu->memory[0x2002] = 0x12;
    cpu->memory[0x2003] = 0x34;
    cpu->sr[SR_FLAG_Z] = 0; // jmp should happen
    JNZ_8080(cpu);

    /* +1 to account for our -1 in the op */
    assert(cpu->pc+1 == 0x3412);

    Reset_8080(cpu);
    cpu->pc = 0x2001;
    cpu->memory[0x2002] = 0x12;
    cpu->memory[0x2003] = 0x34;
    cpu->sr[SR_FLAG_Z] = 1; // jmp shouldn't happen
    JNZ_8080(cpu);

    /* +1 to account for our -1 in the op */
    assert(cpu->pc+1 == 0x2002);

    printf("JNZ passed\n");
    passedTestsCount++;

    /* JZ */
    Reset_8080(cpu);
    cpu->pc = 0x2001;
    cpu->memory[0x2002] = 0x12;
    cpu->memory[0x2003] = 0x34;
    cpu->sr[SR_FLAG_Z] = 1; // jmp should happen
    JZ_8080(cpu);

    /* +1 to account for our -1 in the op */
    assert(cpu->pc+1 == 0x3412);

    Reset_8080(cpu);
    cpu->pc = 0x2001;
    cpu->memory[0x2002] = 0x12;
    cpu->memory[0x2003] = 0x34;
    cpu->sr[SR_FLAG_Z] = 0; // jmp shouldn't happen
    JZ_8080(cpu);

    /* +1 to account for our -1 in the op */
    assert(cpu->pc+1 == 0x2002);

    printf("JZ passed\n");
    passedTestsCount++;
}

void testCallOps(Cpu8080 *cpu)
{
    /* CALL */
    Reset_8080(cpu);
    cpu->sp = 0x2008;
    cpu->pc = 0x2001;
    cpu->memory[0x2002] = 0x56;
    cpu->memory[0x2003] = 0x78;
    CALL_8080(cpu);
    assert(cpu->sp == 0x2006); // SP - 2
    /* +1 to compensate for the -1 */
    assert(cpu->pc+1 == 0x7856);
    /* 0x2003 because next instruction (pc+2) */
    assert(cpu->memory[0x2007] == 0x20); // PC high
    assert(cpu->memory[0x2006] == 0x03); // PC low

    printf("CALL passed\n");
    passedTestsCount++;

    /* CNZ */
    Reset_8080(cpu);
    cpu->sp = 0x2008;
    cpu->pc = 0x2001;
    cpu->memory[0x2002] = 0x56;
    cpu->memory[0x2003] = 0x78;
    cpu->sr[SR_FLAG_Z] = 0;      // Should call
    CNZ_8080(cpu);
    assert(cpu->sp == 0x2006); // SP - 2
    /* +1 to compensate for the -1 */
    assert(cpu->pc+1 == 0x7856);
    /* 0x2003 because next instruction (pc+2) */
    assert(cpu->memory[0x2007] == 0x20); // PC high
    assert(cpu->memory[0x2006] == 0x03); // PC low 
    
    Reset_8080(cpu);
    cpu->sp = 0x2008;
    cpu->pc = 0x2001;
    cpu->memory[0x2002] = 0x56;
    cpu->memory[0x2003] = 0x78;
    cpu->sr[SR_FLAG_Z] = 1;       // Shouldn't call
    CNZ_8080(cpu);
    assert(cpu->sp == 0x2008); // SP - 2
    /* +1 to compensate for the -1 */
    assert(cpu->pc+1 == 0x2002);
    /* 0x2003 because next instruction (pc+2) */
    assert(cpu->memory[0x2007] == 0x00); // PC high
    assert(cpu->memory[0x2006] == 0x00); // PC low

    printf("CNZ passed\n");
    passedTestsCount++;

    /* CZ */
    Reset_8080(cpu);
    cpu->sp = 0x2008;
    cpu->pc = 0x2001;
    cpu->memory[0x2002] = 0x56;
    cpu->memory[0x2003] = 0x78;
    cpu->sr[SR_FLAG_Z] = 1;      // Should call
    CZ_8080(cpu);
    assert(cpu->sp == 0x2006); // SP - 2
    /* +1 to compensate for the -1 */
    assert(cpu->pc+1 == 0x7856);
    /* 0x2003 because next instruction (pc+2) */
    assert(cpu->memory[0x2007] == 0x20); // PC high
    assert(cpu->memory[0x2006] == 0x03); // PC low 
    
    Reset_8080(cpu);
    cpu->sp = 0x2008;
    cpu->pc = 0x2001;
    cpu->memory[0x2002] = 0x56;
    cpu->memory[0x2003] = 0x78;
    cpu->sr[SR_FLAG_Z] = 0;       // Shouldn't call
    CZ_8080(cpu);
    assert(cpu->sp == 0x2008); // SP - 2
    /* +1 to compensate for the -1 */
    assert(cpu->pc+1 == 0x2002);
    /* 0x2003 because next instruction (pc+2) */
    assert(cpu->memory[0x2007] == 0x00); // PC high
    assert(cpu->memory[0x2006] == 0x00); // PC low

    printf("CZ passed\n");
    passedTestsCount++;
}

static inline void testRstOp(Cpu8080 *cpu, uint8_t nnn, Op_8080 op)
{
    Reset_8080(cpu);
    cpu->pc = 0x1234;
    cpu->sp = 0x1999;
    op(cpu);
    assert(cpu->memory[0x1998] == 0x12);
    assert(cpu->memory[0x1997] == 0x34);
    assert(cpu->sp == 0x1997);
    // adjust for the auto pc++
    assert((uint8_t)(cpu->pc+1) == 8*nnn);

    printf("RST %d passed\n", nnn);
    passedTestsCount++;
}

void testRstOps(Cpu8080 *cpu)
{
    testRstOp(cpu, 0, RST_0_8080);
    testRstOp(cpu, 1, RST_1_8080);
}

int main(void)
{
    Cpu8080 cpu;
    Init_8080(&cpu);
    
    testMoveOps(&cpu);
    testAddOps(&cpu);
    testAddcOps(&cpu);
    testSbbOps(&cpu);
    testAndOps(&cpu);
    testXorOps(&cpu);
    testOrOps(&cpu);
    testCmpOps(&cpu);
    testStaxOps(&cpu);
    testStaOp(&cpu);
    testInxOps(&cpu);
    testInr(&cpu);
    testDcr(&cpu);
    testRlc(&cpu);
    testRrc(&cpu);
    testRal(&cpu);
    testRar(&cpu);
    testDadOps(&cpu);
    testLdaxOps(&cpu);
    testDcxOps(&cpu);
    testMviOps(&cpu);
    testLxi(&cpu);
    testLda(&cpu);
    testShld(&cpu);
    testLhld(&cpu);
    testStc(&cpu);
    testComplements(&cpu);
    testReturnOps(&cpu);
    testPopOps(&cpu);
    testPushOps(&cpu);
    testJmpOps(&cpu);
    testCallOps(&cpu);
    testRstOps(&cpu);
    
    printf("\n%d tests passed\n", passedTestsCount);
    
    return 0;
}
